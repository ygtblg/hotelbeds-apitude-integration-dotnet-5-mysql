﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Castle.DynamicProxy;

namespace Core.Utilities.Interceptors
{
    public abstract class MethodInterception : MethodInterceptionBaseAttribute
    {
        protected virtual void OnBefore(IInvocation invocation) { }
        protected virtual void OnAfter(IInvocation invocation) { }
        protected virtual void OnException(IInvocation invocation, System.Exception e) { }
        protected virtual void OnSuccess(IInvocation invocation) { }
        // public override void Intercept(IInvocation invocation)
        // {
        //     var isSuccess = true;
        //     OnBefore(invocation);
        //     try
        //     {
        //         invocation.Proceed();
        //     }
        //     catch (Exception e)
        //     {
        //         isSuccess = false;
        //         OnException(invocation,e);
        //         throw;
        //     }
        //     finally
        //     {
        //         if (isSuccess)
        //         {
        //             OnSuccess(invocation);
        //         }
        //     }
        //     OnAfter(invocation);
        // }


        public override void Intercept(IInvocation invocation)
        {
            if (IsAsyncMethod(invocation.Method))
            {
                System.Console.WriteLine("STARTED ASYNC ****" + invocation.MethodInvocationTarget.Name );
                InterceptAsync(invocation);
            }
            else
            {
                InterceptSync(invocation);
            }
        }


        private void InterceptAsync(IInvocation invocation)
        {
            //Before method execution
            var stopwatch = Stopwatch.StartNew();

            //Calling the actual method, but execution has not been finished yet
            // invocation.Proceed(); // yb-1
            var isSuccess = true;           


            // yb start
            OnBefore(invocation);
            try
            {
                invocation.Proceed();
                System.Console.WriteLine("RUNNING ASYNC ****" + invocation.MethodInvocationTarget.Name );
                //We should wait for finishing of the method execution
                ((Task)invocation.ReturnValue)
                    .ContinueWith(task =>
                    {

                    if(task.Exception != null && task.Exception.Message != null){
                        System.Console.WriteLine("ON ERROR ASYNC ****" + invocation.MethodInvocationTarget.Name );
                        isSuccess = false;
                        OnException(invocation, task.Exception);
                    }

                    //After method execution
                    System.Console.WriteLine("WATCH STOP ****" + invocation.MethodInvocationTarget.Name );
                    stopwatch.Stop();
                    // Logger.InfoFormat(
                    //     "MeasureDurationAsyncInterceptor: {0} executed in {1} milliseconds.",
                    //     invocation.MethodInvocationTarget.Name,
                    //     stopwatch.Elapsed.TotalMilliseconds.ToString("0.000")
                    //     );MySqlConnector.MySqlException
                });

            }
            catch (Exception e)
            {
                System.Console.WriteLine("ON ERROR ASYNC ****" + invocation.MethodInvocationTarget.Name );
                isSuccess = false;
                OnException(invocation, e);
                throw;
            }
            finally
            {
                if (isSuccess)
                {
                    System.Console.WriteLine("FINISHED BY SUCCESS ASYNC ****" + invocation.MethodInvocationTarget.Name );
                    OnSuccess(invocation);
                }
            }
            OnAfter(invocation);
            // end of yb



        }

        private void InterceptSync(IInvocation invocation)
        {
            //Before method execution
            var stopwatch = Stopwatch.StartNew();

            //Executing the actual method
            // invocation.Proceed(); // yb-2



            // yb start
            var isSuccess = true;
            OnBefore(invocation);
            try
            {
                invocation.Proceed();
            }
            catch (Exception e)
            {
                isSuccess = false;
                OnException(invocation, e);
                throw;
            }
            finally
            {
                if (isSuccess)
                {
                    OnSuccess(invocation);
                }
            }
            OnAfter(invocation);
            // end of yb







            //After method execution
            stopwatch.Stop();
            // Logger.InfoFormat(
            //     "MeasureDurationAsyncInterceptor: {0} executed in {1} milliseconds.",
            //     invocation.MethodInvocationTarget.Name,
            //     stopwatch.Elapsed.TotalMilliseconds.ToString("0.000")
            //     );
        }

        public static bool IsAsyncMethod(MethodInfo method)
        {
            return (
                method.ReturnType == typeof(Task) ||
                (method.ReturnType.IsGenericType && method.ReturnType.GetGenericTypeDefinition() == typeof(Task<>))
                );
        }



    }
}
