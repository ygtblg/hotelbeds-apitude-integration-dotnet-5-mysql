﻿/*
CREATED BY Yiğit Bilge<yigitbilge@gmail.com> 2020-11-18 05:15:00
*/
using System.Collections.Generic;
using Microsoft.Extensions.Configuration; 

namespace Core.Utilities.BedsApi
{
    public class BedsApiHelper:IBedsApiHelper
    {
        public IConfiguration Configuration { get; }
        private BedsApiCredentials _bedsApiCredentials; 
        private AppSettings _appSettings; 
        private List<Environments> _environments; 


        public BedsApiHelper(IConfiguration configuration)
        {
            Configuration = configuration;
            // System.Console.WriteLine( Configuration.GetSection("AppSettings").Get<AppSettings>());
            _appSettings = Configuration.GetSection("AppSettings").Get<AppSettings>(); 
            _environments = Configuration.GetSection("Environments").Get<List<Environments>>(); 
            // _bedsApiCredentials = Configuration.GetSection("BedsApiCredentials").Get<BedsApiCredentials>(); 
            // System.Console.WriteLine(_appSettings.ENVIRONMENT);

            foreach (var e in _environments)
            {
                
                if(e.name.Equals(_appSettings.ENVIRONMENT)){
                    System.Console.WriteLine(e.name +" ?= "+ _appSettings.ENVIRONMENT);
                    _bedsApiCredentials = e.BedsApiCredentials;
                }
            }
        }

        public BedsApiCredentials getCredentials()
        {  
            return _bedsApiCredentials;
        }

    }
}
