namespace Core.Utilities.BedsApi
{
    public interface IBedsApiHelper
    {
        BedsApiCredentials getCredentials();
    }
}