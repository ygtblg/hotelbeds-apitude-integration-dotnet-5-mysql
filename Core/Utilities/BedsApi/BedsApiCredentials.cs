namespace Core.Utilities.BedsApi
{
    public class BedsApiCredentials
    {
        public  string BasePath { get; set; }
        public  string Version { get; set; }
        public  double TimeoutSeconds { get; set; }
        public  string Apikey { get; set; }
        public  string SharedSecret { get; set; } 
    }

    public class Environments
    {
        public  string name { get; set; }
        public BedsApiCredentials BedsApiCredentials  { get; set; }
    }

    public class AppSettings
    {
        public string ENVIRONMENT { get; set; }
    }


}