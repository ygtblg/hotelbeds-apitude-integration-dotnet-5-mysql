﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Core.CrossCuttingConcerns.Logging;
using Core.CrossCuttingConcerns.Logging.Log4Net;
using Core.CrossCuttingConcerns.Logging.Log4Net.Loggers;
using FluentValidation;
using Microsoft.AspNetCore.Http;


namespace Core.Extensions
{
    public class ExceptionMiddleware
    {
        private RequestDelegate _next;
        private LoggerServiceBase _loggerServiceBase;

        public ExceptionMiddleware(RequestDelegate next)
        {
            _next = next;

            //if (loggerService.BaseType != typeof(LoggerServiceBase))
            //{
            //    throw new System.Exception(AspectMessages.WrongLoggerType);
            //}

            _loggerServiceBase = (LoggerServiceBase)Activator.CreateInstance(typeof(FileLogger));

        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (Exception e)
            {
                await HandleExceptionAsync(httpContext, e);
            }
        }

        private Task HandleExceptionAsync(HttpContext httpContext, Exception e)
        {


            LogDetailWithException logDetailWithException = GetLogDetail();
            logDetailWithException.ExceptionMessage = e.Message;
            _loggerServiceBase.Error(logDetailWithException);


            httpContext.Response.ContentType = "application/json";
            httpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

            string message = "Internal Server Error";
            string exceptionType = e.GetType().ToString();
            string validationExceptionType = typeof(ValidationException).ToString();
            if (e.GetType() == typeof(ValidationException))
            {
                message = e.Message;
            }
            else if (e.GetType() == typeof(UnauthorizedAccessException))
            {
                message = e.Message;
                httpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
            }

            return httpContext.Response.WriteAsync(new ErrorDetails
            {
                StatusCode = httpContext.Response.StatusCode,
                Message = message
            }.ToString());
        }

        private LogDetailWithException GetLogDetail()
        {
            var logParameters = new List<LogMethodParameter>();
            var logDetailWithException = new LogDetailWithException
            {
                MethodName = "",
                MethodTarget = "",
                LogMethodParameters = logParameters,
                LogCreated = DateTime.Now
            };

            return logDetailWithException;
        }








    }
}
