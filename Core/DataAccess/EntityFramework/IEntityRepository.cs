/*
CREATED BY Yiğit Bilge<yigitbilge@gmail.com> 2020-11-18 05:15:00
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Core.Entities;
using Microsoft.EntityFrameworkCore.Query;

namespace Core.DataAccess
{
    public interface IEntityRepository<T> where T : class, IEntity, new()
    {
        Task<T> Get(Expression<Func<T, bool>> filter); 
        Task<IList<T>> GetList(Expression<Func<T, Object>>[] includes=null, Expression<Func<T, bool>> filter = null);
        Task<IList<T>> GetListByIncludes(Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null);
        void Add(T entity);
        void Update(T entity);
        void Delete(T entity);
    }
}
