/*
CREATED BY Yiğit Bilge<yigitbilge@gmail.com> 2020-11-18 05:15:00
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;

namespace Core.DataAccess.EntityFramework
{



    public class EfEntityRepositoryBase<TEntity, TContext> : IEntityRepository<TEntity>
    where TEntity : class, IEntity, new()
    where TContext : DbContext, new()
    {



        public void Add(TEntity entity)
        {
            using (var context = new TContext())
            {
                var addedEntity = context.Entry(entity);
                addedEntity.State = EntityState.Added;
                context.SaveChanges();
            }
        }

        public void Update(TEntity entity)
        {
            using (var context = new TContext())
            {
                var updatedEntity = context.Entry(entity);
                updatedEntity.State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void Delete(TEntity entity)
        {
            using (var context = new TContext())
            {
                var deletedEntity = context.Entry(entity);
                deletedEntity.State = EntityState.Deleted;
                context.SaveChanges();
            }
        }

        public async Task<TEntity> Get(Expression<Func<TEntity, bool>> filter)
        {
            using (var context = new TContext())
            {
                return await context.Set<TEntity>().SingleOrDefaultAsync(filter);
            }
        }

        public IList<TEntity> GetList_(Expression<Func<TEntity, bool>> filter = null)
        {
            using (var context = new TContext())
            {

                return filter == null
                    ? context.Set<TEntity>().ToList()
                    : context.Set<TEntity>().Where(filter).ToList();
            }
        }

        public async Task<IList<TEntity>> GetList(Expression<Func<TEntity, Object>>[] includes = null, Expression<Func<TEntity, bool>> filter = null)
        {
            using (var context = new TContext())
            {
                DbSet<TEntity> dbSet = context.Set<TEntity>();
                IQueryable<TEntity> query = dbSet.AsQueryable(); ;
                if (includes != null)
                {
                    foreach (var includeExpression in includes)
                    {
                        query = query.Include(includeExpression);
                    }

                    return filter == null
                        ? await query.ToListAsync()
                        : await query.Where(filter).ToListAsync();
                }

                return filter == null
                    ? await dbSet.ToListAsync()
                    : await dbSet.Where(filter).ToListAsync();
            }
        }

        public async Task<IList<TEntity>> GetListByIncludes(Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include = null)
        {
            using (var db = new TContext())
            {

                IQueryable<TEntity> query = db.Set<TEntity>().AsQueryable<TEntity>();
                if (include != null)
                {
                    query = include(query);
                }
                return await query.ToListAsync();
            }
        }


    }
}
