/*
CREATED BY Yiğit Bilge<yigitbilge@gmail.com> 2020-11-18 05:15:00
*/
using System.Collections.Generic;

namespace Core.Entities.Dtos
{
    public class UserForListDTO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public byte[] PasswordSalt { get; set; }
        public byte[] PasswordHash { get; set; }
        public bool Status { get; set; }
        public List<UserImagesForListDTO> Images { get; set; }
    }
}