﻿/*
CREATED BY Yiğit Bilge<yigitbilge@gmail.com> 2020-11-18 05:15:00
*/
using System;
using System.Collections.Generic;
using System.Text;
using Core.Entities;

namespace Core.Entities.Concrete
{
    public class UserOperationClaim:IEntity
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int OperationClaimId { get; set; }

    }
}
