/*
CREATED BY Yiğit Bilge<yigitbilge@gmail.com> 2020-11-18 05:15:00
*/
using System; 

namespace Core.Entities.Concrete
{
    public class UserImage
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime DateAdded { get; set; }
        public bool IsProfile { get; set; }
        public int UserId { get; set; } 
        
        public virtual User User { get; set; } 
    }
}