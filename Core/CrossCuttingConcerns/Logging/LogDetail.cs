﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.CrossCuttingConcerns.Logging
{
    public class LogDetail
    {
        public string MethodName { get; set; }
        public string MethodTarget { get; set; }
        public DateTime LogCreated { get; set; }
        public List<LogMethodParameter> LogMethodParameters { get; set; }
        
    }


}
