﻿/*
CREATED BY Yiğit Bilge<yigitbilge@gmail.com> 2020-11-18 05:15:00
*/
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.CrossCuttingConcerns.Logging
{
    public class LogMethodParameter
    {
        public string Name { get; set; }
        public object Value { get; set; }
        public string Type { get; set; }
        
    }
}
