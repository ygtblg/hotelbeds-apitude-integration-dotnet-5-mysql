/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 15/08/2021 11:07:46
   ThisFileName:BookPublisherManager.cs
   ThisFilePath:C:\projects\BackendPartner\dotnet\agile-ionic-generator-001-api\Business\Concrete\BookPublisherManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class BookPublisherManager : IBookPublisherService
    {
        private IBookPublisherDal _BookPublisherDal;

        public BookPublisherManager(IBookPublisherDal BookPublisherDal)
        {
            _BookPublisherDal = BookPublisherDal;
        }

        public IResult Add(BookPublisher BookPublisher)
        {
            //Business codes
            _BookPublisherDal.Add(BookPublisher);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(BookPublisher BookPublisher)
        {
            _BookPublisherDal.Delete(BookPublisher);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<BookPublisher>> GetById(int BookPublisherId)
        {
            return new SuccessDataResult<BookPublisher>( await _BookPublisherDal.Get(p => p.BookPublisherId == BookPublisherId));
        }

        public async Task<IDataResult<List<BookPublisher>>> GetList()
        {
            var u = (await _BookPublisherDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<BookPublisher>>(u);
        }

        public IResult Update(BookPublisher BookPublisher)
        {
            _BookPublisherDal.Update(BookPublisher);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
