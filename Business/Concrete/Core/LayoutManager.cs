/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 15/08/2021 11:07:46
   ThisFileName:LayoutManager.cs
   ThisFilePath:C:\projects\BackendPartner\dotnet\agile-ionic-generator-001-api\Business\Concrete\LayoutManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class LayoutManager : ILayoutService
    {
        private ILayoutDal _LayoutDal;

        public LayoutManager(ILayoutDal LayoutDal)
        {
            _LayoutDal = LayoutDal;
        }

        public IResult Add(Layout Layout)
        {
            //Business codes
            _LayoutDal.Add(Layout);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(Layout Layout)
        {
            _LayoutDal.Delete(Layout);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<Layout>> GetById(int LayoutId)
        {
            return new SuccessDataResult<Layout>( await _LayoutDal.Get(p => p.LayoutId == LayoutId));
        }

        public async Task<IDataResult<List<Layout>>> GetList()
        {
            var u = (await _LayoutDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<Layout>>(u);
        }

        public IResult Update(Layout Layout)
        {
            _LayoutDal.Update(Layout);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
