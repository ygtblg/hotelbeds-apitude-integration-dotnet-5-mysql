/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 15/08/2021 11:07:46
   ThisFileName:BookCategoryManager.cs
   ThisFilePath:C:\projects\BackendPartner\dotnet\agile-ionic-generator-001-api\Business\Concrete\BookCategoryManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class BookCategoryManager : IBookCategoryService
    {
        private IBookCategoryDal _BookCategoryDal;

        public BookCategoryManager(IBookCategoryDal BookCategoryDal)
        {
            _BookCategoryDal = BookCategoryDal;
        }

        public IResult Add(BookCategory BookCategory)
        {
            //Business codes
            _BookCategoryDal.Add(BookCategory);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(BookCategory BookCategory)
        {
            _BookCategoryDal.Delete(BookCategory);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<BookCategory>> GetById(int BookCategoryId)
        {
            return new SuccessDataResult<BookCategory>( await _BookCategoryDal.Get(p => p.BookCategoryId == BookCategoryId));
        }

        public async Task<IDataResult<List<BookCategory>>> GetList()
        {
            var u = (await _BookCategoryDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<BookCategory>>(u);
        }

        public IResult Update(BookCategory BookCategory)
        {
            _BookCategoryDal.Update(BookCategory);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
