/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 15/08/2021 11:07:46
   ThisFileName:LayoutParamManager.cs
   ThisFilePath:C:\projects\BackendPartner\dotnet\agile-ionic-generator-001-api\Business\Concrete\LayoutParamManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class LayoutParamManager : ILayoutParamService
    {
        private ILayoutParamDal _LayoutParamDal;

        public LayoutParamManager(ILayoutParamDal LayoutParamDal)
        {
            _LayoutParamDal = LayoutParamDal;
        }

        public IResult Add(LayoutParam LayoutParam)
        {
            //Business codes
            _LayoutParamDal.Add(LayoutParam);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(LayoutParam LayoutParam)
        {
            _LayoutParamDal.Delete(LayoutParam);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<LayoutParam>> GetById(int LayoutParamId)
        {
            return new SuccessDataResult<LayoutParam>( await _LayoutParamDal.Get(p => p.LayoutParamId == LayoutParamId));
        }

        public async Task<IDataResult<List<LayoutParam>>> GetList()
        {
            var u = (await _LayoutParamDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<LayoutParam>>(u);
        }

        public IResult Update(LayoutParam LayoutParam)
        {
            _LayoutParamDal.Update(LayoutParam);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
