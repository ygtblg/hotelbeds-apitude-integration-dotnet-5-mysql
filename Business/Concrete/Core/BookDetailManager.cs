/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 15/08/2021 11:07:46
   ThisFileName:BookDetailManager.cs
   ThisFilePath:C:\projects\BackendPartner\dotnet\agile-ionic-generator-001-api\Business\Concrete\BookDetailManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class BookDetailManager : IBookDetailService
    {
        private IBookDetailDal _BookDetailDal;

        public BookDetailManager(IBookDetailDal BookDetailDal)
        {
            _BookDetailDal = BookDetailDal;
        }

        public IResult Add(BookDetail BookDetail)
        {
            //Business codes
            _BookDetailDal.Add(BookDetail);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(BookDetail BookDetail)
        {
            _BookDetailDal.Delete(BookDetail);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<BookDetail>> GetById(int BookDetailId)
        {
            return new SuccessDataResult<BookDetail>( await _BookDetailDal.Get(p => p.BookDetailId == BookDetailId));
        }

        public async Task<IDataResult<List<BookDetail>>> GetList()
        {
            var u = (await _BookDetailDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<BookDetail>>(u);
        }

        public IResult Update(BookDetail BookDetail)
        {
            _BookDetailDal.Update(BookDetail);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
