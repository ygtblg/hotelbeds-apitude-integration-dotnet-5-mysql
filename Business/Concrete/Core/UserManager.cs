﻿/*
CREATED BY Yiğit Bilge<yigitbilge@gmail.com> 2020-11-18 05:15:00
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Business.Abstract;
using Business.BusinessAspects.Autofac;
using Core.Aspects.Autofac.Logging;
using Core.CrossCuttingConcerns.Logging.Log4Net.Loggers;
using Core.Entities.Concrete;
using Core.Utilities.Results;
using DataAccess.Abstract;

namespace Business.Concrete
{
    public class UserManager : IUserService
    {
        IUserDal _userDal;

        public UserManager(IUserDal userDal)
        {
            _userDal = userDal;
        }

        public List<OperationClaim> GetClaims(User user)
        {
            return _userDal.GetClaims(user);
        }

        public void Add(User user)
        {
            _userDal.Add(user);
        }

        public async Task<User> GetByMail(string email)
        {
            return await _userDal.Get(u => u.Email == email);
        }


        // [SecuredOperation("Product.List,Admin")]
        [SecuredOperation( "User.List", typeof(FileLogger) )]
        [LogAspect(typeof(FileLogger))]
        public async Task<IDataResult<List<User>>> GetList()
        {
            Expression<Func<User, Object>>[] includes = { i => i.Images };
            var u = (await _userDal.GetList(includes)).ToList();
            return new SuccessDataResult<List<User>>(u);
        }


    }
}
