/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 15/08/2021 11:07:46
   ThisFileName:PublisherManager.cs
   ThisFilePath:C:\projects\BackendPartner\dotnet\agile-ionic-generator-001-api\Business\Concrete\PublisherManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class PublisherManager : IPublisherService
    {
        private IPublisherDal _PublisherDal;

        public PublisherManager(IPublisherDal PublisherDal)
        {
            _PublisherDal = PublisherDal;
        }

        public IResult Add(Publisher Publisher)
        {
            //Business codes
            _PublisherDal.Add(Publisher);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(Publisher Publisher)
        {
            _PublisherDal.Delete(Publisher);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<Publisher>> GetById(int PublisherId)
        {
            return new SuccessDataResult<Publisher>( await _PublisherDal.Get(p => p.PublisherId == PublisherId));
        }

        public async Task<IDataResult<List<Publisher>>> GetList()
        {
            var u = (await _PublisherDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<Publisher>>(u);
        }

        public IResult Update(Publisher Publisher)
        {
            _PublisherDal.Update(Publisher);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
