/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 15/08/2021 11:07:46
   ThisFileName:BookManager.cs
   ThisFilePath:C:\projects\BackendPartner\dotnet\agile-ionic-generator-001-api\Business\Concrete\BookManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class BookManager : IBookService
    {
        private IBookDal _BookDal;

        public BookManager(IBookDal BookDal)
        {
            _BookDal = BookDal;
        }

        public IResult Add(Book Book)
        {
            //Business codes
            _BookDal.Add(Book);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(Book Book)
        {
            _BookDal.Delete(Book);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<Book>> GetById(int BookId)
        {
            return new SuccessDataResult<Book>( await _BookDal.Get(p => p.BookId == BookId));
        }

        public async Task<IDataResult<List<Book>>> GetList()
        {
            var u = (await _BookDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<Book>>(u);
        }

        public IResult Update(Book Book)
        {
            _BookDal.Update(Book);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
