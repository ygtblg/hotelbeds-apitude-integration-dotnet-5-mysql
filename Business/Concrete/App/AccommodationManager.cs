/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:AccommodationManager.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Concrete\App\AccommodationManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class AccommodationManager : IAccommodationService
    {
        private IAccommodationDal _AccommodationDal;

        public AccommodationManager(IAccommodationDal AccommodationDal)
        {
            _AccommodationDal = AccommodationDal;
        }

        public IResult Add(Accommodation Accommodation)
        {
            //Business codes
            _AccommodationDal.Add(Accommodation);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(Accommodation Accommodation)
        {
            _AccommodationDal.Delete(Accommodation);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<Accommodation>> GetById(int AccommodationId)
        {
            return new SuccessDataResult<Accommodation>( await _AccommodationDal.Get(p => p.AccommodationId == AccommodationId));
        }

        public async Task<IDataResult<List<Accommodation>>> GetList()
        {
            var u = (await _AccommodationDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<Accommodation>>(u);
        }

        public IResult Update(Accommodation Accommodation)
        {
            _AccommodationDal.Update(Accommodation);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
