/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:RoomFacilityManager.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Concrete\App\RoomFacilityManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class RoomFacilityManager : IRoomFacilityService
    {
        private IRoomFacilityDal _RoomFacilityDal;

        public RoomFacilityManager(IRoomFacilityDal RoomFacilityDal)
        {
            _RoomFacilityDal = RoomFacilityDal;
        }

        public IResult Add(RoomFacility RoomFacility)
        {
            //Business codes
            _RoomFacilityDal.Add(RoomFacility);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(RoomFacility RoomFacility)
        {
            _RoomFacilityDal.Delete(RoomFacility);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<RoomFacility>> GetById(int RoomFacilityId)
        {
            return new SuccessDataResult<RoomFacility>( await _RoomFacilityDal.Get(p => p.RoomFacilityId == RoomFacilityId));
        }

        public async Task<IDataResult<List<RoomFacility>>> GetList()
        {
            var u = (await _RoomFacilityDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<RoomFacility>>(u);
        }

        public IResult Update(RoomFacility RoomFacility)
        {
            _RoomFacilityDal.Update(RoomFacility);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
