/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:WildcardManager.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Concrete\App\WildcardManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class WildcardManager : IWildcardService
    {
        private IWildcardDal _WildcardDal;

        public WildcardManager(IWildcardDal WildcardDal)
        {
            _WildcardDal = WildcardDal;
        }

        public IResult Add(Wildcard Wildcard)
        {
            //Business codes
            _WildcardDal.Add(Wildcard);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(Wildcard Wildcard)
        {
            _WildcardDal.Delete(Wildcard);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<Wildcard>> GetById(int WildcardId)
        {
            return new SuccessDataResult<Wildcard>( await _WildcardDal.Get(p => p.WildcardId == WildcardId));
        }

        public async Task<IDataResult<List<Wildcard>>> GetList()
        {
            var u = (await _WildcardDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<Wildcard>>(u);
        }

        public IResult Update(Wildcard Wildcard)
        {
            _WildcardDal.Update(Wildcard);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
