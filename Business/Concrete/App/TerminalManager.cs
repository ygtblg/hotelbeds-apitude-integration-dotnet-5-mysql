/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:TerminalManager.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Concrete\App\TerminalManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class TerminalManager : ITerminalService
    {
        private ITerminalDal _TerminalDal;

        public TerminalManager(ITerminalDal TerminalDal)
        {
            _TerminalDal = TerminalDal;
        }

        public IResult Add(Terminal Terminal)
        {
            //Business codes
            _TerminalDal.Add(Terminal);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(Terminal Terminal)
        {
            _TerminalDal.Delete(Terminal);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<Terminal>> GetById(int TerminalId)
        {
            return new SuccessDataResult<Terminal>( await _TerminalDal.Get(p => p.TerminalId == TerminalId));
        }

        public async Task<IDataResult<List<Terminal>>> GetList()
        {
            var u = (await _TerminalDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<Terminal>>(u);
        }

        public IResult Update(Terminal Terminal)
        {
            _TerminalDal.Update(Terminal);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
