/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:CountryManager.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Concrete\App\CountryManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class CountryManager : ICountryService
    {
        private ICountryDal _CountryDal;

        public CountryManager(ICountryDal CountryDal)
        {
            _CountryDal = CountryDal;
        }

        public IResult Add(Country Country)
        {
            //Business codes
            _CountryDal.Add(Country);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(Country Country)
        {
            _CountryDal.Delete(Country);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<Country>> GetById(int CountryId)
        {
            return new SuccessDataResult<Country>( await _CountryDal.Get(p => p.CountryId == CountryId));
        }

        public async Task<IDataResult<List<Country>>> GetList()
        {
            var u = (await _CountryDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<Country>>(u);
        }

        public IResult Update(Country Country)
        {
            _CountryDal.Update(Country);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
