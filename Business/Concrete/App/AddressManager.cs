/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:AddressManager.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Concrete\App\AddressManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class AddressManager : IAddressService
    {
        private IAddressDal _AddressDal;

        public AddressManager(IAddressDal AddressDal)
        {
            _AddressDal = AddressDal;
        }

        public IResult Add(Address Address)
        {
            //Business codes
            _AddressDal.Add(Address);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(Address Address)
        {
            _AddressDal.Delete(Address);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<Address>> GetById(int AddressId)
        {
            return new SuccessDataResult<Address>( await _AddressDal.Get(p => p.AddressId == AddressId));
        }

        public async Task<IDataResult<List<Address>>> GetList()
        {
            var u = (await _AddressDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<Address>>(u);
        }

        public IResult Update(Address Address)
        {
            _AddressDal.Update(Address);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
