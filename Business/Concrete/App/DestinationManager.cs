/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:DestinationManager.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Concrete\App\DestinationManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class DestinationManager : IDestinationService
    {
        private IDestinationDal _DestinationDal;

        public DestinationManager(IDestinationDal DestinationDal)
        {
            _DestinationDal = DestinationDal;
        }

        public IResult Add(Destination Destination)
        {
            //Business codes
            _DestinationDal.Add(Destination);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(Destination Destination)
        {
            _DestinationDal.Delete(Destination);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<Destination>> GetById(int DestinationId)
        {
            return new SuccessDataResult<Destination>( await _DestinationDal.Get(p => p.DestinationId == DestinationId));
        }

        public async Task<IDataResult<List<Destination>>> GetList()
        {
            var u = (await _DestinationDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<Destination>>(u);
        }

        public IResult Update(Destination Destination)
        {
            _DestinationDal.Update(Destination);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
