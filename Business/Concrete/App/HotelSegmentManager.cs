/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:HotelSegmentManager.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Concrete\App\HotelSegmentManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class HotelSegmentManager : IHotelSegmentService
    {
        private IHotelSegmentDal _HotelSegmentDal;

        public HotelSegmentManager(IHotelSegmentDal HotelSegmentDal)
        {
            _HotelSegmentDal = HotelSegmentDal;
        }

        public IResult Add(HotelSegment HotelSegment)
        {
            //Business codes
            _HotelSegmentDal.Add(HotelSegment);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(HotelSegment HotelSegment)
        {
            _HotelSegmentDal.Delete(HotelSegment);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<HotelSegment>> GetById(int HotelSegmentId)
        {
            return new SuccessDataResult<HotelSegment>( await _HotelSegmentDal.Get(p => p.HotelSegmentId == HotelSegmentId));
        }

        public async Task<IDataResult<List<HotelSegment>>> GetList()
        {
            var u = (await _HotelSegmentDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<HotelSegment>>(u);
        }

        public IResult Update(HotelSegment HotelSegment)
        {
            _HotelSegmentDal.Update(HotelSegment);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
