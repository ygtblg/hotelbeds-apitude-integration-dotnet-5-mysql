/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:ZoneManager.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Concrete\App\ZoneManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class ZoneManager : IZoneService
    {
        private IZoneDal _ZoneDal;

        public ZoneManager(IZoneDal ZoneDal)
        {
            _ZoneDal = ZoneDal;
        }

        public IResult Add(Zone Zone)
        {
            //Business codes
            _ZoneDal.Add(Zone);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(Zone Zone)
        {
            _ZoneDal.Delete(Zone);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<Zone>> GetById(int ZoneId)
        {
            return new SuccessDataResult<Zone>( await _ZoneDal.Get(p => p.ZoneId == ZoneId));
        }

        public async Task<IDataResult<List<Zone>>> GetList()
        {
            var u = (await _ZoneDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<Zone>>(u);
        }

        public IResult Update(Zone Zone)
        {
            _ZoneDal.Update(Zone);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
