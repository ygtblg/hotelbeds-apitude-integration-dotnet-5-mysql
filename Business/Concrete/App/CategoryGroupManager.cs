/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:CategoryGroupManager.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Concrete\App\CategoryGroupManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class CategoryGroupManager : ICategoryGroupService
    {
        private ICategoryGroupDal _CategoryGroupDal;

        public CategoryGroupManager(ICategoryGroupDal CategoryGroupDal)
        {
            _CategoryGroupDal = CategoryGroupDal;
        }

        public IResult Add(CategoryGroup CategoryGroup)
        {
            //Business codes
            _CategoryGroupDal.Add(CategoryGroup);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(CategoryGroup CategoryGroup)
        {
            _CategoryGroupDal.Delete(CategoryGroup);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<CategoryGroup>> GetById(int CategoryGroupId)
        {
            return new SuccessDataResult<CategoryGroup>( await _CategoryGroupDal.Get(p => p.CategoryGroupId == CategoryGroupId));
        }

        public async Task<IDataResult<List<CategoryGroup>>> GetList()
        {
            var u = (await _CategoryGroupDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<CategoryGroup>>(u);
        }

        public IResult Update(CategoryGroup CategoryGroup)
        {
            _CategoryGroupDal.Update(CategoryGroup);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
