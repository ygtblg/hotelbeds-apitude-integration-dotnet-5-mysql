/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:HotelRoomDescriptionManager.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Concrete\App\HotelRoomDescriptionManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class HotelRoomDescriptionManager : IHotelRoomDescriptionService
    {
        private IHotelRoomDescriptionDal _HotelRoomDescriptionDal;

        public HotelRoomDescriptionManager(IHotelRoomDescriptionDal HotelRoomDescriptionDal)
        {
            _HotelRoomDescriptionDal = HotelRoomDescriptionDal;
        }

        public IResult Add(HotelRoomDescription HotelRoomDescription)
        {
            //Business codes
            _HotelRoomDescriptionDal.Add(HotelRoomDescription);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(HotelRoomDescription HotelRoomDescription)
        {
            _HotelRoomDescriptionDal.Delete(HotelRoomDescription);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<HotelRoomDescription>> GetById(int HotelRoomDescriptionId)
        {
            return new SuccessDataResult<HotelRoomDescription>( await _HotelRoomDescriptionDal.Get(p => p.HotelRoomDescriptionId == HotelRoomDescriptionId));
        }

        public async Task<IDataResult<List<HotelRoomDescription>>> GetList()
        {
            var u = (await _HotelRoomDescriptionDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<HotelRoomDescription>>(u);
        }

        public IResult Update(HotelRoomDescription HotelRoomDescription)
        {
            _HotelRoomDescriptionDal.Update(HotelRoomDescription);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
