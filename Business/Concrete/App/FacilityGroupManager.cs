/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:FacilityGroupManager.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Concrete\App\FacilityGroupManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class FacilityGroupManager : IFacilityGroupService
    {
        private IFacilityGroupDal _FacilityGroupDal;

        public FacilityGroupManager(IFacilityGroupDal FacilityGroupDal)
        {
            _FacilityGroupDal = FacilityGroupDal;
        }

        public IResult Add(FacilityGroup FacilityGroup)
        {
            //Business codes
            _FacilityGroupDal.Add(FacilityGroup);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(FacilityGroup FacilityGroup)
        {
            _FacilityGroupDal.Delete(FacilityGroup);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<FacilityGroup>> GetById(int FacilityGroupId)
        {
            return new SuccessDataResult<FacilityGroup>( await _FacilityGroupDal.Get(p => p.FacilityGroupId == FacilityGroupId));
        }

        public async Task<IDataResult<List<FacilityGroup>>> GetList()
        {
            var u = (await _FacilityGroupDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<FacilityGroup>>(u);
        }

        public IResult Update(FacilityGroup FacilityGroup)
        {
            _FacilityGroupDal.Update(FacilityGroup);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
