/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:HotelManager.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Concrete\App\HotelManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class HotelManager : IHotelService
    {
        private IHotelDal _HotelDal;

        public HotelManager(IHotelDal HotelDal)
        {
            _HotelDal = HotelDal;
        }

        public IResult Add(Hotel Hotel)
        {
            //Business codes
            _HotelDal.Add(Hotel);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(Hotel Hotel)
        {
            _HotelDal.Delete(Hotel);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<Hotel>> GetById(int HotelId)
        {
            return new SuccessDataResult<Hotel>( await _HotelDal.Get(p => p.HotelId == HotelId));
        }

        public async Task<IDataResult<List<Hotel>>> GetList()
        {
            var u = (await _HotelDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<Hotel>>(u);
        }

        public IResult Update(Hotel Hotel)
        {
            _HotelDal.Update(Hotel);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
