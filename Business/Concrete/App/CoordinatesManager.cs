/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:CoordinatesManager.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Concrete\App\CoordinatesManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class CoordinatesManager : ICoordinatesService
    {
        private ICoordinatesDal _CoordinatesDal;

        public CoordinatesManager(ICoordinatesDal CoordinatesDal)
        {
            _CoordinatesDal = CoordinatesDal;
        }

        public IResult Add(Coordinates Coordinates)
        {
            //Business codes
            _CoordinatesDal.Add(Coordinates);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(Coordinates Coordinates)
        {
            _CoordinatesDal.Delete(Coordinates);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<Coordinates>> GetById(int CoordinatesId)
        {
            return new SuccessDataResult<Coordinates>( await _CoordinatesDal.Get(p => p.CoordinatesId == CoordinatesId));
        }

        public async Task<IDataResult<List<Coordinates>>> GetList()
        {
            var u = (await _CoordinatesDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<Coordinates>>(u);
        }

        public IResult Update(Coordinates Coordinates)
        {
            _CoordinatesDal.Update(Coordinates);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
