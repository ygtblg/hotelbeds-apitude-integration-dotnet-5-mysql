/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:RoomDataManager.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Concrete\App\RoomDataManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class RoomDataManager : IRoomDataService
    {
        private IRoomDataDal _RoomDataDal;

        public RoomDataManager(IRoomDataDal RoomDataDal)
        {
            _RoomDataDal = RoomDataDal;
        }

        public IResult Add(RoomData RoomData)
        {
            //Business codes
            _RoomDataDal.Add(RoomData);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(RoomData RoomData)
        {
            _RoomDataDal.Delete(RoomData);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<RoomData>> GetById(int RoomDataId)
        {
            return new SuccessDataResult<RoomData>( await _RoomDataDal.Get(p => p.RoomDataId == RoomDataId));
        }

        public async Task<IDataResult<List<RoomData>>> GetList()
        {
            var u = (await _RoomDataDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<RoomData>>(u);
        }

        public IResult Update(RoomData RoomData)
        {
            _RoomDataDal.Update(RoomData);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
