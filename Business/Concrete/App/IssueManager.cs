/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:IssueManager.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Concrete\App\IssueManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class IssueManager : IIssueService
    {
        private IIssueDal _IssueDal;

        public IssueManager(IIssueDal IssueDal)
        {
            _IssueDal = IssueDal;
        }

        public IResult Add(Issue Issue)
        {
            //Business codes
            _IssueDal.Add(Issue);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(Issue Issue)
        {
            _IssueDal.Delete(Issue);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<Issue>> GetById(int IssueId)
        {
            return new SuccessDataResult<Issue>( await _IssueDal.Get(p => p.IssueId == IssueId));
        }

        public async Task<IDataResult<List<Issue>>> GetList()
        {
            var u = (await _IssueDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<Issue>>(u);
        }

        public IResult Update(Issue Issue)
        {
            _IssueDal.Update(Issue);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
