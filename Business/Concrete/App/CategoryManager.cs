/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:CategoryManager.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Concrete\App\CategoryManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class CategoryManager : ICategoryService
    {
        private ICategoryDal _CategoryDal;

        public CategoryManager(ICategoryDal CategoryDal)
        {
            _CategoryDal = CategoryDal;
        }

        public IResult Add(Category Category)
        {
            //Business codes
            _CategoryDal.Add(Category);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(Category Category)
        {
            _CategoryDal.Delete(Category);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<Category>> GetById(int CategoryId)
        {
            return new SuccessDataResult<Category>( await _CategoryDal.Get(p => p.CategoryId == CategoryId));
        }

        public async Task<IDataResult<List<Category>>> GetList()
        {
            var u = (await _CategoryDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<Category>>(u);
        }

        public IResult Update(Category Category)
        {
            _CategoryDal.Update(Category);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
