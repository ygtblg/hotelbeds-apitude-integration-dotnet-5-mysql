/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:DescriptionManager.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Concrete\App\DescriptionManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class DescriptionManager : IDescriptionService
    {
        private IDescriptionDal _DescriptionDal;

        public DescriptionManager(IDescriptionDal DescriptionDal)
        {
            _DescriptionDal = DescriptionDal;
        }

        public IResult Add(Description Description)
        {
            //Business codes
            _DescriptionDal.Add(Description);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(Description Description)
        {
            _DescriptionDal.Delete(Description);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<Description>> GetById(int DescriptionId)
        {
            return new SuccessDataResult<Description>( await _DescriptionDal.Get(p => p.DescriptionId == DescriptionId));
        }

        public async Task<IDataResult<List<Description>>> GetList()
        {
            var u = (await _DescriptionDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<Description>>(u);
        }

        public IResult Update(Description Description)
        {
            _DescriptionDal.Update(Description);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
