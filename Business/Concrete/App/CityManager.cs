/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:CityManager.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Concrete\App\CityManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class CityManager : ICityService
    {
        private ICityDal _CityDal;

        public CityManager(ICityDal CityDal)
        {
            _CityDal = CityDal;
        }

        public IResult Add(City City)
        {
            //Business codes
            _CityDal.Add(City);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(City City)
        {
            _CityDal.Delete(City);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<City>> GetById(int CityId)
        {
            return new SuccessDataResult<City>( await _CityDal.Get(p => p.CityId == CityId));
        }

        public async Task<IDataResult<List<City>>> GetList()
        {
            var u = (await _CityDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<City>>(u);
        }

        public IResult Update(City City)
        {
            _CityDal.Update(City);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
