/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:FacilityTypologyManager.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Concrete\App\FacilityTypologyManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class FacilityTypologyManager : IFacilityTypologyService
    {
        private IFacilityTypologyDal _FacilityTypologyDal;

        public FacilityTypologyManager(IFacilityTypologyDal FacilityTypologyDal)
        {
            _FacilityTypologyDal = FacilityTypologyDal;
        }

        public IResult Add(FacilityTypology FacilityTypology)
        {
            //Business codes
            _FacilityTypologyDal.Add(FacilityTypology);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(FacilityTypology FacilityTypology)
        {
            _FacilityTypologyDal.Delete(FacilityTypology);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<FacilityTypology>> GetById(int FacilityTypologyId)
        {
            return new SuccessDataResult<FacilityTypology>( await _FacilityTypologyDal.Get(p => p.FacilityTypologyId == FacilityTypologyId));
        }

        public async Task<IDataResult<List<FacilityTypology>>> GetList()
        {
            var u = (await _FacilityTypologyDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<FacilityTypology>>(u);
        }

        public IResult Update(FacilityTypology FacilityTypology)
        {
            _FacilityTypologyDal.Update(FacilityTypology);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
