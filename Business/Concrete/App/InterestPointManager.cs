/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:InterestPointManager.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Concrete\App\InterestPointManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class InterestPointManager : IInterestPointService
    {
        private IInterestPointDal _InterestPointDal;

        public InterestPointManager(IInterestPointDal InterestPointDal)
        {
            _InterestPointDal = InterestPointDal;
        }

        public IResult Add(InterestPoint InterestPoint)
        {
            //Business codes
            _InterestPointDal.Add(InterestPoint);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(InterestPoint InterestPoint)
        {
            _InterestPointDal.Delete(InterestPoint);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<InterestPoint>> GetById(int InterestPointId)
        {
            return new SuccessDataResult<InterestPoint>( await _InterestPointDal.Get(p => p.InterestPointId == InterestPointId));
        }

        public async Task<IDataResult<List<InterestPoint>>> GetList()
        {
            var u = (await _InterestPointDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<InterestPoint>>(u);
        }

        public IResult Update(InterestPoint InterestPoint)
        {
            _InterestPointDal.Update(InterestPoint);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
