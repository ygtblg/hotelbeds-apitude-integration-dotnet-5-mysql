/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:TerminalDataManager.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Concrete\App\TerminalDataManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class TerminalDataManager : ITerminalDataService
    {
        private ITerminalDataDal _TerminalDataDal;

        public TerminalDataManager(ITerminalDataDal TerminalDataDal)
        {
            _TerminalDataDal = TerminalDataDal;
        }

        public IResult Add(TerminalData TerminalData)
        {
            //Business codes
            _TerminalDataDal.Add(TerminalData);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(TerminalData TerminalData)
        {
            _TerminalDataDal.Delete(TerminalData);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<TerminalData>> GetById(int TerminalDataId)
        {
            return new SuccessDataResult<TerminalData>( await _TerminalDataDal.Get(p => p.TerminalDataId == TerminalDataId));
        }

        public async Task<IDataResult<List<TerminalData>>> GetList()
        {
            var u = (await _TerminalDataDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<TerminalData>>(u);
        }

        public IResult Update(TerminalData TerminalData)
        {
            _TerminalDataDal.Update(TerminalData);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
