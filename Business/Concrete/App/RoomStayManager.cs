/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:RoomStayManager.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Concrete\App\RoomStayManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class RoomStayManager : IRoomStayService
    {
        private IRoomStayDal _RoomStayDal;

        public RoomStayManager(IRoomStayDal RoomStayDal)
        {
            _RoomStayDal = RoomStayDal;
        }

        public IResult Add(RoomStay RoomStay)
        {
            //Business codes
            _RoomStayDal.Add(RoomStay);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(RoomStay RoomStay)
        {
            _RoomStayDal.Delete(RoomStay);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<RoomStay>> GetById(int RoomStayId)
        {
            return new SuccessDataResult<RoomStay>( await _RoomStayDal.Get(p => p.RoomStayId == RoomStayId));
        }

        public async Task<IDataResult<List<RoomStay>>> GetList()
        {
            var u = (await _RoomStayDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<RoomStay>>(u);
        }

        public IResult Update(RoomStay RoomStay)
        {
            _RoomStayDal.Update(RoomStay);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
