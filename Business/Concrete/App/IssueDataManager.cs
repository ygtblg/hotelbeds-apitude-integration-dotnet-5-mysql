/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:IssueDataManager.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Concrete\App\IssueDataManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class IssueDataManager : IIssueDataService
    {
        private IIssueDataDal _IssueDataDal;

        public IssueDataManager(IIssueDataDal IssueDataDal)
        {
            _IssueDataDal = IssueDataDal;
        }

        public IResult Add(IssueData IssueData)
        {
            //Business codes
            _IssueDataDal.Add(IssueData);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(IssueData IssueData)
        {
            _IssueDataDal.Delete(IssueData);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<IssueData>> GetById(int IssueDataId)
        {
            return new SuccessDataResult<IssueData>( await _IssueDataDal.Get(p => p.IssueDataId == IssueDataId));
        }

        public async Task<IDataResult<List<IssueData>>> GetList()
        {
            var u = (await _IssueDataDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<IssueData>>(u);
        }

        public IResult Update(IssueData IssueData)
        {
            _IssueDataDal.Update(IssueData);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
