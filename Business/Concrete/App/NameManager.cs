/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:NameManager.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Concrete\App\NameManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class NameManager : INameService
    {
        private INameDal _NameDal;

        public NameManager(INameDal NameDal)
        {
            _NameDal = NameDal;
        }

        public IResult Add(Name Name)
        {
            //Business codes
            _NameDal.Add(Name);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(Name Name)
        {
            _NameDal.Delete(Name);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<Name>> GetById(int NameId)
        {
            return new SuccessDataResult<Name>( await _NameDal.Get(p => p.NameId == NameId));
        }

        public async Task<IDataResult<List<Name>>> GetList()
        {
            var u = (await _NameDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<Name>>(u);
        }

        public IResult Update(Name Name)
        {
            _NameDal.Update(Name);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
