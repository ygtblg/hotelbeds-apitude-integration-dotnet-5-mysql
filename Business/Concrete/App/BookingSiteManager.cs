/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:BookingSiteManager.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Concrete\App\BookingSiteManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class BookingSiteManager : IBookingSiteService
    {
        private IBookingSiteDal _BookingSiteDal;

        public BookingSiteManager(IBookingSiteDal BookingSiteDal)
        {
            _BookingSiteDal = BookingSiteDal;
        }

        public IResult Add(BookingSite BookingSite)
        {
            //Business codes
            _BookingSiteDal.Add(BookingSite);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(BookingSite BookingSite)
        {
            _BookingSiteDal.Delete(BookingSite);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<BookingSite>> GetById(int BookingSiteId)
        {
            return new SuccessDataResult<BookingSite>( await _BookingSiteDal.Get(p => p.BookingSiteId == BookingSiteId));
        }

        public async Task<IDataResult<List<BookingSite>>> GetList()
        {
            var u = (await _BookingSiteDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<BookingSite>>(u);
        }

        public IResult Update(BookingSite BookingSite)
        {
            _BookingSiteDal.Update(BookingSite);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
