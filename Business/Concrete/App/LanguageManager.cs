/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:LanguageManager.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Concrete\App\LanguageManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class LanguageManager : ILanguageService
    {
        private ILanguageDal _LanguageDal;

        public LanguageManager(ILanguageDal LanguageDal)
        {
            _LanguageDal = LanguageDal;
        }

        public IResult Add(Language Language)
        {
            //Business codes
            _LanguageDal.Add(Language);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(Language Language)
        {
            _LanguageDal.Delete(Language);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<Language>> GetById(int LanguageId)
        {
            return new SuccessDataResult<Language>( await _LanguageDal.Get(p => p.LanguageId == LanguageId));
        }

        public async Task<IDataResult<List<Language>>> GetList()
        {
            var u = (await _LanguageDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<Language>>(u);
        }

        public IResult Update(Language Language)
        {
            _LanguageDal.Update(Language);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
