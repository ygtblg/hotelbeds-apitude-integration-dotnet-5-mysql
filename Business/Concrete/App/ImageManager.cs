/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:ImageManager.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Concrete\App\ImageManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class ImageManager : IImageService
    {
        private IImageDal _ImageDal;

        public ImageManager(IImageDal ImageDal)
        {
            _ImageDal = ImageDal;
        }

        public IResult Add(Image Image)
        {
            //Business codes
            _ImageDal.Add(Image);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(Image Image)
        {
            _ImageDal.Delete(Image);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<Image>> GetById(int ImageId)
        {
            return new SuccessDataResult<Image>( await _ImageDal.Get(p => p.ImageId == ImageId));
        }

        public async Task<IDataResult<List<Image>>> GetList()
        {
            var u = (await _ImageDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<Image>>(u);
        }

        public IResult Update(Image Image)
        {
            _ImageDal.Update(Image);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
