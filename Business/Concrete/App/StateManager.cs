/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:StateManager.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Concrete\App\StateManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class StateManager : IStateService
    {
        private IStateDal _StateDal;

        public StateManager(IStateDal StateDal)
        {
            _StateDal = StateDal;
        }

        public IResult Add(State State)
        {
            //Business codes
            _StateDal.Add(State);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(State State)
        {
            _StateDal.Delete(State);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<State>> GetById(int StateId)
        {
            return new SuccessDataResult<State>( await _StateDal.Get(p => p.StateId == StateId));
        }

        public async Task<IDataResult<List<State>>> GetList()
        {
            var u = (await _StateDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<State>>(u);
        }

        public IResult Update(State State)
        {
            _StateDal.Update(State);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
