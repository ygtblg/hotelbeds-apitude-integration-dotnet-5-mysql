/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:CharacteristicDescriptionManager.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Concrete\App\CharacteristicDescriptionManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class CharacteristicDescriptionManager : ICharacteristicDescriptionService
    {
        private ICharacteristicDescriptionDal _CharacteristicDescriptionDal;

        public CharacteristicDescriptionManager(ICharacteristicDescriptionDal CharacteristicDescriptionDal)
        {
            _CharacteristicDescriptionDal = CharacteristicDescriptionDal;
        }

        public IResult Add(CharacteristicDescription CharacteristicDescription)
        {
            //Business codes
            _CharacteristicDescriptionDal.Add(CharacteristicDescription);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(CharacteristicDescription CharacteristicDescription)
        {
            _CharacteristicDescriptionDal.Delete(CharacteristicDescription);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<CharacteristicDescription>> GetById(int CharacteristicDescriptionId)
        {
            return new SuccessDataResult<CharacteristicDescription>( await _CharacteristicDescriptionDal.Get(p => p.CharacteristicDescriptionId == CharacteristicDescriptionId));
        }

        public async Task<IDataResult<List<CharacteristicDescription>>> GetList()
        {
            var u = (await _CharacteristicDescriptionDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<CharacteristicDescription>>(u);
        }

        public IResult Update(CharacteristicDescription CharacteristicDescription)
        {
            _CharacteristicDescriptionDal.Update(CharacteristicDescription);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
