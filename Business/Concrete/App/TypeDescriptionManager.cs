/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:TypeDescriptionManager.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Concrete\App\TypeDescriptionManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class TypeDescriptionManager : ITypeDescriptionService
    {
        private ITypeDescriptionDal _TypeDescriptionDal;

        public TypeDescriptionManager(ITypeDescriptionDal TypeDescriptionDal)
        {
            _TypeDescriptionDal = TypeDescriptionDal;
        }

        public IResult Add(TypeDescription TypeDescription)
        {
            //Business codes
            _TypeDescriptionDal.Add(TypeDescription);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(TypeDescription TypeDescription)
        {
            _TypeDescriptionDal.Delete(TypeDescription);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<TypeDescription>> GetById(int TypeDescriptionId)
        {
            return new SuccessDataResult<TypeDescription>( await _TypeDescriptionDal.Get(p => p.TypeDescriptionId == TypeDescriptionId));
        }

        public async Task<IDataResult<List<TypeDescription>>> GetList()
        {
            var u = (await _TypeDescriptionDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<TypeDescription>>(u);
        }

        public IResult Update(TypeDescription TypeDescription)
        {
            _TypeDescriptionDal.Update(TypeDescription);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
