/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:RoomManager.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Concrete\App\RoomManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class RoomManager : IRoomService
    {
        private IRoomDal _RoomDal;

        public RoomManager(IRoomDal RoomDal)
        {
            _RoomDal = RoomDal;
        }

        public IResult Add(Room Room)
        {
            //Business codes
            _RoomDal.Add(Room);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(Room Room)
        {
            _RoomDal.Delete(Room);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<Room>> GetById(int RoomId)
        {
            return new SuccessDataResult<Room>( await _RoomDal.Get(p => p.RoomId == RoomId));
        }

        public async Task<IDataResult<List<Room>>> GetList()
        {
            var u = (await _RoomDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<Room>>(u);
        }

        public IResult Update(Room Room)
        {
            _RoomDal.Update(Room);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
