/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:PhoneManager.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Concrete\App\PhoneManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class PhoneManager : IPhoneService
    {
        private IPhoneDal _PhoneDal;

        public PhoneManager(IPhoneDal PhoneDal)
        {
            _PhoneDal = PhoneDal;
        }

        public IResult Add(Phone Phone)
        {
            //Business codes
            _PhoneDal.Add(Phone);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(Phone Phone)
        {
            _PhoneDal.Delete(Phone);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<Phone>> GetById(int PhoneId)
        {
            return new SuccessDataResult<Phone>( await _PhoneDal.Get(p => p.PhoneId == PhoneId));
        }

        public async Task<IDataResult<List<Phone>>> GetList()
        {
            var u = (await _PhoneDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<Phone>>(u);
        }

        public IResult Update(Phone Phone)
        {
            _PhoneDal.Update(Phone);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
