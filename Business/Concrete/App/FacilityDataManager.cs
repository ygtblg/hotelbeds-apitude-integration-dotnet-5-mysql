/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:FacilityDataManager.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Concrete\App\FacilityDataManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class FacilityDataManager : IFacilityDataService
    {
        private IFacilityDataDal _FacilityDataDal;

        public FacilityDataManager(IFacilityDataDal FacilityDataDal)
        {
            _FacilityDataDal = FacilityDataDal;
        }

        public IResult Add(FacilityData FacilityData)
        {
            //Business codes
            _FacilityDataDal.Add(FacilityData);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(FacilityData FacilityData)
        {
            _FacilityDataDal.Delete(FacilityData);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<FacilityData>> GetById(int FacilityDataId)
        {
            return new SuccessDataResult<FacilityData>( await _FacilityDataDal.Get(p => p.FacilityDataId == FacilityDataId));
        }

        public async Task<IDataResult<List<FacilityData>>> GetList()
        {
            var u = (await _FacilityDataDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<FacilityData>>(u);
        }

        public IResult Update(FacilityData FacilityData)
        {
            _FacilityDataDal.Update(FacilityData);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
