/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:ImageTypeManager.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Concrete\App\ImageTypeManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class ImageTypeManager : IImageTypeService
    {
        private IImageTypeDal _ImageTypeDal;

        public ImageTypeManager(IImageTypeDal ImageTypeDal)
        {
            _ImageTypeDal = ImageTypeDal;
        }

        public IResult Add(ImageType ImageType)
        {
            //Business codes
            _ImageTypeDal.Add(ImageType);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(ImageType ImageType)
        {
            _ImageTypeDal.Delete(ImageType);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<ImageType>> GetById(int ImageTypeId)
        {
            return new SuccessDataResult<ImageType>( await _ImageTypeDal.Get(p => p.ImageTypeId == ImageTypeId));
        }

        public async Task<IDataResult<List<ImageType>>> GetList()
        {
            var u = (await _ImageTypeDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<ImageType>>(u);
        }

        public IResult Update(ImageType ImageType)
        {
            _ImageTypeDal.Update(ImageType);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
