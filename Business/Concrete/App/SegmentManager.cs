/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:SegmentManager.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Concrete\App\SegmentManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class SegmentManager : ISegmentService
    {
        private ISegmentDal _SegmentDal;

        public SegmentManager(ISegmentDal SegmentDal)
        {
            _SegmentDal = SegmentDal;
        }

        public IResult Add(Segment Segment)
        {
            //Business codes
            _SegmentDal.Add(Segment);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(Segment Segment)
        {
            _SegmentDal.Delete(Segment);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<Segment>> GetById(int SegmentId)
        {
            return new SuccessDataResult<Segment>( await _SegmentDal.Get(p => p.SegmentId == SegmentId));
        }

        public async Task<IDataResult<List<Segment>>> GetList()
        {
            var u = (await _SegmentDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<Segment>>(u);
        }

        public IResult Update(Segment Segment)
        {
            _SegmentDal.Update(Segment);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
