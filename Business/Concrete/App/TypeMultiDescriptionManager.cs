/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:TypeMultiDescriptionManager.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Concrete\App\TypeMultiDescriptionManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class TypeMultiDescriptionManager : ITypeMultiDescriptionService
    {
        private ITypeMultiDescriptionDal _TypeMultiDescriptionDal;

        public TypeMultiDescriptionManager(ITypeMultiDescriptionDal TypeMultiDescriptionDal)
        {
            _TypeMultiDescriptionDal = TypeMultiDescriptionDal;
        }

        public IResult Add(TypeMultiDescription TypeMultiDescription)
        {
            //Business codes
            _TypeMultiDescriptionDal.Add(TypeMultiDescription);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(TypeMultiDescription TypeMultiDescription)
        {
            _TypeMultiDescriptionDal.Delete(TypeMultiDescription);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<TypeMultiDescription>> GetById(int TypeMultiDescriptionId)
        {
            return new SuccessDataResult<TypeMultiDescription>( await _TypeMultiDescriptionDal.Get(p => p.TypeMultiDescriptionId == TypeMultiDescriptionId));
        }

        public async Task<IDataResult<List<TypeMultiDescription>>> GetList()
        {
            var u = (await _TypeMultiDescriptionDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<TypeMultiDescription>>(u);
        }

        public IResult Update(TypeMultiDescription TypeMultiDescription)
        {
            _TypeMultiDescriptionDal.Update(TypeMultiDescription);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
