/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:FacilityManager.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Concrete\App\FacilityManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class FacilityManager : IFacilityService
    {
        private IFacilityDal _FacilityDal;

        public FacilityManager(IFacilityDal FacilityDal)
        {
            _FacilityDal = FacilityDal;
        }

        public IResult Add(Facility Facility)
        {
            //Business codes
            _FacilityDal.Add(Facility);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(Facility Facility)
        {
            _FacilityDal.Delete(Facility);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<Facility>> GetById(int FacilityId)
        {
            return new SuccessDataResult<Facility>( await _FacilityDal.Get(p => p.FacilityId == FacilityId));
        }

        public async Task<IDataResult<List<Facility>>> GetList()
        {
            var u = (await _FacilityDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<Facility>>(u);
        }

        public IResult Update(Facility Facility)
        {
            _FacilityDal.Update(Facility);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
