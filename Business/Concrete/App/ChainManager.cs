/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:ChainManager.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Concrete\App\ChainManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class ChainManager : IChainService
    {
        private IChainDal _ChainDal;

        public ChainManager(IChainDal ChainDal)
        {
            _ChainDal = ChainDal;
        }

        public IResult Add(Chain Chain)
        {
            //Business codes
            _ChainDal.Add(Chain);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(Chain Chain)
        {
            _ChainDal.Delete(Chain);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<Chain>> GetById(int ChainId)
        {
            return new SuccessDataResult<Chain>( await _ChainDal.Get(p => p.ChainId == ChainId));
        }

        public async Task<IDataResult<List<Chain>>> GetList()
        {
            var u = (await _ChainDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<Chain>>(u);
        }

        public IResult Update(Chain Chain)
        {
            _ChainDal.Update(Chain);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
