/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:HotelBoardManager.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Concrete\App\HotelBoardManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class HotelBoardManager : IHotelBoardService
    {
        private IHotelBoardDal _HotelBoardDal;

        public HotelBoardManager(IHotelBoardDal HotelBoardDal)
        {
            _HotelBoardDal = HotelBoardDal;
        }

        public IResult Add(HotelBoard HotelBoard)
        {
            //Business codes
            _HotelBoardDal.Add(HotelBoard);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(HotelBoard HotelBoard)
        {
            _HotelBoardDal.Delete(HotelBoard);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<HotelBoard>> GetById(int HotelBoardId)
        {
            return new SuccessDataResult<HotelBoard>( await _HotelBoardDal.Get(p => p.HotelBoardId == HotelBoardId));
        }

        public async Task<IDataResult<List<HotelBoard>>> GetList()
        {
            var u = (await _HotelBoardDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<HotelBoard>>(u);
        }

        public IResult Update(HotelBoard HotelBoard)
        {
            _HotelBoardDal.Update(HotelBoard);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
