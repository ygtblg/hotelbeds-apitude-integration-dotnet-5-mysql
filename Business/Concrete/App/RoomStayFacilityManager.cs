/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:RoomStayFacilityManager.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Concrete\App\RoomStayFacilityManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class RoomStayFacilityManager : IRoomStayFacilityService
    {
        private IRoomStayFacilityDal _RoomStayFacilityDal;

        public RoomStayFacilityManager(IRoomStayFacilityDal RoomStayFacilityDal)
        {
            _RoomStayFacilityDal = RoomStayFacilityDal;
        }

        public IResult Add(RoomStayFacility RoomStayFacility)
        {
            //Business codes
            _RoomStayFacilityDal.Add(RoomStayFacility);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(RoomStayFacility RoomStayFacility)
        {
            _RoomStayFacilityDal.Delete(RoomStayFacility);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<RoomStayFacility>> GetById(int RoomStayFacilityId)
        {
            return new SuccessDataResult<RoomStayFacility>( await _RoomStayFacilityDal.Get(p => p.RoomStayFacilityId == RoomStayFacilityId));
        }

        public async Task<IDataResult<List<RoomStayFacility>>> GetList()
        {
            var u = (await _RoomStayFacilityDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<RoomStayFacility>>(u);
        }

        public IResult Update(RoomStayFacility RoomStayFacility)
        {
            _RoomStayFacilityDal.Update(RoomStayFacility);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
