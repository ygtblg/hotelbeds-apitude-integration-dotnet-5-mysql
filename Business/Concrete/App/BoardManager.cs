/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:BoardManager.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Concrete\App\BoardManager.cs
*/

using System.Collections.Generic;
using Business.Abstract;
using Business.Constants;
using Core.Utilities.Results;
using DataAccess.Abstract;
using Entities.Concrete;
using System.Linq;
using System.Threading.Tasks;

namespace Business.Concrete
{
    public class BoardManager : IBoardService
    {
        private IBoardDal _BoardDal;

        public BoardManager(IBoardDal BoardDal)
        {
            _BoardDal = BoardDal;
        }

        public IResult Add(Board Board)
        {
            //Business codes
            _BoardDal.Add(Board);
            return new SuccessResult(Messages.ProductAdded);
        }

        public IResult Delete(Board Board)
        {
            _BoardDal.Delete(Board);
            return new SuccessResult(Messages.ProductDeleted);
        }

        public async Task<IDataResult<Board>> GetById(int BoardId)
        {
            return new SuccessDataResult<Board>( await _BoardDal.Get(p => p.BoardId == BoardId));
        }

        public async Task<IDataResult<List<Board>>> GetList()
        {
            var u = (await _BoardDal.getAllLevels()).ToList();
            return new SuccessDataResult<List<Board>>(u);
        }

        public IResult Update(Board Board)
        {
            _BoardDal.Update(Board);
            return new SuccessResult(Messages.ProductUpdated);
        }
    }
}
