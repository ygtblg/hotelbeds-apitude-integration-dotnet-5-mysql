﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Text;
using Autofac.Extras.DynamicProxy;
using Business.Abstract;
using Business.Concrete;
using Castle.DynamicProxy;
using Core.Utilities.Interceptors;
using Core.Utilities.Security.Jwt;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework;
namespace Business.DependencyResolvers.Autofac
{
    public class AutofacBusinessModule:Module
    {
        protected override void Load(ContainerBuilder builder)
        {
 
 
            builder.RegisterType<UserManager>().As<IUserService>();
            builder.RegisterType<EfUserDal>().As<IUserDal>();
            builder.RegisterType<AuthManager>().As<IAuthService>();
            builder.RegisterType<JwtHelper>().As<ITokenHelper>();
               builder.RegisterType<LayoutManager>().As<ILayoutService>();
            builder.RegisterType<EfLayoutDal>().As<ILayoutDal>();
            builder.RegisterType<BookCategoryManager>().As<IBookCategoryService>();
            builder.RegisterType<EfBookCategoryDal>().As<IBookCategoryDal>();
            builder.RegisterType<BookManager>().As<IBookService>();
            builder.RegisterType<EfBookDal>().As<IBookDal>();
            builder.RegisterType<BookDetailManager>().As<IBookDetailService>();
            builder.RegisterType<EfBookDetailDal>().As<IBookDetailDal>();
            builder.RegisterType<BookPublisherManager>().As<IBookPublisherService>();
            builder.RegisterType<EfBookPublisherDal>().As<IBookPublisherDal>();
            builder.RegisterType<LayoutParamManager>().As<ILayoutParamService>();
            builder.RegisterType<EfLayoutParamDal>().As<ILayoutParamDal>();
            builder.RegisterType<PublisherManager>().As<IPublisherService>();
            builder.RegisterType<EfPublisherDal>().As<IPublisherDal>();  
            
            // AutofacBusinessModuleAddingsStart
         
            builder.RegisterType<BookingSiteManager>().As<IBookingSiteService>();
            builder.RegisterType<EfBookingSiteDal>().As<IBookingSiteDal>();
            builder.RegisterType<BoardManager>().As<IBoardService>();
            builder.RegisterType<EfBoardDal>().As<IBoardDal>();
            builder.RegisterType<DescriptionManager>().As<IDescriptionService>();
            builder.RegisterType<EfDescriptionDal>().As<IDescriptionDal>();
            builder.RegisterType<HotelBoardManager>().As<IHotelBoardService>();
            builder.RegisterType<EfHotelBoardDal>().As<IHotelBoardDal>();
            builder.RegisterType<HotelManager>().As<IHotelService>();
            builder.RegisterType<EfHotelDal>().As<IHotelDal>();
            builder.RegisterType<AccommodationManager>().As<IAccommodationService>();
            builder.RegisterType<EfAccommodationDal>().As<IAccommodationDal>();
            builder.RegisterType<TypeMultiDescriptionManager>().As<ITypeMultiDescriptionService>();
            builder.RegisterType<EfTypeMultiDescriptionDal>().As<ITypeMultiDescriptionDal>();
            builder.RegisterType<AddressManager>().As<IAddressService>();
            builder.RegisterType<EfAddressDal>().As<IAddressDal>();
            builder.RegisterType<CategoryManager>().As<ICategoryService>();
            builder.RegisterType<EfCategoryDal>().As<ICategoryDal>();
            builder.RegisterType<CategoryGroupManager>().As<ICategoryGroupService>();
            builder.RegisterType<EfCategoryGroupDal>().As<ICategoryGroupDal>();
            builder.RegisterType<NameManager>().As<INameService>();
            builder.RegisterType<EfNameDal>().As<INameDal>();
            builder.RegisterType<ChainManager>().As<IChainService>();
            builder.RegisterType<EfChainDal>().As<IChainDal>();
            builder.RegisterType<CityManager>().As<ICityService>();
            builder.RegisterType<EfCityDal>().As<ICityDal>();
            builder.RegisterType<CoordinatesManager>().As<ICoordinatesService>();
            builder.RegisterType<EfCoordinatesDal>().As<ICoordinatesDal>();
            builder.RegisterType<CountryManager>().As<ICountryService>();
            builder.RegisterType<EfCountryDal>().As<ICountryDal>();
            builder.RegisterType<StateManager>().As<IStateService>();
            builder.RegisterType<EfStateDal>().As<IStateDal>();
            builder.RegisterType<DestinationManager>().As<IDestinationService>();
            builder.RegisterType<EfDestinationDal>().As<IDestinationDal>();
            builder.RegisterType<ZoneManager>().As<IZoneService>();
            builder.RegisterType<EfZoneDal>().As<IZoneDal>();
            builder.RegisterType<FacilityManager>().As<IFacilityService>();
            builder.RegisterType<EfFacilityDal>().As<IFacilityDal>();
            builder.RegisterType<FacilityDataManager>().As<IFacilityDataService>();
            builder.RegisterType<EfFacilityDataDal>().As<IFacilityDataDal>();
            builder.RegisterType<FacilityGroupManager>().As<IFacilityGroupService>();
            builder.RegisterType<EfFacilityGroupDal>().As<IFacilityGroupDal>();
            builder.RegisterType<FacilityTypologyManager>().As<IFacilityTypologyService>();
            builder.RegisterType<EfFacilityTypologyDal>().As<IFacilityTypologyDal>();
            builder.RegisterType<HotelSegmentManager>().As<IHotelSegmentService>();
            builder.RegisterType<EfHotelSegmentDal>().As<IHotelSegmentDal>();
            builder.RegisterType<ImageManager>().As<IImageService>();
            builder.RegisterType<EfImageDal>().As<IImageDal>();
            builder.RegisterType<ImageTypeManager>().As<IImageTypeService>();
            builder.RegisterType<EfImageTypeDal>().As<IImageTypeDal>();
            builder.RegisterType<InterestPointManager>().As<IInterestPointService>();
            builder.RegisterType<EfInterestPointDal>().As<IInterestPointDal>();
            builder.RegisterType<IssueManager>().As<IIssueService>();
            builder.RegisterType<EfIssueDal>().As<IIssueDal>();
            builder.RegisterType<IssueDataManager>().As<IIssueDataService>();
            builder.RegisterType<EfIssueDataDal>().As<IIssueDataDal>();
            builder.RegisterType<PhoneManager>().As<IPhoneService>();
            builder.RegisterType<EfPhoneDal>().As<IPhoneDal>();
            builder.RegisterType<RoomManager>().As<IRoomService>();
            builder.RegisterType<EfRoomDal>().As<IRoomDal>();
            builder.RegisterType<RoomDataManager>().As<IRoomDataService>();
            builder.RegisterType<EfRoomDataDal>().As<IRoomDataDal>();
            builder.RegisterType<CharacteristicDescriptionManager>().As<ICharacteristicDescriptionService>();
            builder.RegisterType<EfCharacteristicDescriptionDal>().As<ICharacteristicDescriptionDal>();
            builder.RegisterType<TypeDescriptionManager>().As<ITypeDescriptionService>();
            builder.RegisterType<EfTypeDescriptionDal>().As<ITypeDescriptionDal>();
            builder.RegisterType<RoomFacilityManager>().As<IRoomFacilityService>();
            builder.RegisterType<EfRoomFacilityDal>().As<IRoomFacilityDal>();
            builder.RegisterType<RoomStayManager>().As<IRoomStayService>();
            builder.RegisterType<EfRoomStayDal>().As<IRoomStayDal>();
            builder.RegisterType<RoomStayFacilityManager>().As<IRoomStayFacilityService>();
            builder.RegisterType<EfRoomStayFacilityDal>().As<IRoomStayFacilityDal>();
            builder.RegisterType<TerminalManager>().As<ITerminalService>();
            builder.RegisterType<EfTerminalDal>().As<ITerminalDal>();
            builder.RegisterType<TerminalDataManager>().As<ITerminalDataService>();
            builder.RegisterType<EfTerminalDataDal>().As<ITerminalDataDal>();
            builder.RegisterType<WildcardManager>().As<IWildcardService>();
            builder.RegisterType<EfWildcardDal>().As<IWildcardDal>();
            builder.RegisterType<HotelRoomDescriptionManager>().As<IHotelRoomDescriptionService>();
            builder.RegisterType<EfHotelRoomDescriptionDal>().As<IHotelRoomDescriptionDal>();
            builder.RegisterType<LanguageManager>().As<ILanguageService>();
            builder.RegisterType<EfLanguageDal>().As<ILanguageDal>();
            builder.RegisterType<SegmentManager>().As<ISegmentService>();
            builder.RegisterType<EfSegmentDal>().As<ISegmentDal>();

            // AutofacBusinessModuleAddingsEnd
            var assembly = System.Reflection.Assembly.GetExecutingAssembly();
            builder.RegisterAssemblyTypes(assembly).AsImplementedInterfaces()
                .EnableInterfaceInterceptors(new ProxyGenerationOptions()
                {
                    Selector = new AspectInterceptorSelector()
                }).SingleInstance();
        }
    }
}
