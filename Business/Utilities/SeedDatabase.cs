using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Business.Abstract;
using Core.Entities.Concrete;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace Business.Utilities
{
    public static class SeedDatabase
    {

        public static async void Seed(IApplicationBuilder app)
        {
            var scope = app.ApplicationServices.CreateScope();
            var context = scope.ServiceProvider.GetService<ApplicationDbContext>();

            // File.Delete("northwind.db"); 
            // System.Threading.Thread.Sleep(500);
            context.Database.Migrate();
            System.Console.WriteLine("READY");
            if (context.Database.GetPendingMigrations().Count() == 0)
            {
                if (context.Users.Count() == 0)
                {
                    var users = File.ReadAllText("json/user/users.json");
                    var listOfUsers = JsonConvert.DeserializeObject<List<User>>(users);

                    foreach (var user in listOfUsers)
                    {
                        byte[] passwordHash, passwordSalt;
                        Core.Utilities.Security.Hashing.HashingHelper.CreatePasswordHash("yb0216362", out passwordHash, out passwordSalt);
                        user.PasswordHash = passwordHash;
                        user.PasswordSalt = passwordSalt;
                        await context.Users.AddAsync(user);
                    }
                }
                if (context.OperationClaims.Count() == 0)
                {
                    var data = File.ReadAllText("json/user/operationclaims.json");
                    var listOfData = JsonConvert.DeserializeObject<List<OperationClaim>>(data);

                    foreach (var d in listOfData)
                    {
                        await context.OperationClaims.AddAsync(d);
                    }
                }
                if (context.UserOperationClaims.Count() == 0)
                {
                    var data = File.ReadAllText("json/user/useroperationclaims.json");
                    var listOfData = JsonConvert.DeserializeObject<List<UserOperationClaim>>(data);

                    foreach (var d in listOfData)
                    {
                        await context.UserOperationClaims.AddAsync(d);
                    }
                }

 

                // BOOKS
                if (context.Layout.Count() == 0)
                {
                    var data = File.ReadAllText("json/book/layout.json");
                    var listOfData = JsonConvert.DeserializeObject<List<Layout>>(data);

                    foreach (var a in listOfData)
                    {
                        a.Uuid = Guid.NewGuid().ToString();
                        foreach (var b in a.BookCategories)
                        {
                            b.Uuid = Guid.NewGuid().ToString();
                            b.LayoutUUID = a.Uuid;
                            foreach (var c in b.Books)
                            {
                                c.Uuid = Guid.NewGuid().ToString();
                                c.BookCategoryUUID = b.Uuid;
                                c.BookDetail.Uuid = Guid.NewGuid().ToString();
                                c.BookDetail.BookUUID = c.Uuid;
                            }
                        }
                        foreach (var b in a.LayoutParams)
                        {
                            b.Uuid = Guid.NewGuid().ToString();
                            b.LayoutUUID = a.Uuid;
                            b.Val = a.Uuid;
                        }
                        await context.Layout.AddAsync(a);
                    }
                }

                if (context.Publisher.Count() == 0)
                {
                    var data = File.ReadAllText("json/book/publisher.json");
                    var listOfData = JsonConvert.DeserializeObject<List<Publisher>>(data);

                    foreach (var d in listOfData)
                    {
                        d.Uuid = Guid.NewGuid().ToString();
                        await context.Publisher.AddAsync(d);
                    }
                }

                var k = await context.SaveChangesAsync() > 0;
                System.Console.WriteLine("k::" + k);


                if (context.BookPublisher.Count() == 0)
                {
                    var data = File.ReadAllText("json/book/bookPublisher.json");
                    var listOfData = JsonConvert.DeserializeObject<List<BookPublisher>>(data);

                    foreach (var d in listOfData)
                    {
                        d.Uuid = Guid.NewGuid().ToString();
                        await context.BookPublisher.AddAsync(d);
                    }
                }

                var l = await context.SaveChangesAsync() > 0;
                System.Console.WriteLine("l::" + l);







            }
        }
    }
}