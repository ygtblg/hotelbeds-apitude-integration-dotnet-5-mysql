using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Business.Abstract;
using Core.Entities.Concrete;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

namespace Business.Utilities
{
    public static class SeedDatabaseForHotelbedsKeyId
    {

        public static async void Seed(IApplicationBuilder app)
        {
            var scope = app.ApplicationServices.CreateScope();
            var context = scope.ServiceProvider.GetService<ApplicationDbContext>();

            // File.Delete("northwind.db"); 
            // System.Threading.Thread.Sleep(500);
            context.Database.Migrate();
            System.Console.WriteLine("READY");
            if (context.Database.GetPendingMigrations().Count() == 0)
            {


                // BookingSite
                if (context.BookingSite.Count() == 0)
                {
                    var data = File.ReadAllText("json/hotelbeds/bookingSite.json");
                    BookingSite bs = JsonConvert.DeserializeObject<BookingSite>(data);
                    bs.Uuid = Guid.NewGuid().ToString();
                    await context.BookingSite.AddAsync(bs);
                }
                var bookingSite = await context.SaveChangesAsync() > 0;
                System.Console.WriteLine("bookingSite::" + bookingSite);




                // Segment
                var segmentsData = File.ReadAllText("json/hotelbeds/segments.json");
                List<Segment> listOfSegmentData = JsonConvert.DeserializeObject<List<Segment>>(segmentsData);
                if (context.Segment.Count() == 0)
                {
                    foreach (Segment item in listOfSegmentData)
                    {
                        item.Uuid = Guid.NewGuid().ToString();
                        item.BookingSiteId = 1;
                        await context.Segment.AddAsync(item);
                    }
                    var segment = await context.SaveChangesAsync() > 0;
                     System.Console.WriteLine("segment::" + segment);
                }


                // Board
                var boardsData = File.ReadAllText("json/hotelbeds/boards.json");
                List<Board> listOfBoardData = JsonConvert.DeserializeObject<List<Board>>(boardsData);
                if (context.Board.Count() == 0)
                {
                    foreach (Board item in listOfBoardData)
                    {
                        item.Uuid = Guid.NewGuid().ToString();
                        item.BookingSiteId = 1;
                        await context.Board.AddAsync(item);
                    }
                    var board = await context.SaveChangesAsync() > 0;
                     System.Console.WriteLine("board::" + board);
                }

















                // Hotels
                if (context.Hotel.Count() == 0)
                {
                    var data = File.ReadAllText("json/hotelbeds/hotelsRS.json");
                    var listOfData = JsonConvert.DeserializeObject<List<Hotel>>(data);
                    var fData = File.ReadAllText("json/hotelbeds/facilities.json");
                    List<FacilityData> listOfFData = JsonConvert.DeserializeObject<List<FacilityData>>(fData);
                    var ftData = File.ReadAllText("json/hotelbeds/facilityTypologies.json");
                    List<FacilityTypology> listOfFtData = JsonConvert.DeserializeObject<List<FacilityTypology>>(ftData);
                    var fgData = File.ReadAllText("json/hotelbeds/facilityGroups.json");
                    List<FacilityGroup> listOfFgData = JsonConvert.DeserializeObject<List<FacilityGroup>>(fgData);



                    var languagesData = File.ReadAllText("json/hotelbeds/languages.json");
                    List<Language> listOfLanguageData = JsonConvert.DeserializeObject<List<Language>>(languagesData);



                    var categoriesData = File.ReadAllText("json/hotelbeds/categories.json");
                    List<Category> listOfCategoryData = JsonConvert.DeserializeObject<List<Category>>(categoriesData);

                    var accommodationsData = File.ReadAllText("json/hotelbeds/accommodations.json");
                    List<Accommodation> listOfAccommodationData = JsonConvert.DeserializeObject<List<Accommodation>>(accommodationsData);

                    var chainsData = File.ReadAllText("json/hotelbeds/chains.json");
                    List<Chain> listOfChainData = JsonConvert.DeserializeObject<List<Chain>>(chainsData);

                    var countriesData = File.ReadAllText("json/hotelbeds/countries.json");
                    List<Country> listOfCountryData = JsonConvert.DeserializeObject<List<Country>>(countriesData);

                    var destinationsData = File.ReadAllText("json/hotelbeds/destinations.json");
                    List<Destination> listOfDestinationData = JsonConvert.DeserializeObject<List<Destination>>(destinationsData);

                    var groupCategoriesData = File.ReadAllText("json/hotelbeds/groupCategories.json");
                    List<CategoryGroup> listOfCategoryGroupData = JsonConvert.DeserializeObject<List<CategoryGroup>>(groupCategoriesData);

                    var imageTypesData = File.ReadAllText("json/hotelbeds/imageTypes.json");
                    List<ImageType> listOfImageTypeData = JsonConvert.DeserializeObject<List<ImageType>>(imageTypesData);

                    var issuesData = File.ReadAllText("json/hotelbeds/issues.json");
                    List<IssueData> listOfIssueDataData = JsonConvert.DeserializeObject<List<IssueData>>(issuesData);

                    var roomsData = File.ReadAllText("json/hotelbeds/rooms.json");
                    List<RoomData> listOfRoomData = JsonConvert.DeserializeObject<List<RoomData>>(roomsData);

                    var terminalsData = File.ReadAllText("json/hotelbeds/terminals.json");
                    List<TerminalData> listOfTerminalDataData = JsonConvert.DeserializeObject<List<TerminalData>>(terminalsData);


                    foreach (Hotel hotel in listOfData)
                    {
                        hotel.Uuid = Guid.NewGuid().ToString();
                        hotel.BookingSiteId = 1;

                        // ICollection<HotelBoard> hbc = new List<HotelBoard>();
                        // foreach (Board item in context.Board)
                        // {
                        //     HotelBoard hb = new HotelBoard { BoardId = item.BoardId, HotelId = hotel.HotelId };
                        //     hbc.Add(hb);
                        // }
                        // hotel.HotelBoard = hbc;


                        // ICollection<HotelSegment> hsc = new List<HotelSegment>();
                        // foreach (var item in context.Segment)
                        // {
                        //     HotelSegment hs = new HotelSegment { SegmentId = item.SegmentId, HotelId = hotel.HotelId };
                        //     hsc.Add(hs);
                        // }
                        // hotel.HotelSegment = hsc;


                        // FACILITY
                        foreach (Facility f in hotel.Facilities)
                        {
                            foreach (FacilityData fd in listOfFData)
                            {
                                foreach (var ft in listOfFtData)
                                {
                                    if (fd.FacilityTypologyCode == ft.Code)
                                    {
                                        fd.FacilityTypology = ft;
                                    }
                                }
                                foreach (var fg in listOfFgData)
                                {
                                    if (fd.FacilityGroupCode == fg.Code)
                                    {
                                        fd.FacilityGroup = fg;
                                    }
                                }
                                if (fd.Code == f.FacilityCode)
                                {
                                    f.FacilityData = fd;
                                }
                            }
                        }

                        // COUNTRY.STATE
                        foreach (Country item in listOfCountryData)
                        {
                            if (hotel.CountryCode == item.Code)
                            {
                                hotel.Country = item;

                                foreach (State state in item.States)
                                {
                                    if (state.Code == hotel.StateCode)
                                    {
                                        hotel.State = state;
                                    }
                                }

                            }

                        }

                        // DESTINATION.ZONE
                        foreach (Destination item in listOfDestinationData)
                        {
                            if (item.Code == hotel.DestinationCode)
                            {
                                hotel.Destination = item;
                            }
                            foreach (Zone zone in item.Zones)
                            {
                                if (zone.ZoneCode == hotel.ZoneCode)
                                {
                                    hotel.Zone = zone;
                                }
                            }

                        }

                        // CATEGORY
                        foreach (Category item in listOfCategoryData)
                        {
                            if (item.Code == hotel.CategoryCode)
                            {
                                hotel.Category = item;
                            }
                        }

                        // CATEGORYGROUP
                        foreach (CategoryGroup item in listOfCategoryGroupData)
                        {
                            if (item.Code == hotel.CategoryGroupCode)
                            {
                                hotel.CategoryGroup = item;
                            }
                        }

                        // CHAIN
                        foreach (Chain item in listOfChainData)
                        {
                            if (item.Code == hotel.ChainCode)
                            {
                                hotel.Chain = item;
                            }
                        }

                        // ACCOMMODATION
                        foreach (Accommodation item in listOfAccommodationData)
                        {
                            if (item.Code == hotel.AccommodationTypeCode)
                            {
                                hotel.Accommodation = item;
                            }
                        }

                        // ROOM.ROOMFACITIES.ROOMSTAY.ROOMDATA
                        foreach (Room room in hotel.Rooms)
                        {
                            foreach (RoomData roomData in listOfRoomData)
                            {
                                if (room.RoomCode == roomData.Code)
                                {
                                    room.RoomData = roomData;
                                }
                            }


                            if (room.RoomFacilities != null)
                            {
                                foreach (RoomFacility f in room.RoomFacilities)
                                {
                                    foreach (FacilityData fd in listOfFData)
                                    {
                                        foreach (var ft in listOfFtData)
                                        {
                                            if (fd.FacilityTypologyCode == ft.Code)
                                            {
                                                fd.FacilityTypology = ft;
                                            }
                                        }
                                        foreach (var fg in listOfFgData)
                                        {
                                            if (fd.FacilityGroupCode == fg.Code)
                                            {
                                                fd.FacilityGroup = fg;
                                            }
                                        }
                                        if (fd.Code == f.FacilityCode)
                                        {
                                            f.FacilityData = fd;
                                        }
                                    }
                                }

                            }

                            if (room.RoomStays != null)
                            {
                                foreach (RoomStay roomStay in room.RoomStays)
                                {
                                    if(roomStay.RoomStayFacilities != null)
                                    foreach (RoomStayFacility f in roomStay.RoomStayFacilities)
                                    {
                                        foreach (FacilityData fd in listOfFData)
                                        {
                                            foreach (var ft in listOfFtData)
                                            {
                                                if (fd.FacilityTypologyCode == ft.Code)
                                                {
                                                    fd.FacilityTypology = ft;
                                                }
                                            }
                                            foreach (var fg in listOfFgData)
                                            {
                                                if (fd.FacilityGroupCode == fg.Code)
                                                {
                                                    fd.FacilityGroup = fg;
                                                }
                                            }
                                            if (fd.Code == f.FacilityCode)
                                            {
                                                f.FacilityData = fd;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        // TERMINAL.TERMINALDATA
                        foreach (Terminal item in hotel.Terminals)
                        {
                            foreach (TerminalData td in listOfTerminalDataData)
                            {
                                if (item.TerminalCode == td.Code)
                                {
                                    item.TerminalData = td;
                                }
                            }
                        }

                        // ISSUES.ISSUEDATA
                        foreach (Issue item in hotel.Issues)
                        {
                            foreach (IssueData id in listOfIssueDataData)
                            {
                                if (item.IssueCode == id.Code)
                                {
                                    item.IssueData = id;
                                }
                            }
                        }

                        // INTERESTPOINTS.FACILITYDATA
                        foreach (InterestPoint item in hotel.InterestPoints)
                        {
                            foreach (FacilityData fd in listOfFData)
                            {
                                foreach (var ft in listOfFtData)
                                {
                                    if (fd.FacilityTypologyCode == ft.Code)
                                    {
                                        fd.FacilityTypology = ft;
                                    }
                                }
                                foreach (var fg in listOfFgData)
                                {
                                    if (fd.FacilityGroupCode == fg.Code)
                                    {
                                        fd.FacilityGroup = fg;
                                    }
                                }
                                if (fd.Code == item.FacilityCode)
                                {
                                    item.FacilityData = fd;
                                }
                            }
                        }

                        // IMAGES.IMAGETYPE
                        foreach (Image item in hotel.Images)
                        {
                            foreach (ImageType it in listOfImageTypeData)
                            {
                                if (item.ImageTypeCode == it.Code)
                                {
                                    item.ImageType = it;
                                }
                            }
                        }


                        await context.Hotel.AddAsync(hotel);

                    }
                }
                var hotelsRS = await context.SaveChangesAsync() > 0;
                System.Console.WriteLine("hotelsRS::" + hotelsRS);





            }

            System.Console.WriteLine("ALL SEED DONE");
        }
    }
}