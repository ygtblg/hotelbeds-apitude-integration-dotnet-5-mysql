/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 15/08/2021 11:07:46
   ThisFileName:IPublisherService.cs
   ThisFilePath:C:\projects\BackendPartner\dotnet\agile-ionic-generator-001-api\Business\Abstract\IPublisherService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface IPublisherService
    {
        Task<IDataResult<Publisher>> GetById(int PublisherId);
        Task<IDataResult<List<Publisher>>> GetList(); 
        IResult Add(Publisher Publisher);
        IResult Delete(Publisher Publisher);
        IResult Update(Publisher Publisher);
    }
}
