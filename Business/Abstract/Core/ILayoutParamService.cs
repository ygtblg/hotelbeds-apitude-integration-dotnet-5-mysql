/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 15/08/2021 11:07:46
   ThisFileName:ILayoutParamService.cs
   ThisFilePath:C:\projects\BackendPartner\dotnet\agile-ionic-generator-001-api\Business\Abstract\ILayoutParamService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface ILayoutParamService
    {
        Task<IDataResult<LayoutParam>> GetById(int LayoutParamId);
        Task<IDataResult<List<LayoutParam>>> GetList(); 
        IResult Add(LayoutParam LayoutParam);
        IResult Delete(LayoutParam LayoutParam);
        IResult Update(LayoutParam LayoutParam);
    }
}
