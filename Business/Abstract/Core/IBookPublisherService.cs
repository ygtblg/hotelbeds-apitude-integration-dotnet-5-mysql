/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 15/08/2021 11:07:46
   ThisFileName:IBookPublisherService.cs
   ThisFilePath:C:\projects\BackendPartner\dotnet\agile-ionic-generator-001-api\Business\Abstract\IBookPublisherService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface IBookPublisherService
    {
        Task<IDataResult<BookPublisher>> GetById(int BookPublisherId);
        Task<IDataResult<List<BookPublisher>>> GetList(); 
        IResult Add(BookPublisher BookPublisher);
        IResult Delete(BookPublisher BookPublisher);
        IResult Update(BookPublisher BookPublisher);
    }
}
