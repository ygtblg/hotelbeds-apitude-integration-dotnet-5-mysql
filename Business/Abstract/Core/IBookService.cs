/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 15/08/2021 11:07:46
   ThisFileName:IBookService.cs
   ThisFilePath:C:\projects\BackendPartner\dotnet\agile-ionic-generator-001-api\Business\Abstract\IBookService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface IBookService
    {
        Task<IDataResult<Book>> GetById(int BookId);
        Task<IDataResult<List<Book>>> GetList(); 
        IResult Add(Book Book);
        IResult Delete(Book Book);
        IResult Update(Book Book);
    }
}
