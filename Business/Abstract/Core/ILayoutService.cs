/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 15/08/2021 11:07:46
   ThisFileName:ILayoutService.cs
   ThisFilePath:C:\projects\BackendPartner\dotnet\agile-ionic-generator-001-api\Business\Abstract\ILayoutService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface ILayoutService
    {
        Task<IDataResult<Layout>> GetById(int LayoutId);
        Task<IDataResult<List<Layout>>> GetList(); 
        IResult Add(Layout Layout);
        IResult Delete(Layout Layout);
        IResult Update(Layout Layout);
    }
}
