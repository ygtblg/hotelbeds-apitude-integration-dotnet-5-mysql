/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 15/08/2021 11:07:46
   ThisFileName:IBookCategoryService.cs
   ThisFilePath:C:\projects\BackendPartner\dotnet\agile-ionic-generator-001-api\Business\Abstract\IBookCategoryService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface IBookCategoryService
    {
        Task<IDataResult<BookCategory>> GetById(int BookCategoryId);
        Task<IDataResult<List<BookCategory>>> GetList(); 
        IResult Add(BookCategory BookCategory);
        IResult Delete(BookCategory BookCategory);
        IResult Update(BookCategory BookCategory);
    }
}
