/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 15/08/2021 11:07:46
   ThisFileName:IBookDetailService.cs
   ThisFilePath:C:\projects\BackendPartner\dotnet\agile-ionic-generator-001-api\Business\Abstract\IBookDetailService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface IBookDetailService
    {
        Task<IDataResult<BookDetail>> GetById(int BookDetailId);
        Task<IDataResult<List<BookDetail>>> GetList(); 
        IResult Add(BookDetail BookDetail);
        IResult Delete(BookDetail BookDetail);
        IResult Update(BookDetail BookDetail);
    }
}
