/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:IAccommodationService.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Abstract\App\IAccommodationService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface IAccommodationService
    {
        Task<IDataResult<Accommodation>> GetById(int AccommodationId);
        Task<IDataResult<List<Accommodation>>> GetList(); 
        IResult Add(Accommodation Accommodation);
        IResult Delete(Accommodation Accommodation);
        IResult Update(Accommodation Accommodation);
    }
}
