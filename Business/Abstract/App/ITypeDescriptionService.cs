/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:ITypeDescriptionService.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Abstract\App\ITypeDescriptionService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface ITypeDescriptionService
    {
        Task<IDataResult<TypeDescription>> GetById(int TypeDescriptionId);
        Task<IDataResult<List<TypeDescription>>> GetList(); 
        IResult Add(TypeDescription TypeDescription);
        IResult Delete(TypeDescription TypeDescription);
        IResult Update(TypeDescription TypeDescription);
    }
}
