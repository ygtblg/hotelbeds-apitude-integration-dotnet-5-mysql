/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:IDestinationService.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Abstract\App\IDestinationService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface IDestinationService
    {
        Task<IDataResult<Destination>> GetById(int DestinationId);
        Task<IDataResult<List<Destination>>> GetList(); 
        IResult Add(Destination Destination);
        IResult Delete(Destination Destination);
        IResult Update(Destination Destination);
    }
}
