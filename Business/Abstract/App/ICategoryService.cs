/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:ICategoryService.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Abstract\App\ICategoryService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface ICategoryService
    {
        Task<IDataResult<Category>> GetById(int CategoryId);
        Task<IDataResult<List<Category>>> GetList(); 
        IResult Add(Category Category);
        IResult Delete(Category Category);
        IResult Update(Category Category);
    }
}
