/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:IWildcardService.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Abstract\App\IWildcardService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface IWildcardService
    {
        Task<IDataResult<Wildcard>> GetById(int WildcardId);
        Task<IDataResult<List<Wildcard>>> GetList(); 
        IResult Add(Wildcard Wildcard);
        IResult Delete(Wildcard Wildcard);
        IResult Update(Wildcard Wildcard);
    }
}
