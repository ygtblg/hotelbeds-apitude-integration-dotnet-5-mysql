/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:ICoordinatesService.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Abstract\App\ICoordinatesService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface ICoordinatesService
    {
        Task<IDataResult<Coordinates>> GetById(int CoordinatesId);
        Task<IDataResult<List<Coordinates>>> GetList(); 
        IResult Add(Coordinates Coordinates);
        IResult Delete(Coordinates Coordinates);
        IResult Update(Coordinates Coordinates);
    }
}
