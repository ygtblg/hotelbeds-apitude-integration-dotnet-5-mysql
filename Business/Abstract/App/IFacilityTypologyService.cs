/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:IFacilityTypologyService.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Abstract\App\IFacilityTypologyService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface IFacilityTypologyService
    {
        Task<IDataResult<FacilityTypology>> GetById(int FacilityTypologyId);
        Task<IDataResult<List<FacilityTypology>>> GetList(); 
        IResult Add(FacilityTypology FacilityTypology);
        IResult Delete(FacilityTypology FacilityTypology);
        IResult Update(FacilityTypology FacilityTypology);
    }
}
