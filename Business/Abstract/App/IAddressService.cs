/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:IAddressService.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Abstract\App\IAddressService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface IAddressService
    {
        Task<IDataResult<Address>> GetById(int AddressId);
        Task<IDataResult<List<Address>>> GetList(); 
        IResult Add(Address Address);
        IResult Delete(Address Address);
        IResult Update(Address Address);
    }
}
