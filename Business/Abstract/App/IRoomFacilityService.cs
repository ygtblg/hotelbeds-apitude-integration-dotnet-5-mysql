/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:IRoomFacilityService.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Abstract\App\IRoomFacilityService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface IRoomFacilityService
    {
        Task<IDataResult<RoomFacility>> GetById(int RoomFacilityId);
        Task<IDataResult<List<RoomFacility>>> GetList(); 
        IResult Add(RoomFacility RoomFacility);
        IResult Delete(RoomFacility RoomFacility);
        IResult Update(RoomFacility RoomFacility);
    }
}
