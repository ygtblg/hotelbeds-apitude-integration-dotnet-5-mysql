/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:IPhoneService.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Abstract\App\IPhoneService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface IPhoneService
    {
        Task<IDataResult<Phone>> GetById(int PhoneId);
        Task<IDataResult<List<Phone>>> GetList(); 
        IResult Add(Phone Phone);
        IResult Delete(Phone Phone);
        IResult Update(Phone Phone);
    }
}
