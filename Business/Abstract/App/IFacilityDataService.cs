/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:IFacilityDataService.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Abstract\App\IFacilityDataService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface IFacilityDataService
    {
        Task<IDataResult<FacilityData>> GetById(int FacilityDataId);
        Task<IDataResult<List<FacilityData>>> GetList(); 
        IResult Add(FacilityData FacilityData);
        IResult Delete(FacilityData FacilityData);
        IResult Update(FacilityData FacilityData);
    }
}
