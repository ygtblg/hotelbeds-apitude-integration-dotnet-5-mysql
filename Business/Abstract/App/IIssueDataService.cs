/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:IIssueDataService.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Abstract\App\IIssueDataService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface IIssueDataService
    {
        Task<IDataResult<IssueData>> GetById(int IssueDataId);
        Task<IDataResult<List<IssueData>>> GetList(); 
        IResult Add(IssueData IssueData);
        IResult Delete(IssueData IssueData);
        IResult Update(IssueData IssueData);
    }
}
