/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:IRoomStayFacilityService.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Abstract\App\IRoomStayFacilityService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface IRoomStayFacilityService
    {
        Task<IDataResult<RoomStayFacility>> GetById(int RoomStayFacilityId);
        Task<IDataResult<List<RoomStayFacility>>> GetList(); 
        IResult Add(RoomStayFacility RoomStayFacility);
        IResult Delete(RoomStayFacility RoomStayFacility);
        IResult Update(RoomStayFacility RoomStayFacility);
    }
}
