/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:ICategoryGroupService.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Abstract\App\ICategoryGroupService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface ICategoryGroupService
    {
        Task<IDataResult<CategoryGroup>> GetById(int CategoryGroupId);
        Task<IDataResult<List<CategoryGroup>>> GetList(); 
        IResult Add(CategoryGroup CategoryGroup);
        IResult Delete(CategoryGroup CategoryGroup);
        IResult Update(CategoryGroup CategoryGroup);
    }
}
