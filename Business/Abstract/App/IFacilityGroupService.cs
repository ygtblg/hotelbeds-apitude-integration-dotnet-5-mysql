/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:IFacilityGroupService.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Abstract\App\IFacilityGroupService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface IFacilityGroupService
    {
        Task<IDataResult<FacilityGroup>> GetById(int FacilityGroupId);
        Task<IDataResult<List<FacilityGroup>>> GetList(); 
        IResult Add(FacilityGroup FacilityGroup);
        IResult Delete(FacilityGroup FacilityGroup);
        IResult Update(FacilityGroup FacilityGroup);
    }
}
