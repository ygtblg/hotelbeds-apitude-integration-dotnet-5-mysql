/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:ICityService.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Abstract\App\ICityService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface ICityService
    {
        Task<IDataResult<City>> GetById(int CityId);
        Task<IDataResult<List<City>>> GetList(); 
        IResult Add(City City);
        IResult Delete(City City);
        IResult Update(City City);
    }
}
