/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:IHotelSegmentService.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Abstract\App\IHotelSegmentService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface IHotelSegmentService
    {
        Task<IDataResult<HotelSegment>> GetById(int HotelSegmentId);
        Task<IDataResult<List<HotelSegment>>> GetList(); 
        IResult Add(HotelSegment HotelSegment);
        IResult Delete(HotelSegment HotelSegment);
        IResult Update(HotelSegment HotelSegment);
    }
}
