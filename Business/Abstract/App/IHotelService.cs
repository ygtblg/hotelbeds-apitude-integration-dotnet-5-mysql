/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:IHotelService.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Abstract\App\IHotelService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface IHotelService
    {
        Task<IDataResult<Hotel>> GetById(int HotelId);
        Task<IDataResult<List<Hotel>>> GetList(); 
        IResult Add(Hotel Hotel);
        IResult Delete(Hotel Hotel);
        IResult Update(Hotel Hotel);
    }
}
