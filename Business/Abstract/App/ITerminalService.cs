/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:ITerminalService.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Abstract\App\ITerminalService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface ITerminalService
    {
        Task<IDataResult<Terminal>> GetById(int TerminalId);
        Task<IDataResult<List<Terminal>>> GetList(); 
        IResult Add(Terminal Terminal);
        IResult Delete(Terminal Terminal);
        IResult Update(Terminal Terminal);
    }
}
