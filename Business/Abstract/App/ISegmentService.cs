/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:ISegmentService.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Abstract\App\ISegmentService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface ISegmentService
    {
        Task<IDataResult<Segment>> GetById(int SegmentId);
        Task<IDataResult<List<Segment>>> GetList(); 
        IResult Add(Segment Segment);
        IResult Delete(Segment Segment);
        IResult Update(Segment Segment);
    }
}
