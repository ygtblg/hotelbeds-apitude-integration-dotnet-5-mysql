/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:IImageService.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Abstract\App\IImageService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface IImageService
    {
        Task<IDataResult<Image>> GetById(int ImageId);
        Task<IDataResult<List<Image>>> GetList(); 
        IResult Add(Image Image);
        IResult Delete(Image Image);
        IResult Update(Image Image);
    }
}
