/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:IIssueService.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Abstract\App\IIssueService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface IIssueService
    {
        Task<IDataResult<Issue>> GetById(int IssueId);
        Task<IDataResult<List<Issue>>> GetList(); 
        IResult Add(Issue Issue);
        IResult Delete(Issue Issue);
        IResult Update(Issue Issue);
    }
}
