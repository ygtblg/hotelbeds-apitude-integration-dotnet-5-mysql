/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:IBookingSiteService.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Abstract\App\IBookingSiteService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface IBookingSiteService
    {
        Task<IDataResult<BookingSite>> GetById(int BookingSiteId);
        Task<IDataResult<List<BookingSite>>> GetList(); 
        IResult Add(BookingSite BookingSite);
        IResult Delete(BookingSite BookingSite);
        IResult Update(BookingSite BookingSite);
    }
}
