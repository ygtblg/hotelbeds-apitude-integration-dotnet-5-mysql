/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:IHotelBoardService.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Abstract\App\IHotelBoardService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface IHotelBoardService
    {
        Task<IDataResult<HotelBoard>> GetById(int HotelBoardId);
        Task<IDataResult<List<HotelBoard>>> GetList(); 
        IResult Add(HotelBoard HotelBoard);
        IResult Delete(HotelBoard HotelBoard);
        IResult Update(HotelBoard HotelBoard);
    }
}
