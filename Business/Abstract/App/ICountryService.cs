/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:ICountryService.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Abstract\App\ICountryService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface ICountryService
    {
        Task<IDataResult<Country>> GetById(int CountryId);
        Task<IDataResult<List<Country>>> GetList(); 
        IResult Add(Country Country);
        IResult Delete(Country Country);
        IResult Update(Country Country);
    }
}
