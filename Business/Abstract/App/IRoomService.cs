/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:IRoomService.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Abstract\App\IRoomService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface IRoomService
    {
        Task<IDataResult<Room>> GetById(int RoomId);
        Task<IDataResult<List<Room>>> GetList(); 
        IResult Add(Room Room);
        IResult Delete(Room Room);
        IResult Update(Room Room);
    }
}
