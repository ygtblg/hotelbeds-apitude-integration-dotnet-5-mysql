/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:IDescriptionService.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Abstract\App\IDescriptionService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface IDescriptionService
    {
        Task<IDataResult<Description>> GetById(int DescriptionId);
        Task<IDataResult<List<Description>>> GetList(); 
        IResult Add(Description Description);
        IResult Delete(Description Description);
        IResult Update(Description Description);
    }
}
