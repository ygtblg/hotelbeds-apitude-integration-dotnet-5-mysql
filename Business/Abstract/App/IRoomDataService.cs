/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:IRoomDataService.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Abstract\App\IRoomDataService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface IRoomDataService
    {
        Task<IDataResult<RoomData>> GetById(int RoomDataId);
        Task<IDataResult<List<RoomData>>> GetList(); 
        IResult Add(RoomData RoomData);
        IResult Delete(RoomData RoomData);
        IResult Update(RoomData RoomData);
    }
}
