/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:IInterestPointService.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Abstract\App\IInterestPointService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface IInterestPointService
    {
        Task<IDataResult<InterestPoint>> GetById(int InterestPointId);
        Task<IDataResult<List<InterestPoint>>> GetList(); 
        IResult Add(InterestPoint InterestPoint);
        IResult Delete(InterestPoint InterestPoint);
        IResult Update(InterestPoint InterestPoint);
    }
}
