/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:IChainService.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Abstract\App\IChainService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface IChainService
    {
        Task<IDataResult<Chain>> GetById(int ChainId);
        Task<IDataResult<List<Chain>>> GetList(); 
        IResult Add(Chain Chain);
        IResult Delete(Chain Chain);
        IResult Update(Chain Chain);
    }
}
