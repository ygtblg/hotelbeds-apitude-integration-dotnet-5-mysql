/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:IRoomStayService.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Abstract\App\IRoomStayService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface IRoomStayService
    {
        Task<IDataResult<RoomStay>> GetById(int RoomStayId);
        Task<IDataResult<List<RoomStay>>> GetList(); 
        IResult Add(RoomStay RoomStay);
        IResult Delete(RoomStay RoomStay);
        IResult Update(RoomStay RoomStay);
    }
}
