/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:ITypeMultiDescriptionService.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Abstract\App\ITypeMultiDescriptionService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface ITypeMultiDescriptionService
    {
        Task<IDataResult<TypeMultiDescription>> GetById(int TypeMultiDescriptionId);
        Task<IDataResult<List<TypeMultiDescription>>> GetList(); 
        IResult Add(TypeMultiDescription TypeMultiDescription);
        IResult Delete(TypeMultiDescription TypeMultiDescription);
        IResult Update(TypeMultiDescription TypeMultiDescription);
    }
}
