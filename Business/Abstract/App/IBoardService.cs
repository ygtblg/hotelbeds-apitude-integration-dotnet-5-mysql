/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:IBoardService.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Abstract\App\IBoardService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface IBoardService
    {
        Task<IDataResult<Board>> GetById(int BoardId);
        Task<IDataResult<List<Board>>> GetList(); 
        IResult Add(Board Board);
        IResult Delete(Board Board);
        IResult Update(Board Board);
    }
}
