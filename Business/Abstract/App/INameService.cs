/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:INameService.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Abstract\App\INameService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface INameService
    {
        Task<IDataResult<Name>> GetById(int NameId);
        Task<IDataResult<List<Name>>> GetList(); 
        IResult Add(Name Name);
        IResult Delete(Name Name);
        IResult Update(Name Name);
    }
}
