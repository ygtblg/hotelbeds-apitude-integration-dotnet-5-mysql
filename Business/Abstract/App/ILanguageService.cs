/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:ILanguageService.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Abstract\App\ILanguageService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface ILanguageService
    {
        Task<IDataResult<Language>> GetById(int LanguageId);
        Task<IDataResult<List<Language>>> GetList(); 
        IResult Add(Language Language);
        IResult Delete(Language Language);
        IResult Update(Language Language);
    }
}
