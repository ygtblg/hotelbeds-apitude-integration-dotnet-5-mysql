/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:IZoneService.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Abstract\App\IZoneService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface IZoneService
    {
        Task<IDataResult<Zone>> GetById(int ZoneId);
        Task<IDataResult<List<Zone>>> GetList(); 
        IResult Add(Zone Zone);
        IResult Delete(Zone Zone);
        IResult Update(Zone Zone);
    }
}
