/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:IStateService.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Abstract\App\IStateService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface IStateService
    {
        Task<IDataResult<State>> GetById(int StateId);
        Task<IDataResult<List<State>>> GetList(); 
        IResult Add(State State);
        IResult Delete(State State);
        IResult Update(State State);
    }
}
