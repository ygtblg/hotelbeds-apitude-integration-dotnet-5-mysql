/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:IHotelRoomDescriptionService.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Abstract\App\IHotelRoomDescriptionService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface IHotelRoomDescriptionService
    {
        Task<IDataResult<HotelRoomDescription>> GetById(int HotelRoomDescriptionId);
        Task<IDataResult<List<HotelRoomDescription>>> GetList(); 
        IResult Add(HotelRoomDescription HotelRoomDescription);
        IResult Delete(HotelRoomDescription HotelRoomDescription);
        IResult Update(HotelRoomDescription HotelRoomDescription);
    }
}
