/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:ICharacteristicDescriptionService.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Abstract\App\ICharacteristicDescriptionService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface ICharacteristicDescriptionService
    {
        Task<IDataResult<CharacteristicDescription>> GetById(int CharacteristicDescriptionId);
        Task<IDataResult<List<CharacteristicDescription>>> GetList(); 
        IResult Add(CharacteristicDescription CharacteristicDescription);
        IResult Delete(CharacteristicDescription CharacteristicDescription);
        IResult Update(CharacteristicDescription CharacteristicDescription);
    }
}
