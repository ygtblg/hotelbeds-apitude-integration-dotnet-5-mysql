/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:IFacilityService.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Abstract\App\IFacilityService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface IFacilityService
    {
        Task<IDataResult<Facility>> GetById(int FacilityId);
        Task<IDataResult<List<Facility>>> GetList(); 
        IResult Add(Facility Facility);
        IResult Delete(Facility Facility);
        IResult Update(Facility Facility);
    }
}
