/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:ITerminalDataService.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Abstract\App\ITerminalDataService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface ITerminalDataService
    {
        Task<IDataResult<TerminalData>> GetById(int TerminalDataId);
        Task<IDataResult<List<TerminalData>>> GetList(); 
        IResult Add(TerminalData TerminalData);
        IResult Delete(TerminalData TerminalData);
        IResult Update(TerminalData TerminalData);
    }
}
