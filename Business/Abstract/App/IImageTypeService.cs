/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:IImageTypeService.cs
   ThisFilePath:C:\Generated\booking-site\server\Business\Abstract\App\IImageTypeService.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Utilities.Results;
using Entities.Concrete;

namespace Business.Abstract
{
    public interface IImageTypeService
    {
        Task<IDataResult<ImageType>> GetById(int ImageTypeId);
        Task<IDataResult<List<ImageType>>> GetList(); 
        IResult Add(ImageType ImageType);
        IResult Delete(ImageType ImageType);
        IResult Update(ImageType ImageType);
    }
}
