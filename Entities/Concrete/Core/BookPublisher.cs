/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 15/08/2021 11:07:46
   ThisFileName:BookPublisher.cs
   ThisFilePath:C:\projects\BackendPartner\dotnet\agile-ionic-generator-001-api\Entities\Concrete\BookPublisher.cs
*/

//		PARENT Module Name : Book
//		PARENT Module Name : Publisher
//		Current field[0].name : publisherUUID
//		Current field[1].name : bookUUID
using Core.Entities;
using System.Collections.Generic;

namespace Entities.Concrete
{
    public class BookPublisher:IEntity
    {
        public int BookPublisherId { get; set; }
        public string Uuid { get; set; }


//      OneToMany Relation With	: Book
        public int BookId { get; set; }
        public Book  Book { get; set; } 

//      OneToMany Relation With	: Publisher
        public int PublisherId { get; set; }
        public Publisher  Publisher { get; set; } 

//			- Iki adet oneToMany field varsa ve model temel tipte bir field icermiyorsa bu bir mapModel'dir.
//			primaryTypeFieldExists :: false
//			oneToManyFieldCount :: 2
//			public ICollection<ProductManufacturer> ProductManufacturer { get; set; } // ÇokaÇok
    }
}
