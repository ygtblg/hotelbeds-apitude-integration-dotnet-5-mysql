/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 15/08/2021 11:07:46
   ThisFileName:BookDetail.cs
   ThisFilePath:C:\projects\BackendPartner\dotnet\agile-ionic-generator-001-api\Entities\Concrete\BookDetail.cs
*/

//		PARENT Module Name : Book
//		Current field[0].name : description
//		Current field[1].name : bookUUID
using Core.Entities;
using System.Collections.Generic;

namespace Entities.Concrete
{
    public class BookDetail:IEntity
    {
        public int BookDetailId { get; set; }
        public string Uuid { get; set; }
        public string Description { get; set; }
        public string BookUUID { get; set; }


        public Book Book { get; set; }
    }
}
