/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 15/08/2021 11:07:46
   ThisFileName:Layout.cs
   ThisFilePath:C:\projects\BackendPartner\dotnet\agile-ionic-generator-001-api\Entities\Concrete\Layout.cs
*/

//		REF Module Name : LayoutParam
//			REF Model Field : uuid : string
//			REF Model Field : key : string
//			REF Model Field : val : string
//			REF Model Field : layoutUUID : string
//		REF Module Name : BookCategory
//			REF Model Field : uuid : string
//			REF Model Field : name : string
//			REF Model Field : books : IBook[]
//			REF Model Field : layoutUUID : string
//		REF Module Name : Publisher
//			REF Model Field : uuid : string
//			REF Model Field : name : string
//			REF Model Field : bookPublishers : IBookPublisher[]
//		Current field[0].name : loggedIn
//		Current field[1].name : error
//		Current field[2].name : success
//		Current field[3].name : isRunningMobile
//		Current field[4].name : windowWidth
//		Current field[5].name : windowHeight
//		Current field[6].name : layoutParams
//		Current field[7].name : bookCategories
//		Current field[8].name : publishers
using Core.Entities;
using System.Collections.Generic;

namespace Entities.Concrete
{
    public class Layout:IEntity
    {
        public int LayoutId { get; set; }
        public string Uuid { get; set; }
        public bool LoggedIn { get; set; }
        public bool Error { get; set; }
        public bool Success { get; set; }
        public bool IsRunningMobile { get; set; }
        public int WindowWidth { get; set; }
        public int WindowHeight { get; set; }
// 1        ILayoutParam
// 2        ILayoutParam
        public ICollection<LayoutParam> LayoutParams { get; set; }
// 1        IBookCategory
// 2        IBookCategory
        public ICollection<BookCategory> BookCategories { get; set; }
// 1        IPublisher
// 2        IPublisher
        public ICollection<Publisher> Publishers { get; set; }


    }
}
