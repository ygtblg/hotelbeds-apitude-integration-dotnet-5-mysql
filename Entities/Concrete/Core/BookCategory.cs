/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 15/08/2021 11:07:46
   ThisFileName:BookCategory.cs
   ThisFilePath:C:\projects\BackendPartner\dotnet\agile-ionic-generator-001-api\Entities\Concrete\BookCategory.cs
*/

//		REF Module Name : Book
//			REF Model Field : uuid : string
//			REF Model Field : name : string
//			REF Model Field : bookDetail : IBookDetail
//			REF Model Field : bookCategoryUUID : string
//			REF Model Field : bookPublishers : IBookPublisher[]
//		PARENT Module Name : Layout
//		Current field[0].name : name
//		Current field[1].name : books
//		Current field[2].name : layoutUUID
using Core.Entities;
using System.Collections.Generic;

namespace Entities.Concrete
{
    public class BookCategory:IEntity
    {
        public int BookCategoryId { get; set; }
        public string Uuid { get; set; }
        public string Name { get; set; }
// 1        IBook
// 2        IBook
        public ICollection<Book> Books { get; set; }
        public string LayoutUUID { get; set; }


//      OneToMany Relation With	: Layout
        public int LayoutId { get; set; }
        public Layout  Layout { get; set; } 

    }
}
