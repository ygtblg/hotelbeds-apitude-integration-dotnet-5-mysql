/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 15/08/2021 11:07:46
   ThisFileName:Publisher.cs
   ThisFilePath:C:\projects\BackendPartner\dotnet\agile-ionic-generator-001-api\Entities\Concrete\Publisher.cs
*/

//		REF Module Name : BookPublisher
//			REF Model Field : uuid : string
//			REF Model Field : publisherUUID : string
//			REF Model Field : bookUUID : string
//		PARENT Module Name : Layout
//		Current field[0].name : name
//		Current field[1].name : bookPublishers
using Core.Entities;
using System.Collections.Generic;

namespace Entities.Concrete
{
    public class Publisher:IEntity
    {
        public int PublisherId { get; set; }
        public string Uuid { get; set; }
        public string Name { get; set; }
// 1        IBookPublisher
// 2        IBookPublisher
        public ICollection<BookPublisher> BookPublishers { get; set; }


//      OneToMany Relation With	: Layout
        public int LayoutId { get; set; }
        public Layout  Layout { get; set; } 

    }
}
