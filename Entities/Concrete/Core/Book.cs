/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 15/08/2021 11:07:46
   ThisFileName:Book.cs
   ThisFilePath:C:\projects\BackendPartner\dotnet\agile-ionic-generator-001-api\Entities\Concrete\Book.cs
*/

//		REF Module Name : BookDetail
//			REF Model Field : uuid : string
//			REF Model Field : description : string
//			REF Model Field : bookUUID : string
//		REF Module Name : BookPublisher
//			REF Model Field : uuid : string
//			REF Model Field : publisherUUID : string
//			REF Model Field : bookUUID : string
//		PARENT Module Name : BookCategory
//		Current field[0].name : name
//		Current field[1].name : bookDetail
//		Current field[2].name : bookCategoryUUID
//		Current field[3].name : bookPublishers
using Core.Entities;
using System.Collections.Generic;

namespace Entities.Concrete
{
    public class Book:IEntity
    {
        public int BookId { get; set; }
        public string Uuid { get; set; }
        public string Name { get; set; }
        public string BookCategoryUUID { get; set; }
// 1        IBookPublisher
// 2        IBookPublisher
        public ICollection<BookPublisher> BookPublishers { get; set; }


//      OneToMany Relation With	: BookCategory
        public int BookCategoryId { get; set; }
        public BookCategory  BookCategory { get; set; } 

//      OneToOne Relation With	: BookDetail
        public int BookDetailId { get; set; }
        public BookDetail BookDetail { get; set; }

    }
}
