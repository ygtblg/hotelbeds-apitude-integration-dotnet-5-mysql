/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 15/08/2021 11:07:46
   ThisFileName:LayoutParam.cs
   ThisFilePath:C:\projects\BackendPartner\dotnet\agile-ionic-generator-001-api\Entities\Concrete\LayoutParam.cs
*/

//		PARENT Module Name : Layout
//		Current field[0].name : key
//		Current field[1].name : val
//		Current field[2].name : layoutUUID
using Core.Entities;
using System.Collections.Generic;

namespace Entities.Concrete
{
    public class LayoutParam:IEntity
    {
        public int LayoutParamId { get; set; }
        public string Uuid { get; set; }
        public string Key { get; set; }
        public string Val { get; set; }
        public string LayoutUUID { get; set; }


//      OneToMany Relation With	: Layout
        public int LayoutId { get; set; }
        public Layout  Layout { get; set; } 

    }
}
