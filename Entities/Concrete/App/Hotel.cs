/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:Hotel.cs
   ThisFilePath:C:\Generated\booking-site\server\Entities\Concrete\App\Hotel.cs
*/

using Core.Entities;
using System;
using System.Collections.Generic;

namespace Entities.Concrete
{
    public class Hotel:IEntity
    {
        public int HotelId { get; set; }
//      Field AddingString 1	: string Uuid
        public string Uuid { get; set; }
//      Field AddingString 2	: string BookingSiteUUID
        public string BookingSiteUUID { get; set; }
//      Field AddingString 2	: int Code
        public int Code { get; set; }
// 1        IHotelBoard
// 2        IHotelBoard
        public ICollection<HotelBoard> HotelBoard { get; set; }
        public List<string> BoardCodes { get; set; }
// 1        IHotelSegment
// 2        IHotelSegment
        public ICollection<HotelSegment> HotelSegment { get; set; }
        public List<int> SegmentCodes { get; set; }
//      Field AddingString 2	: string CountryCode
        public string CountryCode { get; set; }
//      Field AddingString 2	: string StateCode
        public string StateCode { get; set; }
//      Field AddingString 2	: string DestinationCode
        public string DestinationCode { get; set; }
//      Field AddingString 2	: int ZoneCode
        public int ZoneCode { get; set; }
//      Field AddingString 2	: string CategoryCode
        public string CategoryCode { get; set; }
//      Field AddingString 2	: string CategoryGroupCode
        public string CategoryGroupCode { get; set; }
//      Field AddingString 2	: string ChainCode
        public string ChainCode { get; set; }
//      Field AddingString 2	: string AccommodationTypeCode
        public string AccommodationTypeCode { get; set; }
//      Field AddingString 2	: string PostalCode
        public string PostalCode { get; set; }
//      Field AddingString 2	: string Email
        public string Email { get; set; }
//      Field AddingString 2	: string License
        public string License { get; set; }
// 1        IPhone
// 2        IPhone
        public ICollection<Phone> Phones { get; set; }
// 1        IRoom
// 2        IRoom
        public ICollection<Room> Rooms { get; set; }
// 1        IFacility
// 2        IFacility
        public ICollection<Facility> Facilities { get; set; }
// 1        ITerminal
// 2        ITerminal
        public ICollection<Terminal> Terminals { get; set; }
// 1        IIssue
// 2        IIssue
        public ICollection<Issue> Issues { get; set; }
// 1        IInterestPoint
// 2        IInterestPoint
        public ICollection<InterestPoint> InterestPoints { get; set; }
// 1        IImage
// 2        IImage
        public ICollection<Image> Images { get; set; }
// 1        IWildcard
// 2        IWildcard
        public ICollection<Wildcard> Wildcards { get; set; }


//      OneToMany Relation With	: BookingSite
        public int BookingSiteId { get; set; }
//      Field AddingString 3	: BookingSite BookingSite
        public BookingSite BookingSite { get; set; } 

//      OneToOne Relation With	: Name
        public int NameId { get; set; }
        public Name Name { get; set; }

//      OneToOne Relation With	: Description
        public int DescriptionId { get; set; }
        public Description Description { get; set; }

//      OneToOne Relation With	: Country
        public int CountryId { get; set; }
        public Country Country { get; set; }

//      OneToOne Relation With	: State
        public int StateId { get; set; }
        public State State { get; set; }

//      OneToOne Relation With	: Destination
        public int DestinationId { get; set; }
        public Destination Destination { get; set; }

//      OneToOne Relation With	: Zone
        public int ZoneId { get; set; }
        public Zone Zone { get; set; }

//      OneToOne Relation With	: Coordinates
        public int CoordinatesId { get; set; }
        public Coordinates Coordinates { get; set; }

//      OneToOne Relation With	: Category
        public int CategoryId { get; set; }
        public Category Category { get; set; }

//      OneToOne Relation With	: CategoryGroup
        public int CategoryGroupId { get; set; }
        public CategoryGroup CategoryGroup { get; set; }

//      OneToOne Relation With	: Chain
        public int ChainId { get; set; }
        public Chain Chain { get; set; }

//      OneToOne Relation With	: Accommodation
        public int AccommodationId { get; set; }
        public Accommodation Accommodation { get; set; }

//      OneToOne Relation With	: Address
        public int AddressId { get; set; }
        public Address Address { get; set; }

//      OneToOne Relation With	: City
        public int CityId { get; set; }
        public City City { get; set; }

//      Field parentModule.name 4	: booking-site
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Hotel
//      Field currmn 4	: Hotel
//      Field AddingString 4	: BookingSite BookingSite
//      Field parentFieldType 4	: null
//      Field cmn 4	: Language
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Board
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Segment
//      Field currmn 4	: Hotel
//      Field parentModule.name 4	: booking-site
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Hotel
//      Field currmn 4	: Hotel
//      Field AddingString 4	: BookingSite BookingSite
//      Field parentFieldType 4	: null
//      Field cmn 4	: Language
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Board
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Segment
//      Field currmn 4	: Hotel
//      Field parentModule.name 4	: booking-site
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Hotel
//      Field currmn 4	: Hotel
//      Field AddingString 4	: BookingSite BookingSite
//      Field parentFieldType 4	: null
//      Field cmn 4	: Language
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Board
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Segment
//      Field currmn 4	: Hotel
//      Field parentModule.name 4	: booking-site
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Hotel
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Language
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Board
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Segment
//      Field currmn 4	: Hotel
//      Field parentModule.name 4	: booking-site
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Hotel
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Language
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Board
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Segment
//      Field currmn 4	: Hotel
//      Field parentModule.name 4	: booking-site
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Hotel
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Language
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Board
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Segment
//      Field currmn 4	: Hotel
//      Field parentModule.name 4	: booking-site
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Hotel
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Language
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Board
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Segment
//      Field currmn 4	: Hotel
//      Field parentModule.name 4	: booking-site
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Hotel
//      Field currmn 4	: Hotel
//      Field AddingString 4	: BookingSite BookingSite
//      Field parentFieldType 4	: null
//      Field cmn 4	: Language
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Board
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Segment
//      Field currmn 4	: Hotel
//      Field parentModule.name 4	: booking-site
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Hotel
//      Field currmn 4	: Hotel
//      Field AddingString 4	: BookingSite BookingSite
//      Field parentFieldType 4	: null
//      Field cmn 4	: Language
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Board
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Segment
//      Field currmn 4	: Hotel
//      Field parentModule.name 4	: booking-site
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Hotel
//      Field currmn 4	: Hotel
//      Field AddingString 4	: BookingSite BookingSite
//      Field parentFieldType 4	: null
//      Field cmn 4	: Language
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Board
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Segment
//      Field currmn 4	: Hotel
//      Field parentModule.name 4	: booking-site
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Hotel
//      Field currmn 4	: Hotel
//      Field AddingString 4	: BookingSite BookingSite
//      Field parentFieldType 4	: null
//      Field cmn 4	: Language
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Board
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Segment
//      Field currmn 4	: Hotel
//      Field parentModule.name 4	: booking-site
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Hotel
//      Field currmn 4	: Hotel
//      Field AddingString 4	: BookingSite BookingSite
//      Field parentFieldType 4	: null
//      Field cmn 4	: Language
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Board
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Segment
//      Field currmn 4	: Hotel
//      Field parentModule.name 4	: booking-site
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Hotel
//      Field currmn 4	: Hotel
//      Field AddingString 4	: BookingSite BookingSite
//      Field parentFieldType 4	: null
//      Field cmn 4	: Language
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Board
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Segment
//      Field currmn 4	: Hotel
//      Field parentModule.name 4	: booking-site
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Hotel
//      Field currmn 4	: Hotel
//      Field AddingString 4	: BookingSite BookingSite
//      Field parentFieldType 4	: null
//      Field cmn 4	: Language
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Board
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Segment
//      Field currmn 4	: Hotel
//      Field parentModule.name 4	: booking-site
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Hotel
//      Field currmn 4	: Hotel
//      Field AddingString 4	: BookingSite BookingSite
//      Field parentFieldType 4	: null
//      Field cmn 4	: Language
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Board
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Segment
//      Field currmn 4	: Hotel
//      Field parentModule.name 4	: booking-site
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Hotel
//      Field currmn 4	: Hotel
//      Field AddingString 4	: BookingSite BookingSite
//      Field parentFieldType 4	: null
//      Field cmn 4	: Language
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Board
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Segment
//      Field currmn 4	: Hotel
//      Field parentModule.name 4	: booking-site
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Hotel
//      Field currmn 4	: Hotel
//      Field AddingString 4	: BookingSite BookingSite
//      Field parentFieldType 4	: null
//      Field cmn 4	: Language
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Board
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Segment
//      Field currmn 4	: Hotel
//      Field parentModule.name 4	: booking-site
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Hotel
//      Field currmn 4	: Hotel
//      Field AddingString 4	: BookingSite BookingSite
//      Field parentFieldType 4	: null
//      Field cmn 4	: Language
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Board
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Segment
//      Field currmn 4	: Hotel
//      Field parentModule.name 4	: booking-site
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Hotel
//      Field currmn 4	: Hotel
//      Field AddingString 4	: BookingSite BookingSite
//      Field parentFieldType 4	: null
//      Field cmn 4	: Language
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Board
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Segment
//      Field currmn 4	: Hotel
//      Field parentModule.name 4	: booking-site
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Hotel
//      Field currmn 4	: Hotel
//      Field AddingString 4	: BookingSite BookingSite
//      Field parentFieldType 4	: null
//      Field cmn 4	: Language
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Board
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Segment
//      Field currmn 4	: Hotel
//      Field parentModule.name 4	: booking-site
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Hotel
//      Field currmn 4	: Hotel
//      Field AddingString 4	: BookingSite BookingSite
//      Field parentFieldType 4	: null
//      Field cmn 4	: Language
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Board
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Segment
//      Field currmn 4	: Hotel
//      Field parentModule.name 4	: booking-site
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Hotel
//      Field currmn 4	: Hotel
//      Field AddingString 4	: BookingSite BookingSite
//      Field parentFieldType 4	: null
//      Field cmn 4	: Language
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Board
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Segment
//      Field currmn 4	: Hotel
//      Field parentModule.name 4	: booking-site
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Hotel
//      Field currmn 4	: Hotel
//      Field AddingString 4	: BookingSite BookingSite
//      Field parentFieldType 4	: null
//      Field cmn 4	: Language
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Board
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Segment
//      Field currmn 4	: Hotel
//      Field parentModule.name 4	: booking-site
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Hotel
//      Field currmn 4	: Hotel
//      Field AddingString 4	: BookingSite BookingSite
//      Field parentFieldType 4	: null
//      Field cmn 4	: Language
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Board
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Segment
//      Field currmn 4	: Hotel
//      Field parentModule.name 4	: booking-site
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Hotel
//      Field currmn 4	: Hotel
//      Field AddingString 4	: BookingSite BookingSite
//      Field parentFieldType 4	: null
//      Field cmn 4	: Language
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Board
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Segment
//      Field currmn 4	: Hotel
//      Field parentModule.name 4	: booking-site
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Hotel
//      Field currmn 4	: Hotel
//      Field AddingString 4	: BookingSite BookingSite
//      Field parentFieldType 4	: null
//      Field cmn 4	: Language
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Board
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Segment
//      Field currmn 4	: Hotel
//      Field parentModule.name 4	: booking-site
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Hotel
//      Field currmn 4	: Hotel
//      Field AddingString 4	: BookingSite BookingSite
//      Field parentFieldType 4	: null
//      Field cmn 4	: Language
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Board
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Segment
//      Field currmn 4	: Hotel
//      Field parentModule.name 4	: booking-site
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Hotel
//      Field currmn 4	: Hotel
//      Field AddingString 4	: BookingSite BookingSite
//      Field parentFieldType 4	: null
//      Field cmn 4	: Language
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Board
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Segment
//      Field currmn 4	: Hotel
//      Field parentModule.name 4	: booking-site
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Hotel
//      Field currmn 4	: Hotel
//      Field AddingString 4	: BookingSite BookingSite
//      Field parentFieldType 4	: null
//      Field cmn 4	: Language
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Board
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Segment
//      Field currmn 4	: Hotel
//      Field parentModule.name 4	: booking-site
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Hotel
//      Field currmn 4	: Hotel
//      Field AddingString 4	: BookingSite BookingSite
//      Field parentFieldType 4	: null
//      Field cmn 4	: Language
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Board
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Segment
//      Field currmn 4	: Hotel
//      Field parentModule.name 4	: booking-site
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Hotel
//      Field currmn 4	: Hotel
//      Field AddingString 4	: BookingSite BookingSite
//      Field parentFieldType 4	: null
//      Field cmn 4	: Language
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Board
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Segment
//      Field currmn 4	: Hotel
//      Field parentModule.name 4	: booking-site
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Hotel
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Language
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Board
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Segment
//      Field currmn 4	: Hotel
//      Field parentModule.name 4	: booking-site
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Hotel
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Language
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Board
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Segment
//      Field currmn 4	: Hotel
//      Field parentModule.name 4	: booking-site
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Hotel
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Language
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Board
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Segment
//      Field currmn 4	: Hotel
//      Field parentModule.name 4	: booking-site
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Hotel
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Language
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Board
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Segment
//      Field currmn 4	: Hotel
//      Field parentModule.name 4	: booking-site
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Hotel
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Language
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Board
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Segment
//      Field currmn 4	: Hotel
//      Field parentModule.name 4	: booking-site
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Hotel
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Language
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Board
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Segment
//      Field currmn 4	: Hotel
//      Field parentModule.name 4	: booking-site
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Hotel
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Language
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Board
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Segment
//      Field currmn 4	: Hotel
//      Field parentModule.name 4	: booking-site
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Hotel
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Language
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Board
//      Field currmn 4	: Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: Segment
//      Field currmn 4	: Hotel
    }
}
