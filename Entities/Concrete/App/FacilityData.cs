/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:FacilityData.cs
   ThisFilePath:C:\Generated\booking-site\server\Entities\Concrete\App\FacilityData.cs
*/

using Core.Entities;
using System;
using System.Collections.Generic;

namespace Entities.Concrete
{
    public class FacilityData:IEntity
    {
        public int FacilityDataId { get; set; }
//      Field AddingString 1	: string Uuid
        public string Uuid { get; set; }
//      Field AddingString 2	: int Code
        public int Code { get; set; }
//      Field AddingString 2	: int FacilityGroupCode
        public int FacilityGroupCode { get; set; }
//      Field AddingString 2	: int FacilityTypologyCode
        public int FacilityTypologyCode { get; set; }
//      Field AddingString 2	: string FacilityUUID
        public string FacilityUUID { get; set; }
//      Field AddingString 2	: string RoomFacilityUUID
        public string RoomFacilityUUID { get; set; }
//      Field AddingString 2	: string RoomStayFacilityUUID
        public string RoomStayFacilityUUID { get; set; }
//      Field AddingString 2	: string InterestPointUUID
        public string InterestPointUUID { get; set; }


//      OneToOne Relation With	: Description
        public int DescriptionId { get; set; }
        public Description Description { get; set; }

//      OneToOne Relation With	: FacilityTypology
        public int FacilityTypologyId { get; set; }
        public FacilityTypology FacilityTypology { get; set; }

//      OneToOne Relation With	: FacilityGroup
        public int FacilityGroupId { get; set; }
        public FacilityGroup FacilityGroup { get; set; }

//      Field parentModule.name 4	: facility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityData
//      Field currmn 4	: FacilityData
//      Field AddingString 4	: Facility Facility
        public Facility Facility { get; set; }
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentModule.name 4	: interest-point
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityData
//      Field currmn 4	: FacilityData
//      Field AddingString 4	: InterestPoint InterestPoint
        public InterestPoint InterestPoint { get; set; }
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentModule.name 4	: room-facility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityData
//      Field currmn 4	: FacilityData
//      Field AddingString 4	: RoomFacility RoomFacility
        public RoomFacility RoomFacility { get; set; }
//      Field parentModule.name 4	: room-stay-facility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityData
//      Field currmn 4	: FacilityData
//      Field AddingString 4	: RoomStayFacility RoomStayFacility
        public RoomStayFacility RoomStayFacility { get; set; }
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentModule.name 4	: facility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityData
//      Field currmn 4	: FacilityData
//      Field AddingString 4	: Facility Facility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentModule.name 4	: interest-point
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityData
//      Field currmn 4	: FacilityData
//      Field AddingString 4	: InterestPoint InterestPoint
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentModule.name 4	: room-facility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityData
//      Field currmn 4	: FacilityData
//      Field AddingString 4	: RoomFacility RoomFacility
//      Field parentModule.name 4	: room-stay-facility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityData
//      Field currmn 4	: FacilityData
//      Field AddingString 4	: RoomStayFacility RoomStayFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentModule.name 4	: facility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityData
//      Field currmn 4	: FacilityData
//      Field AddingString 4	: Facility Facility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentModule.name 4	: interest-point
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityData
//      Field currmn 4	: FacilityData
//      Field AddingString 4	: InterestPoint InterestPoint
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentModule.name 4	: room-facility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityData
//      Field currmn 4	: FacilityData
//      Field AddingString 4	: RoomFacility RoomFacility
//      Field parentModule.name 4	: room-stay-facility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityData
//      Field currmn 4	: FacilityData
//      Field AddingString 4	: RoomStayFacility RoomStayFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentModule.name 4	: facility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityData
//      Field currmn 4	: FacilityData
//      Field AddingString 4	: Facility Facility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentModule.name 4	: interest-point
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityData
//      Field currmn 4	: FacilityData
//      Field AddingString 4	: InterestPoint InterestPoint
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentModule.name 4	: room-facility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityData
//      Field currmn 4	: FacilityData
//      Field AddingString 4	: RoomFacility RoomFacility
//      Field parentModule.name 4	: room-stay-facility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityData
//      Field currmn 4	: FacilityData
//      Field AddingString 4	: RoomStayFacility RoomStayFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentModule.name 4	: facility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityData
//      Field currmn 4	: FacilityData
//      Field AddingString 4	: Facility Facility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentModule.name 4	: interest-point
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityData
//      Field currmn 4	: FacilityData
//      Field AddingString 4	: InterestPoint InterestPoint
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentModule.name 4	: room-facility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityData
//      Field currmn 4	: FacilityData
//      Field AddingString 4	: RoomFacility RoomFacility
//      Field parentModule.name 4	: room-stay-facility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityData
//      Field currmn 4	: FacilityData
//      Field AddingString 4	: RoomStayFacility RoomStayFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentModule.name 4	: facility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityData
//      Field currmn 4	: FacilityData
//      Field AddingString 4	: Facility Facility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentModule.name 4	: interest-point
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityData
//      Field currmn 4	: FacilityData
//      Field AddingString 4	: InterestPoint InterestPoint
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentModule.name 4	: room-facility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityData
//      Field currmn 4	: FacilityData
//      Field AddingString 4	: RoomFacility RoomFacility
//      Field parentModule.name 4	: room-stay-facility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityData
//      Field currmn 4	: FacilityData
//      Field AddingString 4	: RoomStayFacility RoomStayFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentModule.name 4	: facility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityData
//      Field currmn 4	: FacilityData
//      Field AddingString 4	: Facility Facility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentModule.name 4	: interest-point
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityData
//      Field currmn 4	: FacilityData
//      Field AddingString 4	: InterestPoint InterestPoint
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentModule.name 4	: room-facility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityData
//      Field currmn 4	: FacilityData
//      Field AddingString 4	: RoomFacility RoomFacility
//      Field parentModule.name 4	: room-stay-facility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityData
//      Field currmn 4	: FacilityData
//      Field AddingString 4	: RoomStayFacility RoomStayFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentModule.name 4	: facility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityData
//      Field currmn 4	: FacilityData
//      Field AddingString 4	: Facility Facility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentModule.name 4	: interest-point
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityData
//      Field currmn 4	: FacilityData
//      Field AddingString 4	: InterestPoint InterestPoint
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentModule.name 4	: room-facility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityData
//      Field currmn 4	: FacilityData
//      Field AddingString 4	: RoomFacility RoomFacility
//      Field parentModule.name 4	: room-stay-facility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityData
//      Field currmn 4	: FacilityData
//      Field AddingString 4	: RoomStayFacility RoomStayFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentModule.name 4	: facility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityData
//      Field currmn 4	: FacilityData
//      Field AddingString 4	: Facility Facility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentModule.name 4	: interest-point
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityData
//      Field currmn 4	: FacilityData
//      Field AddingString 4	: InterestPoint InterestPoint
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentModule.name 4	: room-facility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityData
//      Field currmn 4	: FacilityData
//      Field AddingString 4	: RoomFacility RoomFacility
//      Field parentModule.name 4	: room-stay-facility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityData
//      Field currmn 4	: FacilityData
//      Field AddingString 4	: RoomStayFacility RoomStayFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentModule.name 4	: facility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityData
//      Field currmn 4	: FacilityData
//      Field AddingString 4	: Facility Facility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentModule.name 4	: interest-point
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityData
//      Field currmn 4	: FacilityData
//      Field AddingString 4	: InterestPoint InterestPoint
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentModule.name 4	: room-facility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityData
//      Field currmn 4	: FacilityData
//      Field AddingString 4	: RoomFacility RoomFacility
//      Field parentModule.name 4	: room-stay-facility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityData
//      Field currmn 4	: FacilityData
//      Field AddingString 4	: RoomStayFacility RoomStayFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentModule.name 4	: facility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityData
//      Field currmn 4	: FacilityData
//      Field AddingString 4	: Facility Facility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentModule.name 4	: interest-point
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityData
//      Field currmn 4	: FacilityData
//      Field AddingString 4	: InterestPoint InterestPoint
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentModule.name 4	: room-facility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityData
//      Field currmn 4	: FacilityData
//      Field AddingString 4	: RoomFacility RoomFacility
//      Field parentModule.name 4	: room-stay-facility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityData
//      Field currmn 4	: FacilityData
//      Field AddingString 4	: RoomStayFacility RoomStayFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityData
    }
}
