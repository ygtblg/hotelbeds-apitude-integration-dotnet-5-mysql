/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:HotelRoomDescription.cs
   ThisFilePath:C:\Generated\booking-site\server\Entities\Concrete\App\HotelRoomDescription.cs
*/

using Core.Entities;
using System;
using System.Collections.Generic;

namespace Entities.Concrete
{
    public class HotelRoomDescription:IEntity
    {
        public int HotelRoomDescriptionId { get; set; }
//      Field AddingString 1	: string Uuid
        public string Uuid { get; set; }
//      Field AddingString 2	: string HotelRoomDescriptionField
        public string HotelRoomDescriptionField { get; set; }
//      Field AddingString 2	: string Content
        public string Content { get; set; }
//      Field AddingString 2	: string WildcardUUID
        public string WildcardUUID { get; set; }


//      Field parentModule.name 4	: wildcard
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: HotelRoomDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: HotelRoomDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: HotelRoomDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: HotelRoomDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: HotelRoomDescription
//      Field currmn 4	: HotelRoomDescription
//      Field AddingString 4	: Wildcard Wildcard
        public Wildcard Wildcard { get; set; }
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: HotelRoomDescription
//      Field parentModule.name 4	: wildcard
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: HotelRoomDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: HotelRoomDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: HotelRoomDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: HotelRoomDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: HotelRoomDescription
//      Field currmn 4	: HotelRoomDescription
//      Field AddingString 4	: Wildcard Wildcard
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: HotelRoomDescription
//      Field parentModule.name 4	: wildcard
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: HotelRoomDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: HotelRoomDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: HotelRoomDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: HotelRoomDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: HotelRoomDescription
//      Field currmn 4	: HotelRoomDescription
//      Field AddingString 4	: Wildcard Wildcard
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: HotelRoomDescription
//      Field parentModule.name 4	: wildcard
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: HotelRoomDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: HotelRoomDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: HotelRoomDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: HotelRoomDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: HotelRoomDescription
//      Field currmn 4	: HotelRoomDescription
//      Field AddingString 4	: Wildcard Wildcard
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: HotelRoomDescription
    }
}
