/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:TypeDescription.cs
   ThisFilePath:C:\Generated\booking-site\server\Entities\Concrete\App\TypeDescription.cs
*/

using Core.Entities;
using System;
using System.Collections.Generic;

namespace Entities.Concrete
{
    public class TypeDescription:IEntity
    {
        public int TypeDescriptionId { get; set; }
//      Field AddingString 1	: string Uuid
        public string Uuid { get; set; }
//      Field AddingString 2	: string TypeDescriptionField
        public string TypeDescriptionField { get; set; }
//      Field AddingString 2	: string LanguageCode
        public string LanguageCode { get; set; }
//      Field AddingString 2	: string Content
        public string Content { get; set; }
//      Field AddingString 2	: string RoomDataUUID
        public string RoomDataUUID { get; set; }


//      Field parentModule.name 4	: room-data
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TypeDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TypeDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TypeDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TypeDescription
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TypeDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: TypeDescription
//      Field currmn 4	: TypeDescription
//      Field AddingString 4	: RoomData RoomData
        public RoomData RoomData { get; set; }
//      Field parentFieldType 4	: null
//      Field cmn 4	: CharacteristicDescription
//      Field currmn 4	: TypeDescription
//      Field parentModule.name 4	: room-data
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TypeDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TypeDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TypeDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TypeDescription
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TypeDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: TypeDescription
//      Field currmn 4	: TypeDescription
//      Field AddingString 4	: RoomData RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: CharacteristicDescription
//      Field currmn 4	: TypeDescription
//      Field parentModule.name 4	: room-data
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TypeDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TypeDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TypeDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TypeDescription
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TypeDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: TypeDescription
//      Field currmn 4	: TypeDescription
//      Field AddingString 4	: RoomData RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: CharacteristicDescription
//      Field currmn 4	: TypeDescription
//      Field parentModule.name 4	: room-data
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TypeDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TypeDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TypeDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TypeDescription
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TypeDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: TypeDescription
//      Field currmn 4	: TypeDescription
//      Field AddingString 4	: RoomData RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: CharacteristicDescription
//      Field currmn 4	: TypeDescription
//      Field parentModule.name 4	: room-data
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TypeDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TypeDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TypeDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TypeDescription
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TypeDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: TypeDescription
//      Field currmn 4	: TypeDescription
//      Field AddingString 4	: RoomData RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: CharacteristicDescription
//      Field currmn 4	: TypeDescription
    }
}
