/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:RoomStay.cs
   ThisFilePath:C:\Generated\booking-site\server\Entities\Concrete\App\RoomStay.cs
*/

using Core.Entities;
using System;
using System.Collections.Generic;

namespace Entities.Concrete
{
    public class RoomStay:IEntity
    {
        public int RoomStayId { get; set; }
//      Field AddingString 1	: string Uuid
        public string Uuid { get; set; }
//      Field AddingString 2	: string StayType
        public string StayType { get; set; }
//      Field AddingString 2	: string Order
        public string Order { get; set; }
//      Field AddingString 2	: string Description
        public string Description { get; set; }
// 1        IRoomStayFacility
// 2        IRoomStayFacility
        public ICollection<RoomStayFacility> RoomStayFacilities { get; set; }
//      Field AddingString 2	: string RoomUUID
        public string RoomUUID { get; set; }


//      OneToMany Relation With	: Room
        public string RoomCode { get; set; }
//      Field AddingString 3	: Room Room
        public Room Room { get; set; } 

//      Field parentModule.name 4	: room
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStay
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStay
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStay
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStay
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomFacility
//      Field currmn 4	: RoomStay
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomStay
//      Field currmn 4	: RoomStay
//      Field AddingString 4	: Room Room
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomData
//      Field currmn 4	: RoomStay
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStay
//      Field parentModule.name 4	: room
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStay
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStay
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStay
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStay
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomFacility
//      Field currmn 4	: RoomStay
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomStay
//      Field currmn 4	: RoomStay
//      Field AddingString 4	: Room Room
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomData
//      Field currmn 4	: RoomStay
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStay
//      Field parentModule.name 4	: room
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStay
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStay
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStay
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStay
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomFacility
//      Field currmn 4	: RoomStay
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomStay
//      Field currmn 4	: RoomStay
//      Field AddingString 4	: Room Room
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomData
//      Field currmn 4	: RoomStay
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStay
//      Field parentModule.name 4	: room
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStay
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStay
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStay
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStay
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomFacility
//      Field currmn 4	: RoomStay
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomStay
//      Field currmn 4	: RoomStay
//      Field AddingString 4	: Room Room
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomData
//      Field currmn 4	: RoomStay
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStay
//      Field parentModule.name 4	: room
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStay
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStay
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStay
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStay
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomFacility
//      Field currmn 4	: RoomStay
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomStay
//      Field currmn 4	: RoomStay
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomData
//      Field currmn 4	: RoomStay
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStay
//      Field parentModule.name 4	: room
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStay
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStay
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStay
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStay
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomFacility
//      Field currmn 4	: RoomStay
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomStay
//      Field currmn 4	: RoomStay
//      Field AddingString 4	: Room Room
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomData
//      Field currmn 4	: RoomStay
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStay
    }
}
