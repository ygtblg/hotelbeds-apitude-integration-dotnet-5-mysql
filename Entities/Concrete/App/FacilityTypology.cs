/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:FacilityTypology.cs
   ThisFilePath:C:\Generated\booking-site\server\Entities\Concrete\App\FacilityTypology.cs
*/

using Core.Entities;
using System;
using System.Collections.Generic;

namespace Entities.Concrete
{
    public class FacilityTypology:IEntity
    {
        public int FacilityTypologyId { get; set; }
//      Field AddingString 1	: string Uuid
        public string Uuid { get; set; }
//      Field AddingString 2	: int Code
        public int Code { get; set; }
//      Field AddingString 2	: bool NumberFlag
        public bool NumberFlag { get; set; }
//      Field AddingString 2	: bool LogicFlag
        public bool LogicFlag { get; set; }
//      Field AddingString 2	: bool FeeFlag
        public bool FeeFlag { get; set; }
//      Field AddingString 2	: bool DistanceFlag
        public bool DistanceFlag { get; set; }
//      Field AddingString 2	: bool AgeFromFlag
        public bool AgeFromFlag { get; set; }
//      Field AddingString 2	: bool AgeToFlag
        public bool AgeToFlag { get; set; }
//      Field AddingString 2	: bool DateFromFlag
        public bool DateFromFlag { get; set; }
//      Field AddingString 2	: bool DateToFlag
        public bool DateToFlag { get; set; }
//      Field AddingString 2	: bool TimeFromFlag
        public bool TimeFromFlag { get; set; }
//      Field AddingString 2	: bool TimeToFlag
        public bool TimeToFlag { get; set; }
//      Field AddingString 2	: bool IndYesOrNoFlag
        public bool IndYesOrNoFlag { get; set; }
//      Field AddingString 2	: bool AmountFlag
        public bool AmountFlag { get; set; }
//      Field AddingString 2	: bool CurrencyFlag
        public bool CurrencyFlag { get; set; }
//      Field AddingString 2	: bool AppTypeFlag
        public bool AppTypeFlag { get; set; }
//      Field AddingString 2	: bool TextFlag
        public bool TextFlag { get; set; }
//      Field AddingString 2	: string FacilityDataUUID
        public string FacilityDataUUID { get; set; }


//      Field parentModule.name 4	: facility-data
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Description
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityTypology
//      Field currmn 4	: FacilityTypology
//      Field AddingString 4	: FacilityData FacilityData
        public FacilityData FacilityData { get; set; }
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityGroup
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentModule.name 4	: facility-data
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Description
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityTypology
//      Field currmn 4	: FacilityTypology
//      Field AddingString 4	: FacilityData FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityGroup
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentModule.name 4	: facility-data
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Description
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityTypology
//      Field currmn 4	: FacilityTypology
//      Field AddingString 4	: FacilityData FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityGroup
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentModule.name 4	: facility-data
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Description
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityTypology
//      Field currmn 4	: FacilityTypology
//      Field AddingString 4	: FacilityData FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityGroup
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentModule.name 4	: facility-data
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Description
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityTypology
//      Field currmn 4	: FacilityTypology
//      Field AddingString 4	: FacilityData FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityGroup
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentModule.name 4	: facility-data
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Description
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityTypology
//      Field currmn 4	: FacilityTypology
//      Field AddingString 4	: FacilityData FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityGroup
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentModule.name 4	: facility-data
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Description
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityTypology
//      Field currmn 4	: FacilityTypology
//      Field AddingString 4	: FacilityData FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityGroup
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentModule.name 4	: facility-data
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Description
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityTypology
//      Field currmn 4	: FacilityTypology
//      Field AddingString 4	: FacilityData FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityGroup
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentModule.name 4	: facility-data
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Description
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityTypology
//      Field currmn 4	: FacilityTypology
//      Field AddingString 4	: FacilityData FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityGroup
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentModule.name 4	: facility-data
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Description
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityTypology
//      Field currmn 4	: FacilityTypology
//      Field AddingString 4	: FacilityData FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityGroup
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentModule.name 4	: facility-data
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Description
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityTypology
//      Field currmn 4	: FacilityTypology
//      Field AddingString 4	: FacilityData FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityGroup
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentModule.name 4	: facility-data
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Description
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityTypology
//      Field currmn 4	: FacilityTypology
//      Field AddingString 4	: FacilityData FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityGroup
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentModule.name 4	: facility-data
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Description
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityTypology
//      Field currmn 4	: FacilityTypology
//      Field AddingString 4	: FacilityData FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityGroup
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentModule.name 4	: facility-data
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Description
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityTypology
//      Field currmn 4	: FacilityTypology
//      Field AddingString 4	: FacilityData FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityGroup
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentModule.name 4	: facility-data
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Description
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityTypology
//      Field currmn 4	: FacilityTypology
//      Field AddingString 4	: FacilityData FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityGroup
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentModule.name 4	: facility-data
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Description
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityTypology
//      Field currmn 4	: FacilityTypology
//      Field AddingString 4	: FacilityData FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityGroup
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentModule.name 4	: facility-data
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Description
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityTypology
//      Field currmn 4	: FacilityTypology
//      Field AddingString 4	: FacilityData FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityGroup
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentModule.name 4	: facility-data
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Description
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityTypology
//      Field currmn 4	: FacilityTypology
//      Field AddingString 4	: FacilityData FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityGroup
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityTypology
    }
}
