/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:TypeMultiDescription.cs
   ThisFilePath:C:\Generated\booking-site\server\Entities\Concrete\App\TypeMultiDescription.cs
*/

using Core.Entities;
using System;
using System.Collections.Generic;

namespace Entities.Concrete
{
    public class TypeMultiDescription:IEntity
    {
        public int TypeMultiDescriptionId { get; set; }
//      Field AddingString 1	: string Uuid
        public string Uuid { get; set; }
//      Field AddingString 2	: string LanguageCode
        public string LanguageCode { get; set; }
//      Field AddingString 2	: string Content
        public string Content { get; set; }
//      Field AddingString 2	: string AccommodationUUID
        public string AccommodationUUID { get; set; }


//      Field parentModule.name 4	: accommodation
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TypeMultiDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TypeMultiDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: TypeMultiDescription
//      Field currmn 4	: TypeMultiDescription
//      Field AddingString 4	: Accommodation Accommodation
        public Accommodation Accommodation { get; set; }
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TypeMultiDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TypeMultiDescription
//      Field parentModule.name 4	: accommodation
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TypeMultiDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TypeMultiDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: TypeMultiDescription
//      Field currmn 4	: TypeMultiDescription
//      Field AddingString 4	: Accommodation Accommodation
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TypeMultiDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TypeMultiDescription
//      Field parentModule.name 4	: accommodation
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TypeMultiDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TypeMultiDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: TypeMultiDescription
//      Field currmn 4	: TypeMultiDescription
//      Field AddingString 4	: Accommodation Accommodation
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TypeMultiDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TypeMultiDescription
//      Field parentModule.name 4	: accommodation
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TypeMultiDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TypeMultiDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: TypeMultiDescription
//      Field currmn 4	: TypeMultiDescription
//      Field AddingString 4	: Accommodation Accommodation
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TypeMultiDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TypeMultiDescription
    }
}
