/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:Language.cs
   ThisFilePath:C:\Generated\booking-site\server\Entities\Concrete\App\Language.cs
*/

using Core.Entities;
using System;
using System.Collections.Generic;

namespace Entities.Concrete
{
    public class Language:IEntity
    {
        public int LanguageId { get; set; }
//      Field AddingString 1	: string Uuid
        public string Uuid { get; set; }
//      Field AddingString 2	: string Code
        public string Code { get; set; }
//      Field AddingString 2	: string Name
        public string Name { get; set; }
//      Field AddingString 2	: string BookingSiteUUID
        public string BookingSiteUUID { get; set; }


//      OneToMany Relation With	: BookingSite
        public int BookingSiteId { get; set; }
//      Field AddingString 3	: BookingSite BookingSite
        public BookingSite BookingSite { get; set; } 

//      OneToOne Relation With	: Description
        public int DescriptionId { get; set; }
        public Description Description { get; set; }

//      Field parentModule.name 4	: booking-site
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Language
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Language
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Language
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Hotel
//      Field currmn 4	: Language
//      Field parentFieldType 4	: null
//      Field cmn 4	: Language
//      Field currmn 4	: Language
//      Field AddingString 4	: BookingSite BookingSite
//      Field parentFieldType 4	: null
//      Field cmn 4	: Board
//      Field currmn 4	: Language
//      Field parentFieldType 4	: null
//      Field cmn 4	: Segment
//      Field currmn 4	: Language
//      Field parentModule.name 4	: booking-site
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Language
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Language
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Language
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Hotel
//      Field currmn 4	: Language
//      Field parentFieldType 4	: null
//      Field cmn 4	: Language
//      Field currmn 4	: Language
//      Field AddingString 4	: BookingSite BookingSite
//      Field parentFieldType 4	: null
//      Field cmn 4	: Board
//      Field currmn 4	: Language
//      Field parentFieldType 4	: null
//      Field cmn 4	: Segment
//      Field currmn 4	: Language
//      Field parentModule.name 4	: booking-site
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Language
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Language
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Language
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Hotel
//      Field currmn 4	: Language
//      Field parentFieldType 4	: null
//      Field cmn 4	: Language
//      Field currmn 4	: Language
//      Field AddingString 4	: BookingSite BookingSite
//      Field parentFieldType 4	: null
//      Field cmn 4	: Board
//      Field currmn 4	: Language
//      Field parentFieldType 4	: null
//      Field cmn 4	: Segment
//      Field currmn 4	: Language
//      Field parentModule.name 4	: booking-site
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Language
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Language
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Language
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Hotel
//      Field currmn 4	: Language
//      Field parentFieldType 4	: null
//      Field cmn 4	: Language
//      Field currmn 4	: Language
//      Field AddingString 4	: BookingSite BookingSite
//      Field parentFieldType 4	: null
//      Field cmn 4	: Board
//      Field currmn 4	: Language
//      Field parentFieldType 4	: null
//      Field cmn 4	: Segment
//      Field currmn 4	: Language
//      Field parentModule.name 4	: booking-site
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Language
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Language
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Language
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Hotel
//      Field currmn 4	: Language
//      Field parentFieldType 4	: null
//      Field cmn 4	: Language
//      Field currmn 4	: Language
//      Field AddingString 4	: BookingSite BookingSite
//      Field parentFieldType 4	: null
//      Field cmn 4	: Board
//      Field currmn 4	: Language
//      Field parentFieldType 4	: null
//      Field cmn 4	: Segment
//      Field currmn 4	: Language
    }
}
