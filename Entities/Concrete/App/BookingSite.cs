/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:BookingSite.cs
   ThisFilePath:C:\Generated\booking-site\server\Entities\Concrete\App\BookingSite.cs
*/

using Core.Entities;
using System;
using System.Collections.Generic;

namespace Entities.Concrete
{
    public class BookingSite:IEntity
    {
        public int BookingSiteId { get; set; }
//      Field AddingString 1	: string Uuid
        public string Uuid { get; set; }
//      Field AddingString 2	: bool LoggedIn
        public bool LoggedIn { get; set; }
//      Field AddingString 2	: string Error
        public string Error { get; set; }
//      Field AddingString 2	: string Success
        public string Success { get; set; }
//      Field AddingString 2	: bool IsRunningMobile
        public bool IsRunningMobile { get; set; }
//      Field AddingString 2	: int WindowWidth
        public int WindowWidth { get; set; }
//      Field AddingString 2	: int WindowHeight
        public int WindowHeight { get; set; }
// 1        IHotel
// 2        IHotel
        public ICollection<Hotel> Hotels { get; set; }
// 1        ILanguage
// 2        ILanguage
        public ICollection<Language> Languages { get; set; }
// 1        IBoard
// 2        IBoard
        public ICollection<Board> Boards { get; set; }
// 1        ISegment
// 2        ISegment
        public ICollection<Segment> Segments { get; set; }


    }
}
