/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:FacilityGroup.cs
   ThisFilePath:C:\Generated\booking-site\server\Entities\Concrete\App\FacilityGroup.cs
*/

using Core.Entities;
using System;
using System.Collections.Generic;

namespace Entities.Concrete
{
    public class FacilityGroup:IEntity
    {
        public int FacilityGroupId { get; set; }
//      Field AddingString 1	: string Uuid
        public string Uuid { get; set; }
//      Field AddingString 2	: int Code
        public int Code { get; set; }
//      Field AddingString 2	: string FacilityDataUUID
        public string FacilityDataUUID { get; set; }


//      OneToOne Relation With	: Description
        public int DescriptionId { get; set; }
        public Description Description { get; set; }

//      Field parentModule.name 4	: facility-data
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityGroup
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Description
//      Field currmn 4	: FacilityGroup
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityTypology
//      Field currmn 4	: FacilityGroup
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityGroup
//      Field currmn 4	: FacilityGroup
//      Field AddingString 4	: FacilityData FacilityData
        public FacilityData FacilityData { get; set; }
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityGroup
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityGroup
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityGroup
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityGroup
//      Field parentModule.name 4	: facility-data
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityGroup
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Description
//      Field currmn 4	: FacilityGroup
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityTypology
//      Field currmn 4	: FacilityGroup
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityGroup
//      Field currmn 4	: FacilityGroup
//      Field AddingString 4	: FacilityData FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityGroup
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityGroup
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityGroup
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityGroup
//      Field parentModule.name 4	: facility-data
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityGroup
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Description
//      Field currmn 4	: FacilityGroup
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityTypology
//      Field currmn 4	: FacilityGroup
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityGroup
//      Field currmn 4	: FacilityGroup
//      Field AddingString 4	: FacilityData FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityGroup
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityGroup
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityGroup
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityGroup
//      Field parentModule.name 4	: facility-data
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityGroup
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Description
//      Field currmn 4	: FacilityGroup
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityTypology
//      Field currmn 4	: FacilityGroup
//      Field parentFieldType 4	: null
//      Field cmn 4	: FacilityGroup
//      Field currmn 4	: FacilityGroup
//      Field AddingString 4	: FacilityData FacilityData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityGroup
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityGroup
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityGroup
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: FacilityGroup
    }
}
