/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:ImageType.cs
   ThisFilePath:C:\Generated\booking-site\server\Entities\Concrete\App\ImageType.cs
*/

using Core.Entities;
using System;
using System.Collections.Generic;

namespace Entities.Concrete
{
    public class ImageType:IEntity
    {
        public int ImageTypeId { get; set; }
//      Field AddingString 1	: string Uuid
        public string Uuid { get; set; }
//      Field AddingString 2	: string Code
        public string Code { get; set; }
//      Field AddingString 2	: string ImageUUID
        public string ImageUUID { get; set; }


//      OneToOne Relation With	: Description
        public int DescriptionId { get; set; }
        public Description Description { get; set; }

//      Field parentModule.name 4	: image
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: ImageType
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: ImageType
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: ImageType
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: ImageType
//      Field currmn 4	: ImageType
//      Field AddingString 4	: Image Image
        public Image Image { get; set; }
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: ImageType
//      Field parentModule.name 4	: image
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: ImageType
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: ImageType
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: ImageType
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: ImageType
//      Field currmn 4	: ImageType
//      Field AddingString 4	: Image Image
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: ImageType
//      Field parentModule.name 4	: image
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: ImageType
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: ImageType
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: ImageType
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: ImageType
//      Field currmn 4	: ImageType
//      Field AddingString 4	: Image Image
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: ImageType
//      Field parentModule.name 4	: image
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: ImageType
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: ImageType
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: ImageType
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: ImageType
//      Field currmn 4	: ImageType
//      Field AddingString 4	: Image Image
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: ImageType
    }
}
