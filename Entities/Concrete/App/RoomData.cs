/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:RoomData.cs
   ThisFilePath:C:\Generated\booking-site\server\Entities\Concrete\App\RoomData.cs
*/

using Core.Entities;
using System;
using System.Collections.Generic;

namespace Entities.Concrete
{
    public class RoomData:IEntity
    {
        public int RoomDataId { get; set; }
//      Field AddingString 1	: string Uuid
        public string Uuid { get; set; }
//      Field AddingString 2	: string Code
        public string Code { get; set; }
//      Field AddingString 2	: string Type
        public string Type { get; set; }
//      Field AddingString 2	: string Characteristic
        public string Characteristic { get; set; }
//      Field AddingString 2	: int MinPax
        public int MinPax { get; set; }
//      Field AddingString 2	: int MaxPax
        public int MaxPax { get; set; }
//      Field AddingString 2	: int MaxAdults
        public int MaxAdults { get; set; }
//      Field AddingString 2	: int MaxChildren
        public int MaxChildren { get; set; }
//      Field AddingString 2	: int MinAdults
        public int MinAdults { get; set; }
//      Field AddingString 2	: string Description
        public string Description { get; set; }


//      OneToOne Relation With	: TypeDescription
        public int TypeDescriptionId { get; set; }
        public TypeDescription TypeDescription { get; set; }

//      OneToOne Relation With	: CharacteristicDescription
        public int CharacteristicDescriptionId { get; set; }
        public CharacteristicDescription CharacteristicDescription { get; set; }

//      Field parentModule.name 4	: room
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomFacility
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomStay
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomData
//      Field currmn 4	: RoomData
//      Field AddingString 4	: Room Room
        public Room Room { get; set; }
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentModule.name 4	: room
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomFacility
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomStay
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomData
//      Field currmn 4	: RoomData
//      Field AddingString 4	: Room Room
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentModule.name 4	: room
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomFacility
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomStay
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomData
//      Field currmn 4	: RoomData
//      Field AddingString 4	: Room Room
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentModule.name 4	: room
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomFacility
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomStay
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomData
//      Field currmn 4	: RoomData
//      Field AddingString 4	: Room Room
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentModule.name 4	: room
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomFacility
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomStay
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomData
//      Field currmn 4	: RoomData
//      Field AddingString 4	: Room Room
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentModule.name 4	: room
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomFacility
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomStay
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomData
//      Field currmn 4	: RoomData
//      Field AddingString 4	: Room Room
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentModule.name 4	: room
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomFacility
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomStay
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomData
//      Field currmn 4	: RoomData
//      Field AddingString 4	: Room Room
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentModule.name 4	: room
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomFacility
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomStay
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomData
//      Field currmn 4	: RoomData
//      Field AddingString 4	: Room Room
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentModule.name 4	: room
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomFacility
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomStay
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomData
//      Field currmn 4	: RoomData
//      Field AddingString 4	: Room Room
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentModule.name 4	: room
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomFacility
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomStay
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomData
//      Field currmn 4	: RoomData
//      Field AddingString 4	: Room Room
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentModule.name 4	: room
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomFacility
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomStay
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomData
//      Field currmn 4	: RoomData
//      Field AddingString 4	: Room Room
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentModule.name 4	: room
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomFacility
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomStay
//      Field currmn 4	: RoomData
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomData
//      Field currmn 4	: RoomData
//      Field AddingString 4	: Room Room
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomData
    }
}
