/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:CharacteristicDescription.cs
   ThisFilePath:C:\Generated\booking-site\server\Entities\Concrete\App\CharacteristicDescription.cs
*/

using Core.Entities;
using System;
using System.Collections.Generic;

namespace Entities.Concrete
{
    public class CharacteristicDescription:IEntity
    {
        public int CharacteristicDescriptionId { get; set; }
//      Field AddingString 1	: string Uuid
        public string Uuid { get; set; }
//      Field AddingString 2	: string CharacteristicDescriptionField
        public string CharacteristicDescriptionField { get; set; }
//      Field AddingString 2	: string LanguageCode
        public string LanguageCode { get; set; }
//      Field AddingString 2	: string Content
        public string Content { get; set; }
//      Field AddingString 2	: string RoomDataUUID
        public string RoomDataUUID { get; set; }


//      Field parentModule.name 4	: room-data
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: CharacteristicDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: CharacteristicDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: CharacteristicDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: CharacteristicDescription
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: CharacteristicDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: TypeDescription
//      Field currmn 4	: CharacteristicDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: CharacteristicDescription
//      Field currmn 4	: CharacteristicDescription
//      Field AddingString 4	: RoomData RoomData
        public RoomData RoomData { get; set; }
//      Field parentModule.name 4	: room-data
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: CharacteristicDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: CharacteristicDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: CharacteristicDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: CharacteristicDescription
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: CharacteristicDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: TypeDescription
//      Field currmn 4	: CharacteristicDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: CharacteristicDescription
//      Field currmn 4	: CharacteristicDescription
//      Field AddingString 4	: RoomData RoomData
//      Field parentModule.name 4	: room-data
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: CharacteristicDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: CharacteristicDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: CharacteristicDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: CharacteristicDescription
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: CharacteristicDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: TypeDescription
//      Field currmn 4	: CharacteristicDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: CharacteristicDescription
//      Field currmn 4	: CharacteristicDescription
//      Field AddingString 4	: RoomData RoomData
//      Field parentModule.name 4	: room-data
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: CharacteristicDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: CharacteristicDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: CharacteristicDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: CharacteristicDescription
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: CharacteristicDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: TypeDescription
//      Field currmn 4	: CharacteristicDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: CharacteristicDescription
//      Field currmn 4	: CharacteristicDescription
//      Field AddingString 4	: RoomData RoomData
//      Field parentModule.name 4	: room-data
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: CharacteristicDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: CharacteristicDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: CharacteristicDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: CharacteristicDescription
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: CharacteristicDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: TypeDescription
//      Field currmn 4	: CharacteristicDescription
//      Field parentFieldType 4	: null
//      Field cmn 4	: CharacteristicDescription
//      Field currmn 4	: CharacteristicDescription
//      Field AddingString 4	: RoomData RoomData
    }
}
