/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:RoomFacility.cs
   ThisFilePath:C:\Generated\booking-site\server\Entities\Concrete\App\RoomFacility.cs
*/

using Core.Entities;
using System;
using System.Collections.Generic;

namespace Entities.Concrete
{
    public class RoomFacility:IEntity
    {
        public int RoomFacilityId { get; set; }
//      Field AddingString 1	: string Uuid
        public string Uuid { get; set; }
//      Field AddingString 2	: int FacilityCode
        public int FacilityCode { get; set; }
//      Field AddingString 2	: int FacilityGroupCode
        public int FacilityGroupCode { get; set; }
//      Field AddingString 2	: bool IndLogic
        public bool IndLogic { get; set; }
//      Field AddingString 2	: int Number
        public int Number { get; set; }
//      Field AddingString 2	: bool Voucher
        public bool Voucher { get; set; }
//      Field AddingString 2	: string RoomUUID
        public string RoomUUID { get; set; }


//      OneToMany Relation With	: Room
        public string RoomCode { get; set; }
//      Field AddingString 3	: Room Room
        public Room Room { get; set; } 

//      OneToOne Relation With	: FacilityData
        public int FacilityDataId { get; set; }
        public FacilityData FacilityData { get; set; }

//      Field parentModule.name 4	: room
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomFacility
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomFacility
//      Field currmn 4	: RoomFacility
//      Field AddingString 4	: Room Room
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomStay
//      Field currmn 4	: RoomFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomData
//      Field currmn 4	: RoomFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomFacility
//      Field parentModule.name 4	: room
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomFacility
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomFacility
//      Field currmn 4	: RoomFacility
//      Field AddingString 4	: Room Room
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomStay
//      Field currmn 4	: RoomFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomData
//      Field currmn 4	: RoomFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomFacility
//      Field parentModule.name 4	: room
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomFacility
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomFacility
//      Field currmn 4	: RoomFacility
//      Field AddingString 4	: Room Room
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomStay
//      Field currmn 4	: RoomFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomData
//      Field currmn 4	: RoomFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomFacility
//      Field parentModule.name 4	: room
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomFacility
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomFacility
//      Field currmn 4	: RoomFacility
//      Field AddingString 4	: Room Room
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomStay
//      Field currmn 4	: RoomFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomData
//      Field currmn 4	: RoomFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomFacility
//      Field parentModule.name 4	: room
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomFacility
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomFacility
//      Field currmn 4	: RoomFacility
//      Field AddingString 4	: Room Room
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomStay
//      Field currmn 4	: RoomFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomData
//      Field currmn 4	: RoomFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomFacility
//      Field parentModule.name 4	: room
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomFacility
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomFacility
//      Field currmn 4	: RoomFacility
//      Field AddingString 4	: Room Room
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomStay
//      Field currmn 4	: RoomFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomData
//      Field currmn 4	: RoomFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomFacility
//      Field parentModule.name 4	: room
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomFacility
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomFacility
//      Field currmn 4	: RoomFacility
//      Field AddingString 4	: Room Room
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomStay
//      Field currmn 4	: RoomFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomData
//      Field currmn 4	: RoomFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomFacility
//      Field parentModule.name 4	: room
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomFacility
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomFacility
//      Field currmn 4	: RoomFacility
//      Field AddingString 4	: Room Room
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomStay
//      Field currmn 4	: RoomFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomData
//      Field currmn 4	: RoomFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomFacility
    }
}
