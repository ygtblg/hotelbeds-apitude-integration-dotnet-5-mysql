/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:IssueData.cs
   ThisFilePath:C:\Generated\booking-site\server\Entities\Concrete\App\IssueData.cs
*/

using Core.Entities;
using System;
using System.Collections.Generic;

namespace Entities.Concrete
{
    public class IssueData:IEntity
    {
        public int IssueDataId { get; set; }
//      Field AddingString 1	: string Uuid
        public string Uuid { get; set; }
//      Field AddingString 2	: string Code
        public string Code { get; set; }
//      Field AddingString 2	: string Type
        public string Type { get; set; }
//      Field AddingString 2	: bool Alternative
        public bool Alternative { get; set; }
//      Field AddingString 2	: string IssueUUID
        public string IssueUUID { get; set; }


//      OneToOne Relation With	: Description
        public int DescriptionId { get; set; }
        public Description Description { get; set; }

//      OneToOne Relation With	: Name
        public int NameId { get; set; }
        public Name Name { get; set; }

//      Field parentModule.name 4	: issue
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: IssueData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: IssueData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: IssueData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: IssueData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: IssueData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: IssueData
//      Field parentFieldType 4	: null
//      Field cmn 4	: IssueData
//      Field currmn 4	: IssueData
//      Field AddingString 4	: Issue Issue
        public Issue Issue { get; set; }
//      Field parentModule.name 4	: issue
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: IssueData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: IssueData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: IssueData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: IssueData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: IssueData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: IssueData
//      Field parentFieldType 4	: null
//      Field cmn 4	: IssueData
//      Field currmn 4	: IssueData
//      Field AddingString 4	: Issue Issue
//      Field parentModule.name 4	: issue
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: IssueData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: IssueData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: IssueData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: IssueData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: IssueData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: IssueData
//      Field parentFieldType 4	: null
//      Field cmn 4	: IssueData
//      Field currmn 4	: IssueData
//      Field AddingString 4	: Issue Issue
//      Field parentModule.name 4	: issue
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: IssueData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: IssueData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: IssueData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: IssueData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: IssueData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: IssueData
//      Field parentFieldType 4	: null
//      Field cmn 4	: IssueData
//      Field currmn 4	: IssueData
//      Field AddingString 4	: Issue Issue
//      Field parentModule.name 4	: issue
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: IssueData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: IssueData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: IssueData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: IssueData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: IssueData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: IssueData
//      Field parentFieldType 4	: null
//      Field cmn 4	: IssueData
//      Field currmn 4	: IssueData
//      Field AddingString 4	: Issue Issue
//      Field parentModule.name 4	: issue
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: IssueData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: IssueData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: IssueData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: IssueData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: IssueData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: IssueData
//      Field parentFieldType 4	: null
//      Field cmn 4	: IssueData
//      Field currmn 4	: IssueData
//      Field AddingString 4	: Issue Issue
//      Field parentModule.name 4	: issue
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: IssueData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: IssueData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: IssueData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: IssueData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: IssueData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: bool
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: IssueData
//      Field parentFieldType 4	: null
//      Field cmn 4	: IssueData
//      Field currmn 4	: IssueData
//      Field AddingString 4	: Issue Issue
    }
}
