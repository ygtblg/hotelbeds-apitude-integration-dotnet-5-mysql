/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:Category.cs
   ThisFilePath:C:\Generated\booking-site\server\Entities\Concrete\App\Category.cs
*/

using Core.Entities;
using System;
using System.Collections.Generic;

namespace Entities.Concrete
{
    public class Category:IEntity
    {
        public int CategoryId { get; set; }
//      Field AddingString 1	: string Uuid
        public string Uuid { get; set; }
//      Field AddingString 2	: string Code
        public string Code { get; set; }
//      Field AddingString 2	: int SimpleCode
        public int SimpleCode { get; set; }
//      Field AddingString 2	: string AccommodationType
        public string AccommodationType { get; set; }
//      Field AddingString 2	: string Group
        public string Group { get; set; }


//      OneToOne Relation With	: Description
        public int DescriptionId { get; set; }
        public Description Description { get; set; }

//      Field parentModule.name 4	: hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: HotelBoard
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: HotelSegment
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Number
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Name
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Description
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Country
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: State
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Destination
//      Field currmn 4	: Category
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Zone
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Coordinates
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Category
//      Field currmn 4	: Category
//      Field AddingString 4	: Hotel Hotel
        public Hotel Hotel { get; set; }
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: CategoryGroup
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Chain
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Accommodation
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Address
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: City
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Phone
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Room
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Facility
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Terminal
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Issue
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: InterestPoint
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Image
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Wildcard
//      Field currmn 4	: Category
//      Field parentModule.name 4	: hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: HotelBoard
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: HotelSegment
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Number
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Name
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Description
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Country
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: State
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Destination
//      Field currmn 4	: Category
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Zone
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Coordinates
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Category
//      Field currmn 4	: Category
//      Field AddingString 4	: Hotel Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: CategoryGroup
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Chain
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Accommodation
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Address
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: City
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Phone
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Room
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Facility
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Terminal
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Issue
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: InterestPoint
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Image
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Wildcard
//      Field currmn 4	: Category
//      Field parentModule.name 4	: hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: HotelBoard
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: HotelSegment
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Number
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Name
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Description
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Country
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: State
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Destination
//      Field currmn 4	: Category
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Zone
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Coordinates
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Category
//      Field currmn 4	: Category
//      Field AddingString 4	: Hotel Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: CategoryGroup
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Chain
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Accommodation
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Address
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: City
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Phone
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Room
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Facility
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Terminal
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Issue
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: InterestPoint
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Image
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Wildcard
//      Field currmn 4	: Category
//      Field parentModule.name 4	: hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: HotelBoard
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: HotelSegment
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Number
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Name
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Description
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Country
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: State
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Destination
//      Field currmn 4	: Category
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Zone
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Coordinates
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Category
//      Field currmn 4	: Category
//      Field AddingString 4	: Hotel Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: CategoryGroup
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Chain
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Accommodation
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Address
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: City
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Phone
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Room
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Facility
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Terminal
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Issue
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: InterestPoint
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Image
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Wildcard
//      Field currmn 4	: Category
//      Field parentModule.name 4	: hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: HotelBoard
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: HotelSegment
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Number
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Name
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Description
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Country
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: State
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Destination
//      Field currmn 4	: Category
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Zone
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Coordinates
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Category
//      Field currmn 4	: Category
//      Field AddingString 4	: Hotel Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: CategoryGroup
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Chain
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Accommodation
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Address
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: City
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Phone
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Room
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Facility
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Terminal
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Issue
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: InterestPoint
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Image
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Wildcard
//      Field currmn 4	: Category
//      Field parentModule.name 4	: hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: HotelBoard
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: HotelSegment
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Number
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Name
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Description
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Country
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: State
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Destination
//      Field currmn 4	: Category
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: Zone
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Coordinates
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Category
//      Field currmn 4	: Category
//      Field AddingString 4	: Hotel Hotel
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: CategoryGroup
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Chain
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Accommodation
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Address
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: City
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Phone
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Room
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Facility
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Terminal
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Issue
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: InterestPoint
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Image
//      Field currmn 4	: Category
//      Field parentFieldType 4	: null
//      Field cmn 4	: Wildcard
//      Field currmn 4	: Category
    }
}
