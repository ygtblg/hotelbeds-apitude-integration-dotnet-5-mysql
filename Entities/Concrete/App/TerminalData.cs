/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:TerminalData.cs
   ThisFilePath:C:\Generated\booking-site\server\Entities\Concrete\App\TerminalData.cs
*/

using Core.Entities;
using System;
using System.Collections.Generic;

namespace Entities.Concrete
{
    public class TerminalData:IEntity
    {
        public int TerminalDataId { get; set; }
//      Field AddingString 1	: string Uuid
        public string Uuid { get; set; }
//      Field AddingString 2	: string Code
        public string Code { get; set; }
//      Field AddingString 2	: string Type
        public string Type { get; set; }
//      Field AddingString 2	: string Country
        public string Country { get; set; }
//      Field AddingString 2	: string TerminalUUID
        public string TerminalUUID { get; set; }


//      OneToOne Relation With	: Name
        public int NameId { get; set; }
        public Name Name { get; set; }

//      OneToOne Relation With	: Description
        public int DescriptionId { get; set; }
        public Description Description { get; set; }

//      Field parentModule.name 4	: terminal
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TerminalData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TerminalData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: TerminalData
//      Field currmn 4	: TerminalData
//      Field AddingString 4	: Terminal Terminal
        public Terminal Terminal { get; set; }
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TerminalData
//      Field parentModule.name 4	: terminal
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TerminalData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TerminalData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: TerminalData
//      Field currmn 4	: TerminalData
//      Field AddingString 4	: Terminal Terminal
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TerminalData
//      Field parentModule.name 4	: terminal
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TerminalData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TerminalData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: TerminalData
//      Field currmn 4	: TerminalData
//      Field AddingString 4	: Terminal Terminal
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TerminalData
//      Field parentModule.name 4	: terminal
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TerminalData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TerminalData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: TerminalData
//      Field currmn 4	: TerminalData
//      Field AddingString 4	: Terminal Terminal
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TerminalData
//      Field parentModule.name 4	: terminal
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TerminalData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TerminalData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: TerminalData
//      Field currmn 4	: TerminalData
//      Field AddingString 4	: Terminal Terminal
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TerminalData
//      Field parentModule.name 4	: terminal
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TerminalData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TerminalData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: TerminalData
//      Field currmn 4	: TerminalData
//      Field AddingString 4	: Terminal Terminal
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TerminalData
//      Field parentModule.name 4	: terminal
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TerminalData
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TerminalData
//      Field parentFieldType 4	: int
//      Field parentFieldType 4	: null
//      Field cmn 4	: TerminalData
//      Field currmn 4	: TerminalData
//      Field AddingString 4	: Terminal Terminal
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: TerminalData
    }
}
