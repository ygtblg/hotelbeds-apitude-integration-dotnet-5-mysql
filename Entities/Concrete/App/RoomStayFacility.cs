/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:RoomStayFacility.cs
   ThisFilePath:C:\Generated\booking-site\server\Entities\Concrete\App\RoomStayFacility.cs
*/

using Core.Entities;
using System;
using System.Collections.Generic;

namespace Entities.Concrete
{
    public class RoomStayFacility:IEntity
    {
        public int RoomStayFacilityId { get; set; }
//      Field AddingString 1	: string Uuid
        public string Uuid { get; set; }
//      Field AddingString 2	: int FacilityCode
        public int FacilityCode { get; set; }
//      Field AddingString 2	: int FacilityGroupCode
        public int FacilityGroupCode { get; set; }
//      Field AddingString 2	: int Number
        public int Number { get; set; }
//      Field AddingString 2	: string RoomStayUUID
        public string RoomStayUUID { get; set; }


//      OneToMany Relation With	: RoomStay
        public int RoomStayId { get; set; }
//      Field AddingString 3	: RoomStay RoomStay
        public RoomStay RoomStay { get; set; } 

//      OneToOne Relation With	: FacilityData
        public int FacilityDataId { get; set; }
        public FacilityData FacilityData { get; set; }

//      Field parentModule.name 4	: room-stay
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStayFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStayFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStayFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStayFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomStayFacility
//      Field currmn 4	: RoomStayFacility
//      Field AddingString 4	: RoomStay RoomStay
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStayFacility
//      Field parentModule.name 4	: room-stay
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStayFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStayFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStayFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStayFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomStayFacility
//      Field currmn 4	: RoomStayFacility
//      Field AddingString 4	: RoomStay RoomStay
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStayFacility
//      Field parentModule.name 4	: room-stay
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStayFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStayFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStayFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStayFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomStayFacility
//      Field currmn 4	: RoomStayFacility
//      Field AddingString 4	: RoomStay RoomStay
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStayFacility
//      Field parentModule.name 4	: room-stay
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStayFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStayFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStayFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStayFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomStayFacility
//      Field currmn 4	: RoomStayFacility
//      Field AddingString 4	: RoomStay RoomStay
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStayFacility
//      Field parentModule.name 4	: room-stay
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStayFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStayFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStayFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStayFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomStayFacility
//      Field currmn 4	: RoomStayFacility
//      Field AddingString 4	: RoomStay RoomStay
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStayFacility
//      Field parentModule.name 4	: room-stay
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStayFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStayFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStayFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStayFacility
//      Field parentFieldType 4	: null
//      Field cmn 4	: RoomStayFacility
//      Field currmn 4	: RoomStayFacility
//      Field AddingString 4	: RoomStay RoomStay
//      Field parentFieldType 4	: null
//      Field cmn 4	: String
//      Field currmn 4	: RoomStayFacility
    }
}
