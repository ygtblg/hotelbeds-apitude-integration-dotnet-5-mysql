/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:PhoneController.cs
   ThisFilePath:C:\Generated\booking-site\server\WebApi\Controllers\App\PhoneController.cs
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Abstract;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PhoneController : ControllerBase
    {
        private IPhoneService _PhoneService;

        public PhoneController(IPhoneService PhoneService)
        {
            _PhoneService = PhoneService;
        }

        [HttpGet("getall")]
        public IActionResult GetList()
        {
            var result = _PhoneService.GetList();
            if (result.Result.Success)
            {
                return Ok(result.Result.Data);
            }

            return BadRequest(result.Result.Message);
        }

        [HttpPost("add")]
        public IActionResult Add(Phone _Phone)
        {
            var result = _PhoneService.Add(_Phone);
            if (result.Success)
            {
                return Ok(result.Message);
            }

            return BadRequest(result.Message);
        }
    }
}
