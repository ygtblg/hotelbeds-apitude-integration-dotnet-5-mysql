/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:RoomStayController.cs
   ThisFilePath:C:\Generated\booking-site\server\WebApi\Controllers\App\RoomStayController.cs
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Abstract;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoomStayController : ControllerBase
    {
        private IRoomStayService _RoomStayService;

        public RoomStayController(IRoomStayService RoomStayService)
        {
            _RoomStayService = RoomStayService;
        }

        [HttpGet("getall")]
        public IActionResult GetList()
        {
            var result = _RoomStayService.GetList();
            if (result.Result.Success)
            {
                return Ok(result.Result.Data);
            }

            return BadRequest(result.Result.Message);
        }

        [HttpPost("add")]
        public IActionResult Add(RoomStay _RoomStay)
        {
            var result = _RoomStayService.Add(_RoomStay);
            if (result.Success)
            {
                return Ok(result.Message);
            }

            return BadRequest(result.Message);
        }
    }
}
