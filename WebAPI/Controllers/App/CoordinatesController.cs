/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:CoordinatesController.cs
   ThisFilePath:C:\Generated\booking-site\server\WebApi\Controllers\App\CoordinatesController.cs
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Abstract;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CoordinatesController : ControllerBase
    {
        private ICoordinatesService _CoordinatesService;

        public CoordinatesController(ICoordinatesService CoordinatesService)
        {
            _CoordinatesService = CoordinatesService;
        }

        [HttpGet("getall")]
        public IActionResult GetList()
        {
            var result = _CoordinatesService.GetList();
            if (result.Result.Success)
            {
                return Ok(result.Result.Data);
            }

            return BadRequest(result.Result.Message);
        }

        [HttpPost("add")]
        public IActionResult Add(Coordinates _Coordinates)
        {
            var result = _CoordinatesService.Add(_Coordinates);
            if (result.Success)
            {
                return Ok(result.Message);
            }

            return BadRequest(result.Message);
        }
    }
}
