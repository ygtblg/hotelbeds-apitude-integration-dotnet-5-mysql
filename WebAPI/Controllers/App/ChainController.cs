/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:ChainController.cs
   ThisFilePath:C:\Generated\booking-site\server\WebApi\Controllers\App\ChainController.cs
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Abstract;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ChainController : ControllerBase
    {
        private IChainService _ChainService;

        public ChainController(IChainService ChainService)
        {
            _ChainService = ChainService;
        }

        [HttpGet("getall")]
        public IActionResult GetList()
        {
            var result = _ChainService.GetList();
            if (result.Result.Success)
            {
                return Ok(result.Result.Data);
            }

            return BadRequest(result.Result.Message);
        }

        [HttpPost("add")]
        public IActionResult Add(Chain _Chain)
        {
            var result = _ChainService.Add(_Chain);
            if (result.Success)
            {
                return Ok(result.Message);
            }

            return BadRequest(result.Message);
        }
    }
}
