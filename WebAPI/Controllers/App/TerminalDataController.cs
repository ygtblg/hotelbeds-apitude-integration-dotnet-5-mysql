/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:TerminalDataController.cs
   ThisFilePath:C:\Generated\booking-site\server\WebApi\Controllers\App\TerminalDataController.cs
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Abstract;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TerminalDataController : ControllerBase
    {
        private ITerminalDataService _TerminalDataService;

        public TerminalDataController(ITerminalDataService TerminalDataService)
        {
            _TerminalDataService = TerminalDataService;
        }

        [HttpGet("getall")]
        public IActionResult GetList()
        {
            var result = _TerminalDataService.GetList();
            if (result.Result.Success)
            {
                return Ok(result.Result.Data);
            }

            return BadRequest(result.Result.Message);
        }

        [HttpPost("add")]
        public IActionResult Add(TerminalData _TerminalData)
        {
            var result = _TerminalDataService.Add(_TerminalData);
            if (result.Success)
            {
                return Ok(result.Message);
            }

            return BadRequest(result.Message);
        }
    }
}
