/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:HotelSegmentController.cs
   ThisFilePath:C:\Generated\booking-site\server\WebApi\Controllers\App\HotelSegmentController.cs
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Abstract;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HotelSegmentController : ControllerBase
    {
        private IHotelSegmentService _HotelSegmentService;

        public HotelSegmentController(IHotelSegmentService HotelSegmentService)
        {
            _HotelSegmentService = HotelSegmentService;
        }

        [HttpGet("getall")]
        public IActionResult GetList()
        {
            var result = _HotelSegmentService.GetList();
            if (result.Result.Success)
            {
                return Ok(result.Result.Data);
            }

            return BadRequest(result.Result.Message);
        }

        [HttpPost("add")]
        public IActionResult Add(HotelSegment _HotelSegment)
        {
            var result = _HotelSegmentService.Add(_HotelSegment);
            if (result.Success)
            {
                return Ok(result.Message);
            }

            return BadRequest(result.Message);
        }
    }
}
