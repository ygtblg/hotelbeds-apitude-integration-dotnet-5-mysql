/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:FacilityTypologyController.cs
   ThisFilePath:C:\Generated\booking-site\server\WebApi\Controllers\App\FacilityTypologyController.cs
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Abstract;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FacilityTypologyController : ControllerBase
    {
        private IFacilityTypologyService _FacilityTypologyService;

        public FacilityTypologyController(IFacilityTypologyService FacilityTypologyService)
        {
            _FacilityTypologyService = FacilityTypologyService;
        }

        [HttpGet("getall")]
        public IActionResult GetList()
        {
            var result = _FacilityTypologyService.GetList();
            if (result.Result.Success)
            {
                return Ok(result.Result.Data);
            }

            return BadRequest(result.Result.Message);
        }

        [HttpPost("add")]
        public IActionResult Add(FacilityTypology _FacilityTypology)
        {
            var result = _FacilityTypologyService.Add(_FacilityTypology);
            if (result.Success)
            {
                return Ok(result.Message);
            }

            return BadRequest(result.Message);
        }
    }
}
