/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:IssueController.cs
   ThisFilePath:C:\Generated\booking-site\server\WebApi\Controllers\App\IssueController.cs
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Abstract;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IssueController : ControllerBase
    {
        private IIssueService _IssueService;

        public IssueController(IIssueService IssueService)
        {
            _IssueService = IssueService;
        }

        [HttpGet("getall")]
        public IActionResult GetList()
        {
            var result = _IssueService.GetList();
            if (result.Result.Success)
            {
                return Ok(result.Result.Data);
            }

            return BadRequest(result.Result.Message);
        }

        [HttpPost("add")]
        public IActionResult Add(Issue _Issue)
        {
            var result = _IssueService.Add(_Issue);
            if (result.Success)
            {
                return Ok(result.Message);
            }

            return BadRequest(result.Message);
        }
    }
}
