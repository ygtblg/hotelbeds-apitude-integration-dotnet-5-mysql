/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:LanguageController.cs
   ThisFilePath:C:\Generated\booking-site\server\WebApi\Controllers\App\LanguageController.cs
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Abstract;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LanguageController : ControllerBase
    {
        private ILanguageService _LanguageService;

        public LanguageController(ILanguageService LanguageService)
        {
            _LanguageService = LanguageService;
        }

        [HttpGet("getall")]
        public IActionResult GetList()
        {
            var result = _LanguageService.GetList();
            if (result.Result.Success)
            {
                return Ok(result.Result.Data);
            }

            return BadRequest(result.Result.Message);
        }

        [HttpPost("add")]
        public IActionResult Add(Language _Language)
        {
            var result = _LanguageService.Add(_Language);
            if (result.Success)
            {
                return Ok(result.Message);
            }

            return BadRequest(result.Message);
        }
    }
}
