/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:FacilityController.cs
   ThisFilePath:C:\Generated\booking-site\server\WebApi\Controllers\App\FacilityController.cs
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Abstract;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FacilityController : ControllerBase
    {
        private IFacilityService _FacilityService;

        public FacilityController(IFacilityService FacilityService)
        {
            _FacilityService = FacilityService;
        }

        [HttpGet("getall")]
        public IActionResult GetList()
        {
            var result = _FacilityService.GetList();
            if (result.Result.Success)
            {
                return Ok(result.Result.Data);
            }

            return BadRequest(result.Result.Message);
        }

        [HttpPost("add")]
        public IActionResult Add(Facility _Facility)
        {
            var result = _FacilityService.Add(_Facility);
            if (result.Success)
            {
                return Ok(result.Message);
            }

            return BadRequest(result.Message);
        }
    }
}
