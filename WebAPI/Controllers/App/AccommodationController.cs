/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:AccommodationController.cs
   ThisFilePath:C:\Generated\booking-site\server\WebApi\Controllers\App\AccommodationController.cs
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Abstract;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccommodationController : ControllerBase
    {
        private IAccommodationService _AccommodationService;

        public AccommodationController(IAccommodationService AccommodationService)
        {
            _AccommodationService = AccommodationService;
        }

        [HttpGet("getall")]
        public IActionResult GetList()
        {
            var result = _AccommodationService.GetList();
            if (result.Result.Success)
            {
                return Ok(result.Result.Data);
            }

            return BadRequest(result.Result.Message);
        }

        [HttpPost("add")]
        public IActionResult Add(Accommodation _Accommodation)
        {
            var result = _AccommodationService.Add(_Accommodation);
            if (result.Success)
            {
                return Ok(result.Message);
            }

            return BadRequest(result.Message);
        }
    }
}
