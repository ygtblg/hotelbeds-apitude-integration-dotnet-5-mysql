/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:RoomFacilityController.cs
   ThisFilePath:C:\Generated\booking-site\server\WebApi\Controllers\App\RoomFacilityController.cs
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Abstract;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoomFacilityController : ControllerBase
    {
        private IRoomFacilityService _RoomFacilityService;

        public RoomFacilityController(IRoomFacilityService RoomFacilityService)
        {
            _RoomFacilityService = RoomFacilityService;
        }

        [HttpGet("getall")]
        public IActionResult GetList()
        {
            var result = _RoomFacilityService.GetList();
            if (result.Result.Success)
            {
                return Ok(result.Result.Data);
            }

            return BadRequest(result.Result.Message);
        }

        [HttpPost("add")]
        public IActionResult Add(RoomFacility _RoomFacility)
        {
            var result = _RoomFacilityService.Add(_RoomFacility);
            if (result.Success)
            {
                return Ok(result.Message);
            }

            return BadRequest(result.Message);
        }
    }
}
