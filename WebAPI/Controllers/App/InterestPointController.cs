/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:InterestPointController.cs
   ThisFilePath:C:\Generated\booking-site\server\WebApi\Controllers\App\InterestPointController.cs
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Abstract;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InterestPointController : ControllerBase
    {
        private IInterestPointService _InterestPointService;

        public InterestPointController(IInterestPointService InterestPointService)
        {
            _InterestPointService = InterestPointService;
        }

        [HttpGet("getall")]
        public IActionResult GetList()
        {
            var result = _InterestPointService.GetList();
            if (result.Result.Success)
            {
                return Ok(result.Result.Data);
            }

            return BadRequest(result.Result.Message);
        }

        [HttpPost("add")]
        public IActionResult Add(InterestPoint _InterestPoint)
        {
            var result = _InterestPointService.Add(_InterestPoint);
            if (result.Success)
            {
                return Ok(result.Message);
            }

            return BadRequest(result.Message);
        }
    }
}
