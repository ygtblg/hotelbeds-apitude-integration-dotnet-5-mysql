/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:DestinationController.cs
   ThisFilePath:C:\Generated\booking-site\server\WebApi\Controllers\App\DestinationController.cs
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Abstract;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DestinationController : ControllerBase
    {
        private IDestinationService _DestinationService;

        public DestinationController(IDestinationService DestinationService)
        {
            _DestinationService = DestinationService;
        }

        [HttpGet("getall")]
        public IActionResult GetList()
        {
            var result = _DestinationService.GetList();
            if (result.Result.Success)
            {
                return Ok(result.Result.Data);
            }

            return BadRequest(result.Result.Message);
        }

        [HttpPost("add")]
        public IActionResult Add(Destination _Destination)
        {
            var result = _DestinationService.Add(_Destination);
            if (result.Success)
            {
                return Ok(result.Message);
            }

            return BadRequest(result.Message);
        }
    }
}
