/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:TypeMultiDescriptionController.cs
   ThisFilePath:C:\Generated\booking-site\server\WebApi\Controllers\App\TypeMultiDescriptionController.cs
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Abstract;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TypeMultiDescriptionController : ControllerBase
    {
        private ITypeMultiDescriptionService _TypeMultiDescriptionService;

        public TypeMultiDescriptionController(ITypeMultiDescriptionService TypeMultiDescriptionService)
        {
            _TypeMultiDescriptionService = TypeMultiDescriptionService;
        }

        [HttpGet("getall")]
        public IActionResult GetList()
        {
            var result = _TypeMultiDescriptionService.GetList();
            if (result.Result.Success)
            {
                return Ok(result.Result.Data);
            }

            return BadRequest(result.Result.Message);
        }

        [HttpPost("add")]
        public IActionResult Add(TypeMultiDescription _TypeMultiDescription)
        {
            var result = _TypeMultiDescriptionService.Add(_TypeMultiDescription);
            if (result.Success)
            {
                return Ok(result.Message);
            }

            return BadRequest(result.Message);
        }
    }
}
