/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:IssueDataController.cs
   ThisFilePath:C:\Generated\booking-site\server\WebApi\Controllers\App\IssueDataController.cs
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Abstract;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class IssueDataController : ControllerBase
    {
        private IIssueDataService _IssueDataService;

        public IssueDataController(IIssueDataService IssueDataService)
        {
            _IssueDataService = IssueDataService;
        }

        [HttpGet("getall")]
        public IActionResult GetList()
        {
            var result = _IssueDataService.GetList();
            if (result.Result.Success)
            {
                return Ok(result.Result.Data);
            }

            return BadRequest(result.Result.Message);
        }

        [HttpPost("add")]
        public IActionResult Add(IssueData _IssueData)
        {
            var result = _IssueDataService.Add(_IssueData);
            if (result.Success)
            {
                return Ok(result.Message);
            }

            return BadRequest(result.Message);
        }
    }
}
