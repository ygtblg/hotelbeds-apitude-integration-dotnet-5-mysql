/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:CharacteristicDescriptionController.cs
   ThisFilePath:C:\Generated\booking-site\server\WebApi\Controllers\App\CharacteristicDescriptionController.cs
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Abstract;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CharacteristicDescriptionController : ControllerBase
    {
        private ICharacteristicDescriptionService _CharacteristicDescriptionService;

        public CharacteristicDescriptionController(ICharacteristicDescriptionService CharacteristicDescriptionService)
        {
            _CharacteristicDescriptionService = CharacteristicDescriptionService;
        }

        [HttpGet("getall")]
        public IActionResult GetList()
        {
            var result = _CharacteristicDescriptionService.GetList();
            if (result.Result.Success)
            {
                return Ok(result.Result.Data);
            }

            return BadRequest(result.Result.Message);
        }

        [HttpPost("add")]
        public IActionResult Add(CharacteristicDescription _CharacteristicDescription)
        {
            var result = _CharacteristicDescriptionService.Add(_CharacteristicDescription);
            if (result.Success)
            {
                return Ok(result.Message);
            }

            return BadRequest(result.Message);
        }
    }
}
