/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:RoomStayFacilityController.cs
   ThisFilePath:C:\Generated\booking-site\server\WebApi\Controllers\App\RoomStayFacilityController.cs
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Abstract;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoomStayFacilityController : ControllerBase
    {
        private IRoomStayFacilityService _RoomStayFacilityService;

        public RoomStayFacilityController(IRoomStayFacilityService RoomStayFacilityService)
        {
            _RoomStayFacilityService = RoomStayFacilityService;
        }

        [HttpGet("getall")]
        public IActionResult GetList()
        {
            var result = _RoomStayFacilityService.GetList();
            if (result.Result.Success)
            {
                return Ok(result.Result.Data);
            }

            return BadRequest(result.Result.Message);
        }

        [HttpPost("add")]
        public IActionResult Add(RoomStayFacility _RoomStayFacility)
        {
            var result = _RoomStayFacilityService.Add(_RoomStayFacility);
            if (result.Success)
            {
                return Ok(result.Message);
            }

            return BadRequest(result.Message);
        }
    }
}
