/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:HotelRoomDescriptionController.cs
   ThisFilePath:C:\Generated\booking-site\server\WebApi\Controllers\App\HotelRoomDescriptionController.cs
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Abstract;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HotelRoomDescriptionController : ControllerBase
    {
        private IHotelRoomDescriptionService _HotelRoomDescriptionService;

        public HotelRoomDescriptionController(IHotelRoomDescriptionService HotelRoomDescriptionService)
        {
            _HotelRoomDescriptionService = HotelRoomDescriptionService;
        }

        [HttpGet("getall")]
        public IActionResult GetList()
        {
            var result = _HotelRoomDescriptionService.GetList();
            if (result.Result.Success)
            {
                return Ok(result.Result.Data);
            }

            return BadRequest(result.Result.Message);
        }

        [HttpPost("add")]
        public IActionResult Add(HotelRoomDescription _HotelRoomDescription)
        {
            var result = _HotelRoomDescriptionService.Add(_HotelRoomDescription);
            if (result.Success)
            {
                return Ok(result.Message);
            }

            return BadRequest(result.Message);
        }
    }
}
