/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:RoomDataController.cs
   ThisFilePath:C:\Generated\booking-site\server\WebApi\Controllers\App\RoomDataController.cs
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Abstract;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoomDataController : ControllerBase
    {
        private IRoomDataService _RoomDataService;

        public RoomDataController(IRoomDataService RoomDataService)
        {
            _RoomDataService = RoomDataService;
        }

        [HttpGet("getall")]
        public IActionResult GetList()
        {
            var result = _RoomDataService.GetList();
            if (result.Result.Success)
            {
                return Ok(result.Result.Data);
            }

            return BadRequest(result.Result.Message);
        }

        [HttpPost("add")]
        public IActionResult Add(RoomData _RoomData)
        {
            var result = _RoomDataService.Add(_RoomData);
            if (result.Success)
            {
                return Ok(result.Message);
            }

            return BadRequest(result.Message);
        }
    }
}
