/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:FacilityGroupController.cs
   ThisFilePath:C:\Generated\booking-site\server\WebApi\Controllers\App\FacilityGroupController.cs
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Abstract;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FacilityGroupController : ControllerBase
    {
        private IFacilityGroupService _FacilityGroupService;

        public FacilityGroupController(IFacilityGroupService FacilityGroupService)
        {
            _FacilityGroupService = FacilityGroupService;
        }

        [HttpGet("getall")]
        public IActionResult GetList()
        {
            var result = _FacilityGroupService.GetList();
            if (result.Result.Success)
            {
                return Ok(result.Result.Data);
            }

            return BadRequest(result.Result.Message);
        }

        [HttpPost("add")]
        public IActionResult Add(FacilityGroup _FacilityGroup)
        {
            var result = _FacilityGroupService.Add(_FacilityGroup);
            if (result.Success)
            {
                return Ok(result.Message);
            }

            return BadRequest(result.Message);
        }
    }
}
