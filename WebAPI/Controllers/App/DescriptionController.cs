/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:DescriptionController.cs
   ThisFilePath:C:\Generated\booking-site\server\WebApi\Controllers\App\DescriptionController.cs
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Abstract;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DescriptionController : ControllerBase
    {
        private IDescriptionService _DescriptionService;

        public DescriptionController(IDescriptionService DescriptionService)
        {
            _DescriptionService = DescriptionService;
        }

        [HttpGet("getall")]
        public IActionResult GetList()
        {
            var result = _DescriptionService.GetList();
            if (result.Result.Success)
            {
                return Ok(result.Result.Data);
            }

            return BadRequest(result.Result.Message);
        }

        [HttpPost("add")]
        public IActionResult Add(Description _Description)
        {
            var result = _DescriptionService.Add(_Description);
            if (result.Success)
            {
                return Ok(result.Message);
            }

            return BadRequest(result.Message);
        }
    }
}
