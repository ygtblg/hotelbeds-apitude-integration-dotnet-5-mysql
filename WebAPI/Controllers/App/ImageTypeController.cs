/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:ImageTypeController.cs
   ThisFilePath:C:\Generated\booking-site\server\WebApi\Controllers\App\ImageTypeController.cs
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Abstract;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ImageTypeController : ControllerBase
    {
        private IImageTypeService _ImageTypeService;

        public ImageTypeController(IImageTypeService ImageTypeService)
        {
            _ImageTypeService = ImageTypeService;
        }

        [HttpGet("getall")]
        public IActionResult GetList()
        {
            var result = _ImageTypeService.GetList();
            if (result.Result.Success)
            {
                return Ok(result.Result.Data);
            }

            return BadRequest(result.Result.Message);
        }

        [HttpPost("add")]
        public IActionResult Add(ImageType _ImageType)
        {
            var result = _ImageTypeService.Add(_ImageType);
            if (result.Success)
            {
                return Ok(result.Message);
            }

            return BadRequest(result.Message);
        }
    }
}
