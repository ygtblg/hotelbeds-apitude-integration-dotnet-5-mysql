/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:CategoryGroupController.cs
   ThisFilePath:C:\Generated\booking-site\server\WebApi\Controllers\App\CategoryGroupController.cs
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Abstract;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryGroupController : ControllerBase
    {
        private ICategoryGroupService _CategoryGroupService;

        public CategoryGroupController(ICategoryGroupService CategoryGroupService)
        {
            _CategoryGroupService = CategoryGroupService;
        }

        [HttpGet("getall")]
        public IActionResult GetList()
        {
            var result = _CategoryGroupService.GetList();
            if (result.Result.Success)
            {
                return Ok(result.Result.Data);
            }

            return BadRequest(result.Result.Message);
        }

        [HttpPost("add")]
        public IActionResult Add(CategoryGroup _CategoryGroup)
        {
            var result = _CategoryGroupService.Add(_CategoryGroup);
            if (result.Success)
            {
                return Ok(result.Message);
            }

            return BadRequest(result.Message);
        }
    }
}
