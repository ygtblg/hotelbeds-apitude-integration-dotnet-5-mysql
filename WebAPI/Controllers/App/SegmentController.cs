/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:SegmentController.cs
   ThisFilePath:C:\Generated\booking-site\server\WebApi\Controllers\App\SegmentController.cs
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Abstract;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SegmentController : ControllerBase
    {
        private ISegmentService _SegmentService;

        public SegmentController(ISegmentService SegmentService)
        {
            _SegmentService = SegmentService;
        }

        [HttpGet("getall")]
        public IActionResult GetList()
        {
            var result = _SegmentService.GetList();
            if (result.Result.Success)
            {
                return Ok(result.Result.Data);
            }

            return BadRequest(result.Result.Message);
        }

        [HttpPost("add")]
        public IActionResult Add(Segment _Segment)
        {
            var result = _SegmentService.Add(_Segment);
            if (result.Success)
            {
                return Ok(result.Message);
            }

            return BadRequest(result.Message);
        }
    }
}
