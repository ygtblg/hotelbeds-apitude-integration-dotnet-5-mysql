/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:HotelBoardController.cs
   ThisFilePath:C:\Generated\booking-site\server\WebApi\Controllers\App\HotelBoardController.cs
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Abstract;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HotelBoardController : ControllerBase
    {
        private IHotelBoardService _HotelBoardService;

        public HotelBoardController(IHotelBoardService HotelBoardService)
        {
            _HotelBoardService = HotelBoardService;
        }

        [HttpGet("getall")]
        public IActionResult GetList()
        {
            var result = _HotelBoardService.GetList();
            if (result.Result.Success)
            {
                return Ok(result.Result.Data);
            }

            return BadRequest(result.Result.Message);
        }

        [HttpPost("add")]
        public IActionResult Add(HotelBoard _HotelBoard)
        {
            var result = _HotelBoardService.Add(_HotelBoard);
            if (result.Success)
            {
                return Ok(result.Message);
            }

            return BadRequest(result.Message);
        }
    }
}
