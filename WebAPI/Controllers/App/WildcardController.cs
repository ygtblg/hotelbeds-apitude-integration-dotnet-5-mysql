/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:WildcardController.cs
   ThisFilePath:C:\Generated\booking-site\server\WebApi\Controllers\App\WildcardController.cs
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Abstract;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WildcardController : ControllerBase
    {
        private IWildcardService _WildcardService;

        public WildcardController(IWildcardService WildcardService)
        {
            _WildcardService = WildcardService;
        }

        [HttpGet("getall")]
        public IActionResult GetList()
        {
            var result = _WildcardService.GetList();
            if (result.Result.Success)
            {
                return Ok(result.Result.Data);
            }

            return BadRequest(result.Result.Message);
        }

        [HttpPost("add")]
        public IActionResult Add(Wildcard _Wildcard)
        {
            var result = _WildcardService.Add(_Wildcard);
            if (result.Success)
            {
                return Ok(result.Message);
            }

            return BadRequest(result.Message);
        }
    }
}
