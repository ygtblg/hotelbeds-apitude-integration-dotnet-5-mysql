/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:BoardController.cs
   ThisFilePath:C:\Generated\booking-site\server\WebApi\Controllers\App\BoardController.cs
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Abstract;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BoardController : ControllerBase
    {
        private IBoardService _BoardService;

        public BoardController(IBoardService BoardService)
        {
            _BoardService = BoardService;
        }

        [HttpGet("getall")]
        public IActionResult GetList()
        {
            var result = _BoardService.GetList();
            if (result.Result.Success)
            {
                return Ok(result.Result.Data);
            }

            return BadRequest(result.Result.Message);
        }

        [HttpPost("add")]
        public IActionResult Add(Board _Board)
        {
            var result = _BoardService.Add(_Board);
            if (result.Success)
            {
                return Ok(result.Message);
            }

            return BadRequest(result.Message);
        }
    }
}
