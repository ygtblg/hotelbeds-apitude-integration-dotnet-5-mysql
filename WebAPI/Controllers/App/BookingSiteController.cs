/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:BookingSiteController.cs
   ThisFilePath:C:\Generated\booking-site\server\WebApi\Controllers\App\BookingSiteController.cs
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Abstract;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookingSiteController : ControllerBase
    {
        private IBookingSiteService _BookingSiteService;

        public BookingSiteController(IBookingSiteService BookingSiteService)
        {
            _BookingSiteService = BookingSiteService;
        }

        [HttpGet("getall")]
        public IActionResult GetList()
        {
            var result = _BookingSiteService.GetList();
            if (result.Result.Success)
            {
                return Ok(result.Result.Data);
            }

            return BadRequest(result.Result.Message);
        }

        [HttpPost("add")]
        public IActionResult Add(BookingSite _BookingSite)
        {
            var result = _BookingSiteService.Add(_BookingSite);
            if (result.Success)
            {
                return Ok(result.Message);
            }

            return BadRequest(result.Message);
        }
    }
}
