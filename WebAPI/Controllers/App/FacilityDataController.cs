/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:FacilityDataController.cs
   ThisFilePath:C:\Generated\booking-site\server\WebApi\Controllers\App\FacilityDataController.cs
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Abstract;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FacilityDataController : ControllerBase
    {
        private IFacilityDataService _FacilityDataService;

        public FacilityDataController(IFacilityDataService FacilityDataService)
        {
            _FacilityDataService = FacilityDataService;
        }

        [HttpGet("getall")]
        public IActionResult GetList()
        {
            var result = _FacilityDataService.GetList();
            if (result.Result.Success)
            {
                return Ok(result.Result.Data);
            }

            return BadRequest(result.Result.Message);
        }

        [HttpPost("add")]
        public IActionResult Add(FacilityData _FacilityData)
        {
            var result = _FacilityDataService.Add(_FacilityData);
            if (result.Success)
            {
                return Ok(result.Message);
            }

            return BadRequest(result.Message);
        }
    }
}
