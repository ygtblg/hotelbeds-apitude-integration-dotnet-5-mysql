/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 15/08/2021 11:07:46
   ThisFileName:BookPublisherController.cs
   ThisFilePath:C:\projects\BackendPartner\dotnet\agile-ionic-generator-001-api\WebApi\Controllers\BookPublisherController.cs
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Abstract;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookPublisherController : ControllerBase
    {
        private IBookPublisherService _BookPublisherService;

        public BookPublisherController(IBookPublisherService BookPublisherService)
        {
            _BookPublisherService = BookPublisherService;
        }

        [HttpGet("getall")]
        public IActionResult GetList()
        {
            var result = _BookPublisherService.GetList();
            if (result.Result.Success)
            {
                return Ok(result.Result.Data);
            }

            return BadRequest(result.Result.Message);
        }

        [HttpPost("add")]
        public IActionResult Add(BookPublisher _BookPublisher)
        {
            var result = _BookPublisherService.Add(_BookPublisher);
            if (result.Success)
            {
                return Ok(result.Message);
            }

            return BadRequest(result.Message);
        }
    }
}
