/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 15/08/2021 11:07:46
   ThisFileName:LayoutParamController.cs
   ThisFilePath:C:\projects\BackendPartner\dotnet\agile-ionic-generator-001-api\WebApi\Controllers\LayoutParamController.cs
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Business.Abstract;
using Entities.Concrete;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LayoutParamController : ControllerBase
    {
        private ILayoutParamService _LayoutParamService;

        public LayoutParamController(ILayoutParamService LayoutParamService)
        {
            _LayoutParamService = LayoutParamService;
        }

        [HttpGet("getall")]
        public IActionResult GetList()
        {
            var result = _LayoutParamService.GetList();
            if (result.Result.Success)
            {
                return Ok(result.Result.Data);
            }

            return BadRequest(result.Result.Message);
        }

        [HttpPost("add")]
        public IActionResult Add(LayoutParam _LayoutParam)
        {
            var result = _LayoutParamService.Add(_LayoutParam);
            if (result.Success)
            {
                return Ok(result.Message);
            }

            return BadRequest(result.Message);
        }
    }
}
