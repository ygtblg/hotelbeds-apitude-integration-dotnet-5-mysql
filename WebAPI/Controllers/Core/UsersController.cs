﻿/*
CREATED BY Yiğit Bilge<yigitbilge@gmail.com> 2020-11-18 05:15:00
*/
using System.Collections.Generic;
using AutoMapper;
using Business.Abstract;
using Core.Entities.Dtos;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private IUserService _userService;
        private readonly IMapper _mapper;
        public UsersController(IUserService userService, IMapper mapper)
        {
            _userService = userService;
            _mapper = mapper;
        }


        [HttpGet("getall")]
        public IActionResult GetList()
        { 
            var result = _userService.GetList();
            if (result.Result.Success)
            { 
                var data = result.Result.Data;
                var users = _mapper.Map<List<UserForListDTO>>(data);
                return Ok(users);
            }
            return BadRequest(result.Result.Message);

             
        }


    }
}