﻿using Breeze.AspNetCore;
using Breeze.Persistence;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using Entities.Concrete;
using Core.Entities.Concrete;
using System.Linq;
using DataAccess.Concrete.EntityFramework.Contexts;
using WebAPI.BreezeManager;
namespace WebAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    [BreezeQueryFilter]
    public class BreezeController : ControllerBase
    {
        private BreezePersistenceManager persistenceManager;
        public BreezeController(ApplicationDbContext dbContext)
        {
            persistenceManager = new BreezePersistenceManager(dbContext);
        }
        [HttpGet]
        public IQueryable<Book> Books()
        {
            return persistenceManager.Context.Book;
        }
        [HttpGet]
        public IQueryable<BookCategory> Bookcategories()
        {
            return persistenceManager.Context.BookCategory;
        }
        [HttpGet]
        public IQueryable<BookPublisher> Bookpublishers()
        {
            return persistenceManager.Context.BookPublisher;
        }
        [HttpGet]
        public IQueryable<BookDetail> Bookdetails()
        {
            return persistenceManager.Context.BookDetail;
        }
        [HttpGet]
        public IQueryable<Layout> Layout()
        {
            return persistenceManager.Context.Layout;
        }
        [HttpGet]
        public IQueryable<User> User()
        {
            return persistenceManager.Context.Users;
        }
        //  BreezeControllerFunctionsStart
        [HttpGet]
        public IQueryable<BookingSite> BookingSite()
        {
            return persistenceManager.Context.BookingSite;
        }
        [HttpGet]
        public IQueryable<Board> Boards()
        {
            return persistenceManager.Context.Board;
        }
        [HttpGet]
        public IQueryable<Description> Description()
        {
            return persistenceManager.Context.Description;
        }
        [HttpGet]
        public IQueryable<HotelBoard> HotelBoard()
        {
            return persistenceManager.Context.HotelBoard;
        }
        [HttpGet]
        public IQueryable<Hotel> Hotels()
        {
            return persistenceManager.Context.Hotel;
        }
        [HttpGet]
        public IQueryable<Accommodation> Accommodation()
        {
            return persistenceManager.Context.Accommodation;
        }
        [HttpGet]
        public IQueryable<TypeMultiDescription> TypeMultiDescription()
        {
            return persistenceManager.Context.TypeMultiDescription;
        }
        [HttpGet]
        public IQueryable<Address> Address()
        {
            return persistenceManager.Context.Address;
        }
        [HttpGet]
        public IQueryable<Category> Category()
        {
            return persistenceManager.Context.Category;
        }
        [HttpGet]
        public IQueryable<CategoryGroup> CategoryGroup()
        {
            return persistenceManager.Context.CategoryGroup;
        }
        [HttpGet]
        public IQueryable<Name> Name()
        {
            return persistenceManager.Context.Name;
        }
        [HttpGet]
        public IQueryable<Chain> Chain()
        {
            return persistenceManager.Context.Chain;
        }
        [HttpGet]
        public IQueryable<City> City()
        {
            return persistenceManager.Context.City;
        }
        [HttpGet]
        public IQueryable<Coordinates> Coordinates()
        {
            return persistenceManager.Context.Coordinates;
        }
        [HttpGet]
        public IQueryable<Country> Country()
        {
            return persistenceManager.Context.Country;
        }
        [HttpGet]
        public IQueryable<State> States()
        {
            return persistenceManager.Context.State;
        }
        [HttpGet]
        public IQueryable<State> State()
        {
            return persistenceManager.Context.State;
        }
        [HttpGet]
        public IQueryable<Destination> Destination()
        {
            return persistenceManager.Context.Destination;
        }
        [HttpGet]
        public IQueryable<Zone> Zones()
        {
            return persistenceManager.Context.Zone;
        }
        [HttpGet]
        public IQueryable<Zone> Zone()
        {
            return persistenceManager.Context.Zone;
        }
        [HttpGet]
        public IQueryable<Facility> Facilities()
        {
            return persistenceManager.Context.Facility;
        }
        [HttpGet]
        public IQueryable<FacilityData> FacilityData()
        {
            return persistenceManager.Context.FacilityData;
        }
        [HttpGet]
        public IQueryable<FacilityGroup> FacilityGroup()
        {
            return persistenceManager.Context.FacilityGroup;
        }
        [HttpGet]
        public IQueryable<FacilityTypology> FacilityTypology()
        {
            return persistenceManager.Context.FacilityTypology;
        }
        [HttpGet]
        public IQueryable<HotelSegment> HotelSegment()
        {
            return persistenceManager.Context.HotelSegment;
        }
        [HttpGet]
        public IQueryable<Image> Images()
        {
            return persistenceManager.Context.Image;
        }
        [HttpGet]
        public IQueryable<ImageType> ImageType()
        {
            return persistenceManager.Context.ImageType;
        }
        [HttpGet]
        public IQueryable<InterestPoint> InterestPoints()
        {
            return persistenceManager.Context.InterestPoint;
        }
        [HttpGet]
        public IQueryable<Issue> Issues()
        {
            return persistenceManager.Context.Issue;
        }
        [HttpGet]
        public IQueryable<IssueData> IssueData()
        {
            return persistenceManager.Context.IssueData;
        }
        [HttpGet]
        public IQueryable<Phone> Phones()
        {
            return persistenceManager.Context.Phone;
        }
        [HttpGet]
        public IQueryable<Room> Rooms()
        {
            return persistenceManager.Context.Room;
        }
        [HttpGet]
        public IQueryable<RoomData> RoomData()
        {
            return persistenceManager.Context.RoomData;
        }
        [HttpGet]
        public IQueryable<CharacteristicDescription> CharacteristicDescription()
        {
            return persistenceManager.Context.CharacteristicDescription;
        }
        [HttpGet]
        public IQueryable<TypeDescription> TypeDescription()
        {
            return persistenceManager.Context.TypeDescription;
        }
        [HttpGet]
        public IQueryable<RoomFacility> RoomFacilities()
        {
            return persistenceManager.Context.RoomFacility;
        }
        [HttpGet]
        public IQueryable<RoomStay> RoomStays()
        {
            return persistenceManager.Context.RoomStay;
        }
        [HttpGet]
        public IQueryable<RoomStayFacility> RoomStayFacilities()
        {
            return persistenceManager.Context.RoomStayFacility;
        }
        [HttpGet]
        public IQueryable<Terminal> Terminals()
        {
            return persistenceManager.Context.Terminal;
        }
        [HttpGet]
        public IQueryable<TerminalData> TerminalData()
        {
            return persistenceManager.Context.TerminalData;
        }
        [HttpGet]
        public IQueryable<Wildcard> Wildcards()
        {
            return persistenceManager.Context.Wildcard;
        }
        [HttpGet]
        public IQueryable<HotelRoomDescription> HotelRoomDescription()
        {
            return persistenceManager.Context.HotelRoomDescription;
        }
        [HttpGet]
        public IQueryable<Language> Languages()
        {
            return persistenceManager.Context.Language;
        }
        [HttpGet]
        public IQueryable<Segment> Segments()
        {
            return persistenceManager.Context.Segment;
        }

        //  BreezeControllerFunctionsEnd
        [HttpPost]
        public ActionResult<SaveResult> SaveChanges([FromBody] JObject saveBundle)
        {
            return persistenceManager.SaveChanges(saveBundle);
        }
        
    }
}
