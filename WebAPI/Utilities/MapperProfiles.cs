/*
CREATED BY Yiğit Bilge<yigitbilge@gmail.com> 2020-11-18 05:15:00
*/
using AutoMapper;
using Core.Entities.Concrete;
using Core.Entities.Dtos;

namespace WebAPI.Utilities
{
    public class MapperProfiles: Profile
    {
        public MapperProfiles()
        {
            CreateMap<User,UserForListDTO>();
            CreateMap<UserImage,UserImagesForListDTO>(); 
        }
    }
}