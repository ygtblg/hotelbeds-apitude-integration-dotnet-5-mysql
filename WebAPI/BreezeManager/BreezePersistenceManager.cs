﻿using Breeze.Persistence.EFCore;
using DataAccess.Concrete.EntityFramework.Contexts;


namespace WebAPI.BreezeManager
{
    public class BreezePersistenceManager : EFPersistenceManager<ApplicationDbContext>
    {
        public BreezePersistenceManager(ApplicationDbContext dbContext) : base(dbContext) { }
    }
}
