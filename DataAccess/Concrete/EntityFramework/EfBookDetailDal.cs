/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 15/08/2021 11:07:46
   ThisFileName:EfBookDetailDal.cs
   ThisFilePath:C:\projects\BackendPartner\dotnet\agile-ionic-generator-001-api\DataAccess\Concrete\EntityFramework\EfBookDetailDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfBookDetailDal : EfEntityRepositoryBase<BookDetail, ApplicationDbContext>, IBookDetailDal
    {
        
        public async Task<IList<BookDetail>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<BookDetail> query = db.Set<BookDetail>().AsQueryable<BookDetail>();
                query = query
                    .Select(
                        a => new BookDetail
                        {

                                // primitive fields
                                BookDetailId = a.BookDetailId,
                                Uuid = a.Uuid,
                                Description = a.Description,
                                BookUUID = a.BookUUID,
                                // object fields
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



