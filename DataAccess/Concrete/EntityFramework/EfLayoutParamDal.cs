/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 15/08/2021 11:07:46
   ThisFileName:EfLayoutParamDal.cs
   ThisFilePath:C:\projects\BackendPartner\dotnet\agile-ionic-generator-001-api\DataAccess\Concrete\EntityFramework\EfLayoutParamDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfLayoutParamDal : EfEntityRepositoryBase<LayoutParam, ApplicationDbContext>, ILayoutParamDal
    {
        
        public async Task<IList<LayoutParam>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<LayoutParam> query = db.Set<LayoutParam>().AsQueryable<LayoutParam>();
                query = query
                    .Select(
                        a => new LayoutParam
                        {

                                // primitive fields
                                LayoutParamId = a.LayoutParamId,
                                Uuid = a.Uuid,
                                Key = a.Key,
                                Val = a.Val,
                                LayoutUUID = a.LayoutUUID,
                                // object fields
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



