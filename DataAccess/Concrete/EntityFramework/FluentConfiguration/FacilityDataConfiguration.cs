/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:FacilityDataConfiguration.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\FluentConfiguration\FacilityDataConfiguration.cs
*/

using Microsoft.EntityFrameworkCore;
using Entities.Concrete;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace DataAccess.Concrete.EntityFramework.FluentConfiguration
{
    public class  FacilityDataConfiguration : IEntityTypeConfiguration<FacilityData>
    {
        public void Configure(EntityTypeBuilder<FacilityData> builder)
        {
            builder.Property(a => a.Code)
            .HasColumnName("FacilityDataCode");
            builder.HasKey(a => a.Code);


//			OneToOne Relation With	: Description
//			FacilityData ve Description tablolari arasinda bireBir iliski 
  			builder.HasOne( a => a.Description ) // bir 'Description' nesnesi vardir.
  			.WithOne( a => a.FacilityData ).HasForeignKey<FacilityData>("DescriptionCode");

//			OneToOne Relation With	: FacilityTypology
//			FacilityData ve FacilityTypology tablolari arasinda bireBir iliski 
  			builder.HasOne( a => a.FacilityTypology ) // bir 'FacilityTypology' nesnesi vardir.
  			.WithOne( a => a.FacilityData ).HasForeignKey<FacilityData>("FacilityTypologyCode");

//			OneToOne Relation With	: FacilityGroup
//			FacilityData ve FacilityGroup tablolari arasinda bireBir iliski 
  			builder.HasOne( a => a.FacilityGroup ) // bir 'FacilityGroup' nesnesi vardir.
  			.WithOne( a => a.FacilityData ).HasForeignKey<FacilityData>("FacilityGroupCode");
        }
    }
}
