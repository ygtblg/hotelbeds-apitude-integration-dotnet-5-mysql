/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:StateConfiguration.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\FluentConfiguration\StateConfiguration.cs
*/

using Microsoft.EntityFrameworkCore;
using Entities.Concrete;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace DataAccess.Concrete.EntityFramework.FluentConfiguration
{
    public class  StateConfiguration : IEntityTypeConfiguration<State>
    {
        public void Configure(EntityTypeBuilder<State> builder)
        {
            builder.Property(a => a.Code)
            .HasColumnName("StateCode");
            builder.HasKey(a => a.Code);


//			OneToMany Relation With	: Country
//			State ve Country tablolari arasinda bireCok iliski 
  			builder.HasOne(a => a.Country ) // State nesnesi sadece bir Country nesnesine baglidir.
  			.WithMany( a => a.States ).HasForeignKey( a => a.CountryCode ); // Country nesnesinin bircok State nesnesi vardir ve bu State nesnesini, State nesnesindeki 'CountryId' ile iliskilendirir.
        }
    }
}
