/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 15/08/2021 11:07:46
   ThisFileName:LayoutParamConfiguration.cs
   ThisFilePath:C:\projects\BackendPartner\dotnet\agile-ionic-generator-001-api\DataAccess\Concrete\EntityFramework\FluentConfiguration\LayoutParamConfiguration.cs
*/

//		PARENT Module Name : Layout
//		Current field[0].name : key
//		Current field[1].name : val
//		Current field[2].name : layoutUUID
using Microsoft.EntityFrameworkCore;
using Entities.Concrete;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace DataAccess.Concrete.EntityFramework.FluentConfiguration
{
    public class  LayoutParamConfiguration : IEntityTypeConfiguration<LayoutParam>
    {
        public void Configure(EntityTypeBuilder<LayoutParam> builder)
        {

            builder.HasKey(a => a. LayoutParamId);

//			OneToMany Relation With	: Layout
//			LayoutParam ve Layout tablolari arasinda bireCok iliski 
  			builder.HasOne(a => a.Layout ) // LayoutParam nesnesi sadece bir Layout nesnesine baglidir.
  			.WithMany( a => a.LayoutParams ).HasForeignKey( a => a.LayoutId ); // Layout nesnesinin bircok LayoutParam nesnesi vardir ve bu LayoutParam nesnesini, LayoutParam nesnesindeki 'LayoutId' ile iliskilendirir.
        }
    }
}
