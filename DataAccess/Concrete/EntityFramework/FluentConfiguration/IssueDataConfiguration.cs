/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:IssueDataConfiguration.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\FluentConfiguration\IssueDataConfiguration.cs
*/

using Microsoft.EntityFrameworkCore;
using Entities.Concrete;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace DataAccess.Concrete.EntityFramework.FluentConfiguration
{
    public class  IssueDataConfiguration : IEntityTypeConfiguration<IssueData>
    {
        public void Configure(EntityTypeBuilder<IssueData> builder)
        {
            builder.Property(a => a.Code)
            .HasColumnName("IssueDataCode");
            builder.HasKey(a => a.Code);


//			OneToOne Relation With	: Description
//			IssueData ve Description tablolari arasinda bireBir iliski 
  			builder.HasOne( a => a.Description ) // bir 'Description' nesnesi vardir.
  			.WithOne( a => a.IssueData ).HasForeignKey<IssueData>("DescriptionCode");

//			OneToOne Relation With	: Name
//			IssueData ve Name tablolari arasinda bireBir iliski 
  			builder.HasOne( a => a.Name ) // bir 'Name' nesnesi vardir.
  			.WithOne( a => a.IssueData ).HasForeignKey<IssueData>("NameCode");
        }
    }
}
