/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 15/08/2021 11:07:46
   ThisFileName:BookConfiguration.cs
   ThisFilePath:C:\projects\BackendPartner\dotnet\agile-ionic-generator-001-api\DataAccess\Concrete\EntityFramework\FluentConfiguration\BookConfiguration.cs
*/

//		REF Module Name : BookDetail
//			REF Model Field : uuid : string
//			REF Model Field : description : string
//			REF Model Field : bookUUID : string
//		REF Module Name : BookPublisher
//			REF Model Field : uuid : string
//			REF Model Field : publisherUUID : string
//			REF Model Field : bookUUID : string
//		PARENT Module Name : BookCategory
//		Current field[0].name : name
//		Current field[1].name : bookDetail
//		Current field[2].name : bookCategoryUUID
//		Current field[3].name : bookPublishers
using Microsoft.EntityFrameworkCore;
using Entities.Concrete;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace DataAccess.Concrete.EntityFramework.FluentConfiguration
{
    public class  BookConfiguration : IEntityTypeConfiguration<Book>
    {
        public void Configure(EntityTypeBuilder<Book> builder)
        {

            builder.HasKey(a => a. BookId);

//			OneToMany Relation With	: BookCategory
//			Book ve BookCategory tablolari arasinda bireCok iliski 
  			builder.HasOne(a => a.BookCategory ) // Book nesnesi sadece bir BookCategory nesnesine baglidir.
  			.WithMany( a => a.Books ).HasForeignKey( a => a.BookCategoryId ); // BookCategory nesnesinin bircok Book nesnesi vardir ve bu Book nesnesini, Book nesnesindeki 'BookCategoryId' ile iliskilendirir.

//			Ref Field : uuid : string
//			Ref Field : description : string
//			Ref Field : bookUUID : string
//			Ref Field : uuid : string
//			Ref Field : publisherUUID : string
//			Ref Field : bookUUID : string
//			OneToOne Relation With	: BookDetail
//			Book ve BookDetail tablolari arasinda bireBir iliski 
  			builder.HasOne( a => a.BookDetail ) // bir 'BookDetail' nesnesi vardir.
  			.WithOne( a => a.Book ).HasForeignKey<Book>("BookDetailId");
        }
    }
}
