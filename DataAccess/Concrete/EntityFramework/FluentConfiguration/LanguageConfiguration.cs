/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:LanguageConfiguration.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\FluentConfiguration\LanguageConfiguration.cs
*/

using Microsoft.EntityFrameworkCore;
using Entities.Concrete;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace DataAccess.Concrete.EntityFramework.FluentConfiguration
{
    public class  LanguageConfiguration : IEntityTypeConfiguration<Language>
    {
        public void Configure(EntityTypeBuilder<Language> builder)
        {
            builder.Property(a => a.Code)
            .HasColumnName("LanguageCode");
            builder.HasKey(a => a.Code);


//			OneToMany Relation With	: BookingSite
//			Language ve BookingSite tablolari arasinda bireCok iliski 
  			builder.HasOne(a => a.BookingSite ) // Language nesnesi sadece bir BookingSite nesnesine baglidir.
  			.WithMany( a => a.Languages ).HasForeignKey( a => a.BookingSiteId ); // BookingSite nesnesinin bircok Language nesnesi vardir ve bu Language nesnesini, Language nesnesindeki 'BookingSiteId' ile iliskilendirir.

//			OneToOne Relation With	: Description
//			Language ve Description tablolari arasinda bireBir iliski 
  			builder.HasOne( a => a.Description ) // bir 'Description' nesnesi vardir.
  			.WithOne( a => a.Language ).HasForeignKey<Language>("DescriptionCode");
        }
    }
}
