/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:BookingSiteConfiguration.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\FluentConfiguration\BookingSiteConfiguration.cs
*/

using Microsoft.EntityFrameworkCore;
using Entities.Concrete;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace DataAccess.Concrete.EntityFramework.FluentConfiguration
{
    public class  BookingSiteConfiguration : IEntityTypeConfiguration<BookingSite>
    {
        public void Configure(EntityTypeBuilder<BookingSite> builder)
        {
            builder.HasKey(a => a. BookingSiteId);

        }
    }
}
