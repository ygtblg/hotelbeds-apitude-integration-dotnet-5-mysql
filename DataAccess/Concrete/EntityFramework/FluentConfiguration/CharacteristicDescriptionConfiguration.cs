/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:CharacteristicDescriptionConfiguration.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\FluentConfiguration\CharacteristicDescriptionConfiguration.cs
*/

using Microsoft.EntityFrameworkCore;
using Entities.Concrete;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace DataAccess.Concrete.EntityFramework.FluentConfiguration
{
    public class  CharacteristicDescriptionConfiguration : IEntityTypeConfiguration<CharacteristicDescription>
    {
        public void Configure(EntityTypeBuilder<CharacteristicDescription> builder)
        {
            builder.HasKey(a => a. CharacteristicDescriptionId);

        }
    }
}
