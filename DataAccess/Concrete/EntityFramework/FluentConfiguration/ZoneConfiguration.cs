/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:ZoneConfiguration.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\FluentConfiguration\ZoneConfiguration.cs
*/

using Microsoft.EntityFrameworkCore;
using Entities.Concrete;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace DataAccess.Concrete.EntityFramework.FluentConfiguration
{
    public class  ZoneConfiguration : IEntityTypeConfiguration<Zone>
    {
        public void Configure(EntityTypeBuilder<Zone> builder)
        {
            builder.Property(a => a.ZoneCode)
            .HasColumnName("ZoneCode");
            builder.HasKey(a => a.ZoneCode);


//			OneToMany Relation With	: Destination
//			Zone ve Destination tablolari arasinda bireCok iliski 
  			builder.HasOne(a => a.Destination ) // Zone nesnesi sadece bir Destination nesnesine baglidir.
  			.WithMany( a => a.Zones ).HasForeignKey( a => a.DestinationCode ); // Destination nesnesinin bircok Zone nesnesi vardir ve bu Zone nesnesini, Zone nesnesindeki 'DestinationId' ile iliskilendirir.

//			OneToOne Relation With	: Description
//			Zone ve Description tablolari arasinda bireBir iliski 
  			builder.HasOne( a => a.Description ) // bir 'Description' nesnesi vardir.
  			.WithOne( a => a.Zone ).HasForeignKey<Zone>("DescriptionCode");
        }
    }
}
