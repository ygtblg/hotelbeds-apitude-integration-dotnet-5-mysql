/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:TerminalDataConfiguration.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\FluentConfiguration\TerminalDataConfiguration.cs
*/

using Microsoft.EntityFrameworkCore;
using Entities.Concrete;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace DataAccess.Concrete.EntityFramework.FluentConfiguration
{
    public class  TerminalDataConfiguration : IEntityTypeConfiguration<TerminalData>
    {
        public void Configure(EntityTypeBuilder<TerminalData> builder)
        {
            builder.Property(a => a.Code)
            .HasColumnName("TerminalDataCode");
            builder.HasKey(a => a.Code);


//			OneToOne Relation With	: Name
//			TerminalData ve Name tablolari arasinda bireBir iliski 
  			builder.HasOne( a => a.Name ) // bir 'Name' nesnesi vardir.
  			.WithOne( a => a.TerminalData ).HasForeignKey<TerminalData>("NameCode");

//			OneToOne Relation With	: Description
//			TerminalData ve Description tablolari arasinda bireBir iliski 
  			builder.HasOne( a => a.Description ) // bir 'Description' nesnesi vardir.
  			.WithOne( a => a.TerminalData ).HasForeignKey<TerminalData>("DescriptionCode");
        }
    }
}
