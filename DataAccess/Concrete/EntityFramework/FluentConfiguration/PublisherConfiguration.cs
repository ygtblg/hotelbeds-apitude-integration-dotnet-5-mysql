/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 15/08/2021 11:07:46
   ThisFileName:PublisherConfiguration.cs
   ThisFilePath:C:\projects\BackendPartner\dotnet\agile-ionic-generator-001-api\DataAccess\Concrete\EntityFramework\FluentConfiguration\PublisherConfiguration.cs
*/

//		REF Module Name : BookPublisher
//			REF Model Field : uuid : string
//			REF Model Field : publisherUUID : string
//			REF Model Field : bookUUID : string
//		PARENT Module Name : Layout
//		Current field[0].name : name
//		Current field[1].name : bookPublishers
using Microsoft.EntityFrameworkCore;
using Entities.Concrete;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace DataAccess.Concrete.EntityFramework.FluentConfiguration
{
    public class  PublisherConfiguration : IEntityTypeConfiguration<Publisher>
    {
        public void Configure(EntityTypeBuilder<Publisher> builder)
        {

            builder.HasKey(a => a. PublisherId);

//			OneToMany Relation With	: Layout
//			Publisher ve Layout tablolari arasinda bireCok iliski 
  			builder.HasOne(a => a.Layout ) // Publisher nesnesi sadece bir Layout nesnesine baglidir.
  			.WithMany( a => a.Publishers ).HasForeignKey( a => a.LayoutId ); // Layout nesnesinin bircok Publisher nesnesi vardir ve bu Publisher nesnesini, Publisher nesnesindeki 'LayoutId' ile iliskilendirir.
        }
    }
}
