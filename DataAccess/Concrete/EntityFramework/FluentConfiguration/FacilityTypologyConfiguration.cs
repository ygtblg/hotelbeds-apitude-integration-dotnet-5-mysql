/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:FacilityTypologyConfiguration.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\FluentConfiguration\FacilityTypologyConfiguration.cs
*/

using Microsoft.EntityFrameworkCore;
using Entities.Concrete;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace DataAccess.Concrete.EntityFramework.FluentConfiguration
{
    public class  FacilityTypologyConfiguration : IEntityTypeConfiguration<FacilityTypology>
    {
        public void Configure(EntityTypeBuilder<FacilityTypology> builder)
        {
            builder.Property(a => a.Code)
            .HasColumnName("FacilityTypologyCode");
            builder.HasKey(a => a.Code);

        }
    }
}
