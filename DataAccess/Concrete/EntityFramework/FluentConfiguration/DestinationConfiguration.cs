/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:DestinationConfiguration.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\FluentConfiguration\DestinationConfiguration.cs
*/

using Microsoft.EntityFrameworkCore;
using Entities.Concrete;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace DataAccess.Concrete.EntityFramework.FluentConfiguration
{
    public class  DestinationConfiguration : IEntityTypeConfiguration<Destination>
    {
        public void Configure(EntityTypeBuilder<Destination> builder)
        {
            builder.Property(a => a.Code)
            .HasColumnName("DestinationCode");
            builder.HasKey(a => a.Code);


//			OneToOne Relation With	: Name
//			Destination ve Name tablolari arasinda bireBir iliski 
  			builder.HasOne( a => a.Name ) // bir 'Name' nesnesi vardir.
  			.WithOne( a => a.Destination ).HasForeignKey<Destination>("NameCode");
        }
    }
}
