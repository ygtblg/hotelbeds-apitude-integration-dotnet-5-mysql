/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:RoomFacilityConfiguration.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\FluentConfiguration\RoomFacilityConfiguration.cs
*/

using Microsoft.EntityFrameworkCore;
using Entities.Concrete;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace DataAccess.Concrete.EntityFramework.FluentConfiguration
{
    public class  RoomFacilityConfiguration : IEntityTypeConfiguration<RoomFacility>
    {
        public void Configure(EntityTypeBuilder<RoomFacility> builder)
        {
            builder.HasKey(a => a. RoomFacilityId);


//			OneToMany Relation With	: Room
//			RoomFacility ve Room tablolari arasinda bireCok iliski 
  			builder.HasOne(a => a.Room ) // RoomFacility nesnesi sadece bir Room nesnesine baglidir.
  			.WithMany( a => a.RoomFacilities ).HasForeignKey( a => a.RoomCode ); // Room nesnesinin bircok RoomFacility nesnesi vardir ve bu RoomFacility nesnesini, RoomFacility nesnesindeki 'RoomId' ile iliskilendirir.

//			OneToOne Relation With	: FacilityData
//			RoomFacility ve FacilityData tablolari arasinda bireBir iliski 
  			builder.HasOne( a => a.FacilityData ) // bir 'FacilityData' nesnesi vardir.
  			.WithOne( a => a.RoomFacility ).HasForeignKey<RoomFacility>("FacilityDataCode");
        }
    }
}
