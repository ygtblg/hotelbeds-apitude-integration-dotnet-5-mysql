/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:HotelSegmentConfiguration.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\FluentConfiguration\HotelSegmentConfiguration.cs
*/

using Microsoft.EntityFrameworkCore;
using Entities.Concrete;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace DataAccess.Concrete.EntityFramework.FluentConfiguration
{
    public class  HotelSegmentConfiguration : IEntityTypeConfiguration<HotelSegment>
    {
        public void Configure(EntityTypeBuilder<HotelSegment> builder)
        {
            builder.HasKey(a => a. HotelSegmentId);

//			- Iki adet oneToMany field varsa ve model temel tipte bir field icermiyorsa bu bir mapModel'dir.
//			primaryTypeFieldExists :: false
//			oneToManyFieldCount :: 2
              builder.HasKey(ab => new { ab.HotelCode, ab.SegmentCode }); //  iki adet primary key tanimlandi

//			OneToMany Relation With	: Hotel
//			HotelSegment ve Hotel tablolari arasinda bireCok iliski 
  			builder.HasOne(a => a.Hotel ) // HotelSegment nesnesi sadece bir Hotel nesnesine baglidir.
  			.WithMany( a => a.HotelSegment ).HasForeignKey( a => a.HotelCode ); // Hotel nesnesinin bircok HotelSegment nesnesi vardir ve bu HotelSegment nesnesini, HotelSegment nesnesindeki 'HotelId' ile iliskilendirir.

//			OneToMany Relation With	: Segment
//			HotelSegment ve Segment tablolari arasinda bireCok iliski 
  			builder.HasOne(a => a.Segment ) // HotelSegment nesnesi sadece bir Segment nesnesine baglidir.
  			.WithMany( a => a.HotelSegment ).HasForeignKey( a => a.SegmentCode ); // Segment nesnesinin bircok HotelSegment nesnesi vardir ve bu HotelSegment nesnesini, HotelSegment nesnesindeki 'SegmentId' ile iliskilendirir.
        }
    }
}
