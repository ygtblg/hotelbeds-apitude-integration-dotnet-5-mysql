/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:ChainConfiguration.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\FluentConfiguration\ChainConfiguration.cs
*/

using Microsoft.EntityFrameworkCore;
using Entities.Concrete;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace DataAccess.Concrete.EntityFramework.FluentConfiguration
{
    public class  ChainConfiguration : IEntityTypeConfiguration<Chain>
    {
        public void Configure(EntityTypeBuilder<Chain> builder)
        {
            builder.Property(a => a.Code)
            .HasColumnName("ChainCode");
            builder.HasKey(a => a.Code);


//			OneToOne Relation With	: Description
//			Chain ve Description tablolari arasinda bireBir iliski 
  			builder.HasOne( a => a.Description ) // bir 'Description' nesnesi vardir.
  			.WithOne( a => a.Chain ).HasForeignKey<Chain>("DescriptionCode");
        }
    }
}
