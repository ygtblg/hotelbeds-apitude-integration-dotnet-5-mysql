/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:FacilityConfiguration.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\FluentConfiguration\FacilityConfiguration.cs
*/

using Microsoft.EntityFrameworkCore;
using Entities.Concrete;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
namespace DataAccess.Concrete.EntityFramework.FluentConfiguration
{
    public class  FacilityConfiguration : IEntityTypeConfiguration<Facility>
    {
        public void Configure(EntityTypeBuilder<Facility> builder)
        {
            builder.Property(a => a.FacilityCode)
            .HasColumnName("FacilityCode");
            builder.HasKey(a => a.FacilityCode);

            builder.Property(c => c.TimeFrom)
            .HasColumnType(@"time").HasConversion(v => v.TimeOfDay, v => DateTime.Now.Date.Add(v) );

            builder.Property(c => c.TimeTo)
            .HasColumnType(@"time").HasConversion(v => v.TimeOfDay, v => DateTime.Now.Date.Add(v) );

            builder.Property(c => c.DateTo)
            .HasColumnType("date");


//			OneToMany Relation With	: Hotel
//			Facility ve Hotel tablolari arasinda bireCok iliski 
  			builder.HasOne(a => a.Hotel ) // Facility nesnesi sadece bir Hotel nesnesine baglidir.
  			.WithMany( a => a.Facilities ).HasForeignKey( a => a.HotelCode ); // Hotel nesnesinin bircok Facility nesnesi vardir ve bu Facility nesnesini, Facility nesnesindeki 'HotelId' ile iliskilendirir.

//			OneToOne Relation With	: FacilityData
//			Facility ve FacilityData tablolari arasinda bireBir iliski 
  			builder.HasOne( a => a.FacilityData ) // bir 'FacilityData' nesnesi vardir.
  			.WithOne( a => a.Facility ).HasForeignKey<Facility>("FacilityDataCode");
        }
    }
}
