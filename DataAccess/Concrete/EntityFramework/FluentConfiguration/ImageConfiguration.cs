/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:ImageConfiguration.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\FluentConfiguration\ImageConfiguration.cs
*/

using Microsoft.EntityFrameworkCore;
using Entities.Concrete;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace DataAccess.Concrete.EntityFramework.FluentConfiguration
{
    public class  ImageConfiguration : IEntityTypeConfiguration<Image>
    {
        public void Configure(EntityTypeBuilder<Image> builder)
        {
            builder.HasKey(a => a. ImageId);


//			OneToMany Relation With	: Hotel
//			Image ve Hotel tablolari arasinda bireCok iliski 
  			builder.HasOne(a => a.Hotel ) // Image nesnesi sadece bir Hotel nesnesine baglidir.
  			.WithMany( a => a.Images ).HasForeignKey( a => a.HotelCode ); // Hotel nesnesinin bircok Image nesnesi vardir ve bu Image nesnesini, Image nesnesindeki 'HotelId' ile iliskilendirir.

//			OneToOne Relation With	: ImageType
//			Image ve ImageType tablolari arasinda bireBir iliski 
  			builder.HasOne( a => a.ImageType ) // bir 'ImageType' nesnesi vardir.
  			.WithOne( a => a.Image ).HasForeignKey<Image>("ImageTypeCode");
        }
    }
}
