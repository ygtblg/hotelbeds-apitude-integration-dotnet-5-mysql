/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:RoomConfiguration.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\FluentConfiguration\RoomConfiguration.cs
*/

using Microsoft.EntityFrameworkCore;
using Entities.Concrete;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace DataAccess.Concrete.EntityFramework.FluentConfiguration
{
    public class  RoomConfiguration : IEntityTypeConfiguration<Room>
    {
        public void Configure(EntityTypeBuilder<Room> builder)
        {
            builder.Property(a => a.RoomCode)
            .HasColumnName("RoomCode");
            builder.HasKey(a => a.RoomCode);


//			OneToMany Relation With	: Hotel
//			Room ve Hotel tablolari arasinda bireCok iliski 
  			builder.HasOne(a => a.Hotel ) // Room nesnesi sadece bir Hotel nesnesine baglidir.
  			.WithMany( a => a.Rooms ).HasForeignKey( a => a.HotelCode ); // Hotel nesnesinin bircok Room nesnesi vardir ve bu Room nesnesini, Room nesnesindeki 'HotelId' ile iliskilendirir.

//			OneToOne Relation With	: RoomData
//			Room ve RoomData tablolari arasinda bireBir iliski 
  			builder.HasOne( a => a.RoomData ) // bir 'RoomData' nesnesi vardir.
  			.WithOne( a => a.Room ).HasForeignKey<Room>("RoomDataCode");
        }
    }
}
