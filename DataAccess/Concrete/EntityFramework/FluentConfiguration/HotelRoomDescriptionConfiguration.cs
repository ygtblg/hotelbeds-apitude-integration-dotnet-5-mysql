/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:HotelRoomDescriptionConfiguration.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\FluentConfiguration\HotelRoomDescriptionConfiguration.cs
*/

using Microsoft.EntityFrameworkCore;
using Entities.Concrete;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace DataAccess.Concrete.EntityFramework.FluentConfiguration
{
    public class  HotelRoomDescriptionConfiguration : IEntityTypeConfiguration<HotelRoomDescription>
    {
        public void Configure(EntityTypeBuilder<HotelRoomDescription> builder)
        {
            builder.HasKey(a => a. HotelRoomDescriptionId);

        }
    }
}
