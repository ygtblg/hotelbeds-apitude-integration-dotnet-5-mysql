/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 15/08/2021 11:07:46
   ThisFileName:BookPublisherConfiguration.cs
   ThisFilePath:C:\projects\BackendPartner\dotnet\agile-ionic-generator-001-api\DataAccess\Concrete\EntityFramework\FluentConfiguration\BookPublisherConfiguration.cs
*/

//		PARENT Module Name : Book
//		PARENT Module Name : Publisher
//		Current field[0].name : publisherUUID
//		Current field[1].name : bookUUID
using Microsoft.EntityFrameworkCore;
using Entities.Concrete;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace DataAccess.Concrete.EntityFramework.FluentConfiguration
{
    public class  BookPublisherConfiguration : IEntityTypeConfiguration<BookPublisher>
    {
        public void Configure(EntityTypeBuilder<BookPublisher> builder)
        {

//			- Iki adet oneToMany field varsa ve model temel tipte bir field icermiyorsa bu bir mapModel'dir.
//			primaryTypeFieldExists :: false
//			oneToManyFieldCount :: 2
              builder.HasKey(ab => new { ab.BookId, ab.PublisherId }); //  iki adet primary key tanimlandi

//			OneToMany Relation With	: Book
//			BookPublisher ve Book tablolari arasinda bireCok iliski 
  			builder.HasOne(a => a.Book ) // BookPublisher nesnesi sadece bir Book nesnesine baglidir.
  			.WithMany( a => a.BookPublishers ).HasForeignKey( a => a.BookId ); // Book nesnesinin bircok BookPublisher nesnesi vardir ve bu BookPublisher nesnesini, BookPublisher nesnesindeki 'BookId' ile iliskilendirir.

//			OneToMany Relation With	: Publisher
//			BookPublisher ve Publisher tablolari arasinda bireCok iliski 
  			builder.HasOne(a => a.Publisher ) // BookPublisher nesnesi sadece bir Publisher nesnesine baglidir.
  			.WithMany( a => a.BookPublishers ).HasForeignKey( a => a.PublisherId ); // Publisher nesnesinin bircok BookPublisher nesnesi vardir ve bu BookPublisher nesnesini, BookPublisher nesnesindeki 'PublisherId' ile iliskilendirir.
        }
    }
}
