/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 15/08/2021 11:07:46
   ThisFileName:LayoutConfiguration.cs
   ThisFilePath:C:\projects\BackendPartner\dotnet\agile-ionic-generator-001-api\DataAccess\Concrete\EntityFramework\FluentConfiguration\LayoutConfiguration.cs
*/

//		REF Module Name : LayoutParam
//			REF Model Field : uuid : string
//			REF Model Field : key : string
//			REF Model Field : val : string
//			REF Model Field : layoutUUID : string
//		REF Module Name : BookCategory
//			REF Model Field : uuid : string
//			REF Model Field : name : string
//			REF Model Field : books : IBook[]
//			REF Model Field : layoutUUID : string
//		REF Module Name : Publisher
//			REF Model Field : uuid : string
//			REF Model Field : name : string
//			REF Model Field : bookPublishers : IBookPublisher[]
//		Current field[0].name : loggedIn
//		Current field[1].name : error
//		Current field[2].name : success
//		Current field[3].name : isRunningMobile
//		Current field[4].name : windowWidth
//		Current field[5].name : windowHeight
//		Current field[6].name : layoutParams
//		Current field[7].name : bookCategories
//		Current field[8].name : publishers
using Microsoft.EntityFrameworkCore;
using Entities.Concrete;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace DataAccess.Concrete.EntityFramework.FluentConfiguration
{
    public class  LayoutConfiguration : IEntityTypeConfiguration<Layout>
    {
        public void Configure(EntityTypeBuilder<Layout> builder)
        {

        }
    }
}
