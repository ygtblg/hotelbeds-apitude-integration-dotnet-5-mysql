/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:FacilityGroupConfiguration.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\FluentConfiguration\FacilityGroupConfiguration.cs
*/

using Microsoft.EntityFrameworkCore;
using Entities.Concrete;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace DataAccess.Concrete.EntityFramework.FluentConfiguration
{
    public class  FacilityGroupConfiguration : IEntityTypeConfiguration<FacilityGroup>
    {
        public void Configure(EntityTypeBuilder<FacilityGroup> builder)
        {
            builder.Property(a => a.Code)
            .HasColumnName("FacilityGroupCode");
            builder.HasKey(a => a.Code);


//			OneToOne Relation With	: Description
//			FacilityGroup ve Description tablolari arasinda bireBir iliski 
  			builder.HasOne( a => a.Description ) // bir 'Description' nesnesi vardir.
  			.WithOne( a => a.FacilityGroup ).HasForeignKey<FacilityGroup>("DescriptionCode");
        }
    }
}
