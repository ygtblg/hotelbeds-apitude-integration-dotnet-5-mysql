/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:InterestPointConfiguration.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\FluentConfiguration\InterestPointConfiguration.cs
*/

using Microsoft.EntityFrameworkCore;
using Entities.Concrete;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace DataAccess.Concrete.EntityFramework.FluentConfiguration
{
    public class  InterestPointConfiguration : IEntityTypeConfiguration<InterestPoint>
    {
        public void Configure(EntityTypeBuilder<InterestPoint> builder)
        {
            builder.HasKey(a => a. InterestPointId);


//			OneToMany Relation With	: Hotel
//			InterestPoint ve Hotel tablolari arasinda bireCok iliski 
  			builder.HasOne(a => a.Hotel ) // InterestPoint nesnesi sadece bir Hotel nesnesine baglidir.
  			.WithMany( a => a.InterestPoints ).HasForeignKey( a => a.HotelCode ); // Hotel nesnesinin bircok InterestPoint nesnesi vardir ve bu InterestPoint nesnesini, InterestPoint nesnesindeki 'HotelId' ile iliskilendirir.

//			OneToOne Relation With	: FacilityData
//			InterestPoint ve FacilityData tablolari arasinda bireBir iliski 
  			builder.HasOne( a => a.FacilityData ) // bir 'FacilityData' nesnesi vardir.
  			.WithOne( a => a.InterestPoint ).HasForeignKey<InterestPoint>("FacilityDataCode");
        }
    }
}
