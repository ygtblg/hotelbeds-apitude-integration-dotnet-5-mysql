/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:RoomStayConfiguration.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\FluentConfiguration\RoomStayConfiguration.cs
*/

using Microsoft.EntityFrameworkCore;
using Entities.Concrete;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace DataAccess.Concrete.EntityFramework.FluentConfiguration
{
    public class  RoomStayConfiguration : IEntityTypeConfiguration<RoomStay>
    {
        public void Configure(EntityTypeBuilder<RoomStay> builder)
        {
            builder.HasKey(a => a. RoomStayId);


//			OneToMany Relation With	: Room
//			RoomStay ve Room tablolari arasinda bireCok iliski 
  			builder.HasOne(a => a.Room ) // RoomStay nesnesi sadece bir Room nesnesine baglidir.
  			.WithMany( a => a.RoomStays ).HasForeignKey( a => a.RoomCode ); // Room nesnesinin bircok RoomStay nesnesi vardir ve bu RoomStay nesnesini, RoomStay nesnesindeki 'RoomId' ile iliskilendirir.
        }
    }
}
