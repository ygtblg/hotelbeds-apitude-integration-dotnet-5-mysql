/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:BoardConfiguration.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\FluentConfiguration\BoardConfiguration.cs
*/

using Microsoft.EntityFrameworkCore;
using Entities.Concrete;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace DataAccess.Concrete.EntityFramework.FluentConfiguration
{
    public class  BoardConfiguration : IEntityTypeConfiguration<Board>
    {
        public void Configure(EntityTypeBuilder<Board> builder)
        {
            builder.Property(a => a.Code)
            .HasColumnName("BoardCode");
            builder.HasKey(a => a.Code);


//			OneToMany Relation With	: BookingSite
//			Board ve BookingSite tablolari arasinda bireCok iliski 
  			builder.HasOne(a => a.BookingSite ) // Board nesnesi sadece bir BookingSite nesnesine baglidir.
  			.WithMany( a => a.Boards ).HasForeignKey( a => a.BookingSiteId ); // BookingSite nesnesinin bircok Board nesnesi vardir ve bu Board nesnesini, Board nesnesindeki 'BookingSiteId' ile iliskilendirir.

//			OneToOne Relation With	: Description
//			Board ve Description tablolari arasinda bireBir iliski 
  			builder.HasOne( a => a.Description ) // bir 'Description' nesnesi vardir.
  			.WithOne( a => a.Board ).HasForeignKey<Board>("DescriptionCode");
        }
    }
}
