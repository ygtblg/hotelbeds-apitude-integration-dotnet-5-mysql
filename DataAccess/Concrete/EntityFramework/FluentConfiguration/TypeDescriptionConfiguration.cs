/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:TypeDescriptionConfiguration.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\FluentConfiguration\TypeDescriptionConfiguration.cs
*/

using Microsoft.EntityFrameworkCore;
using Entities.Concrete;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace DataAccess.Concrete.EntityFramework.FluentConfiguration
{
    public class  TypeDescriptionConfiguration : IEntityTypeConfiguration<TypeDescription>
    {
        public void Configure(EntityTypeBuilder<TypeDescription> builder)
        {
            builder.HasKey(a => a. TypeDescriptionId);

        }
    }
}
