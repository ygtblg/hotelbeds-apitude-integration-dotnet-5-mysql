/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:TerminalConfiguration.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\FluentConfiguration\TerminalConfiguration.cs
*/

using Microsoft.EntityFrameworkCore;
using Entities.Concrete;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace DataAccess.Concrete.EntityFramework.FluentConfiguration
{
    public class  TerminalConfiguration : IEntityTypeConfiguration<Terminal>
    {
        public void Configure(EntityTypeBuilder<Terminal> builder)
        {
            builder.Property(a => a.TerminalCode)
            .HasColumnName("TerminalCode");
            builder.HasKey(a => a.TerminalCode);


//			OneToMany Relation With	: Hotel
//			Terminal ve Hotel tablolari arasinda bireCok iliski 
  			builder.HasOne(a => a.Hotel ) // Terminal nesnesi sadece bir Hotel nesnesine baglidir.
  			.WithMany( a => a.Terminals ).HasForeignKey( a => a.HotelCode ); // Hotel nesnesinin bircok Terminal nesnesi vardir ve bu Terminal nesnesini, Terminal nesnesindeki 'HotelId' ile iliskilendirir.

//			OneToOne Relation With	: TerminalData
//			Terminal ve TerminalData tablolari arasinda bireBir iliski 
  			builder.HasOne( a => a.TerminalData ) // bir 'TerminalData' nesnesi vardir.
  			.WithOne( a => a.Terminal ).HasForeignKey<Terminal>("TerminalDataCode");
        }
    }
}
