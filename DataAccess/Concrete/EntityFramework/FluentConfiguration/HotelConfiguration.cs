/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:HotelConfiguration.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\FluentConfiguration\HotelConfiguration.cs
*/

using Microsoft.EntityFrameworkCore;
using Entities.Concrete;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Newtonsoft.Json;
using System.Collections.Generic;
namespace DataAccess.Concrete.EntityFramework.FluentConfiguration
{
    public class  HotelConfiguration : IEntityTypeConfiguration<Hotel>
    {
        public void Configure(EntityTypeBuilder<Hotel> builder)
        {
            builder.Property(a => a.Code)
            .HasColumnName("HotelCode");
            builder.HasKey(a => a.Code);

            builder.Property(e => e.BoardCodes)
            .HasConversion(
				v => JsonConvert.SerializeObject(v),
				v => JsonConvert.DeserializeObject<List<string>>(v));

            builder.Property(e => e.SegmentCodes)
            .HasConversion(
				v => JsonConvert.SerializeObject(v),
				v => JsonConvert.DeserializeObject<List<int>>(v));


//			OneToMany Relation With	: BookingSite
//			Hotel ve BookingSite tablolari arasinda bireCok iliski 
  			builder.HasOne(a => a.BookingSite ) // Hotel nesnesi sadece bir BookingSite nesnesine baglidir.
  			.WithMany( a => a.Hotels ).HasForeignKey( a => a.BookingSiteId ); // BookingSite nesnesinin bircok Hotel nesnesi vardir ve bu Hotel nesnesini, Hotel nesnesindeki 'BookingSiteId' ile iliskilendirir.

//			OneToOne Relation With	: Name
//			Hotel ve Name tablolari arasinda bireBir iliski 
  			builder.HasOne( a => a.Name ) // bir 'Name' nesnesi vardir.
  			.WithOne( a => a.Hotel ).HasForeignKey<Hotel>("NameCode");

//			OneToOne Relation With	: Description
//			Hotel ve Description tablolari arasinda bireBir iliski 
  			builder.HasOne( a => a.Description ) // bir 'Description' nesnesi vardir.
  			.WithOne( a => a.Hotel ).HasForeignKey<Hotel>("DescriptionCode");

//			OneToOne Relation With	: Country
//			Hotel ve Country tablolari arasinda bireBir iliski 
  			builder.HasOne( a => a.Country ) // bir 'Country' nesnesi vardir.
  			.WithOne( a => a.Hotel ).HasForeignKey<Hotel>("CountryCode");

//			OneToOne Relation With	: State
//			Hotel ve State tablolari arasinda bireBir iliski 
  			builder.HasOne( a => a.State ) // bir 'State' nesnesi vardir.
  			.WithOne( a => a.Hotel ).HasForeignKey<Hotel>("StateCode");

//			OneToOne Relation With	: Destination
//			Hotel ve Destination tablolari arasinda bireBir iliski 
  			builder.HasOne( a => a.Destination ) // bir 'Destination' nesnesi vardir.
  			.WithOne( a => a.Hotel ).HasForeignKey<Hotel>("DestinationCode");

//			OneToOne Relation With	: Zone
//			Hotel ve Zone tablolari arasinda bireBir iliski 
  			builder.HasOne( a => a.Zone ) // bir 'Zone' nesnesi vardir.
  			.WithOne( a => a.Hotel ).HasForeignKey<Hotel>("ZoneCode");

//			OneToOne Relation With	: Coordinates
//			Hotel ve Coordinates tablolari arasinda bireBir iliski 
  			builder.HasOne( a => a.Coordinates ) // bir 'Coordinates' nesnesi vardir.
  			.WithOne( a => a.Hotel ).HasForeignKey<Hotel>("CoordinatesId");

//			OneToOne Relation With	: Category
//			Hotel ve Category tablolari arasinda bireBir iliski 
  			builder.HasOne( a => a.Category ) // bir 'Category' nesnesi vardir.
  			.WithOne( a => a.Hotel ).HasForeignKey<Hotel>("CategoryCode");

//			OneToOne Relation With	: CategoryGroup
//			Hotel ve CategoryGroup tablolari arasinda bireBir iliski 
  			builder.HasOne( a => a.CategoryGroup ) // bir 'CategoryGroup' nesnesi vardir.
  			.WithOne( a => a.Hotel ).HasForeignKey<Hotel>("CategoryGroupCode");

//			OneToOne Relation With	: Chain
//			Hotel ve Chain tablolari arasinda bireBir iliski 
  			builder.HasOne( a => a.Chain ) // bir 'Chain' nesnesi vardir.
  			.WithOne( a => a.Hotel ).HasForeignKey<Hotel>("ChainCode");

//			OneToOne Relation With	: Accommodation
//			Hotel ve Accommodation tablolari arasinda bireBir iliski 
  			builder.HasOne( a => a.Accommodation ) // bir 'Accommodation' nesnesi vardir.
  			.WithOne( a => a.Hotel ).HasForeignKey<Hotel>("AccommodationCode");

//			OneToOne Relation With	: Address
//			Hotel ve Address tablolari arasinda bireBir iliski 
  			builder.HasOne( a => a.Address ) // bir 'Address' nesnesi vardir.
  			.WithOne( a => a.Hotel ).HasForeignKey<Hotel>("AddressId");

//			OneToOne Relation With	: City
//			Hotel ve City tablolari arasinda bireBir iliski 
  			builder.HasOne( a => a.City ) // bir 'City' nesnesi vardir.
  			.WithOne( a => a.Hotel ).HasForeignKey<Hotel>("CityCode");
        }
    }
}
