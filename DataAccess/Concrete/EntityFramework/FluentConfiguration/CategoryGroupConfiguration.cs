/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:CategoryGroupConfiguration.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\FluentConfiguration\CategoryGroupConfiguration.cs
*/

using Microsoft.EntityFrameworkCore;
using Entities.Concrete;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace DataAccess.Concrete.EntityFramework.FluentConfiguration
{
    public class  CategoryGroupConfiguration : IEntityTypeConfiguration<CategoryGroup>
    {
        public void Configure(EntityTypeBuilder<CategoryGroup> builder)
        {
            builder.Property(a => a.Code)
            .HasColumnName("CategoryGroupCode");
            builder.HasKey(a => a.Code);


//			OneToOne Relation With	: Name
//			CategoryGroup ve Name tablolari arasinda bireBir iliski 
  			builder.HasOne( a => a.Name ) // bir 'Name' nesnesi vardir.
  			.WithOne( a => a.CategoryGroup ).HasForeignKey<CategoryGroup>("NameCode");

//			OneToOne Relation With	: Description
//			CategoryGroup ve Description tablolari arasinda bireBir iliski 
  			builder.HasOne( a => a.Description ) // bir 'Description' nesnesi vardir.
  			.WithOne( a => a.CategoryGroup ).HasForeignKey<CategoryGroup>("DescriptionCode");
        }
    }
}
