/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:SegmentConfiguration.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\FluentConfiguration\SegmentConfiguration.cs
*/

using Microsoft.EntityFrameworkCore;
using Entities.Concrete;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace DataAccess.Concrete.EntityFramework.FluentConfiguration
{
    public class  SegmentConfiguration : IEntityTypeConfiguration<Segment>
    {
        public void Configure(EntityTypeBuilder<Segment> builder)
        {
            builder.Property(a => a.Code)
            .HasColumnName("SegmentCode");
            builder.HasKey(a => a.Code);


//			OneToMany Relation With	: BookingSite
//			Segment ve BookingSite tablolari arasinda bireCok iliski 
  			builder.HasOne(a => a.BookingSite ) // Segment nesnesi sadece bir BookingSite nesnesine baglidir.
  			.WithMany( a => a.Segments ).HasForeignKey( a => a.BookingSiteId ); // BookingSite nesnesinin bircok Segment nesnesi vardir ve bu Segment nesnesini, Segment nesnesindeki 'BookingSiteId' ile iliskilendirir.

//			OneToOne Relation With	: Description
//			Segment ve Description tablolari arasinda bireBir iliski 
  			builder.HasOne( a => a.Description ) // bir 'Description' nesnesi vardir.
  			.WithOne( a => a.Segment ).HasForeignKey<Segment>("DescriptionCode");
        }
    }
}
