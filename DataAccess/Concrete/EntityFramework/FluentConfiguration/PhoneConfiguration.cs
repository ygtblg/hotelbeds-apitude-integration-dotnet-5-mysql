/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:PhoneConfiguration.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\FluentConfiguration\PhoneConfiguration.cs
*/

using Microsoft.EntityFrameworkCore;
using Entities.Concrete;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace DataAccess.Concrete.EntityFramework.FluentConfiguration
{
    public class  PhoneConfiguration : IEntityTypeConfiguration<Phone>
    {
        public void Configure(EntityTypeBuilder<Phone> builder)
        {
            builder.HasKey(a => a. PhoneId);


//			OneToMany Relation With	: Hotel
//			Phone ve Hotel tablolari arasinda bireCok iliski 
  			builder.HasOne(a => a.Hotel ) // Phone nesnesi sadece bir Hotel nesnesine baglidir.
  			.WithMany( a => a.Phones ).HasForeignKey( a => a.HotelCode ); // Hotel nesnesinin bircok Phone nesnesi vardir ve bu Phone nesnesini, Phone nesnesindeki 'HotelId' ile iliskilendirir.
        }
    }
}
