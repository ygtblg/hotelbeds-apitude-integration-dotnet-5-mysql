/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:HotelBoardConfiguration.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\FluentConfiguration\HotelBoardConfiguration.cs
*/

using Microsoft.EntityFrameworkCore;
using Entities.Concrete;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace DataAccess.Concrete.EntityFramework.FluentConfiguration
{
    public class  HotelBoardConfiguration : IEntityTypeConfiguration<HotelBoard>
    {
        public void Configure(EntityTypeBuilder<HotelBoard> builder)
        {
            builder.HasKey(a => a. HotelBoardId);

//			- Iki adet oneToMany field varsa ve model temel tipte bir field icermiyorsa bu bir mapModel'dir.
//			primaryTypeFieldExists :: false
//			oneToManyFieldCount :: 2
              builder.HasKey(ab => new { ab.BoardCode, ab.HotelCode }); //  iki adet primary key tanimlandi

//			OneToMany Relation With	: Board
//			HotelBoard ve Board tablolari arasinda bireCok iliski 
  			builder.HasOne(a => a.Board ) // HotelBoard nesnesi sadece bir Board nesnesine baglidir.
  			.WithMany( a => a.HotelBoard ).HasForeignKey( a => a.BoardCode ); // Board nesnesinin bircok HotelBoard nesnesi vardir ve bu HotelBoard nesnesini, HotelBoard nesnesindeki 'BoardId' ile iliskilendirir.

//			OneToMany Relation With	: Hotel
//			HotelBoard ve Hotel tablolari arasinda bireCok iliski 
  			builder.HasOne(a => a.Hotel ) // HotelBoard nesnesi sadece bir Hotel nesnesine baglidir.
  			.WithMany( a => a.HotelBoard ).HasForeignKey( a => a.HotelCode ); // Hotel nesnesinin bircok HotelBoard nesnesi vardir ve bu HotelBoard nesnesini, HotelBoard nesnesindeki 'HotelId' ile iliskilendirir.
        }
    }
}
