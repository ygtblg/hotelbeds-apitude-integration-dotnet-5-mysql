/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:CategoryConfiguration.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\FluentConfiguration\CategoryConfiguration.cs
*/

using Microsoft.EntityFrameworkCore;
using Entities.Concrete;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace DataAccess.Concrete.EntityFramework.FluentConfiguration
{
    public class  CategoryConfiguration : IEntityTypeConfiguration<Category>
    {
        public void Configure(EntityTypeBuilder<Category> builder)
        {
            builder.Property(a => a.Code)
            .HasColumnName("CategoryCode");
            builder.HasKey(a => a.Code);


//			OneToOne Relation With	: Description
//			Category ve Description tablolari arasinda bireBir iliski 
  			builder.HasOne( a => a.Description ) // bir 'Description' nesnesi vardir.
  			.WithOne( a => a.Category ).HasForeignKey<Category>("DescriptionCode");
        }
    }
}
