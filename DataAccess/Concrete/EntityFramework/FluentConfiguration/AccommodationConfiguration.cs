/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:AccommodationConfiguration.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\FluentConfiguration\AccommodationConfiguration.cs
*/

using Microsoft.EntityFrameworkCore;
using Entities.Concrete;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace DataAccess.Concrete.EntityFramework.FluentConfiguration
{
    public class  AccommodationConfiguration : IEntityTypeConfiguration<Accommodation>
    {
        public void Configure(EntityTypeBuilder<Accommodation> builder)
        {
            builder.Property(a => a.Code)
            .HasColumnName("AccommodationCode");
            builder.HasKey(a => a.Code);


//			OneToOne Relation With	: TypeMultiDescription
//			Accommodation ve TypeMultiDescription tablolari arasinda bireBir iliski 
  			builder.HasOne( a => a.TypeMultiDescription ) // bir 'TypeMultiDescription' nesnesi vardir.
  			.WithOne( a => a.Accommodation ).HasForeignKey<Accommodation>("TypeMultiDescriptionCode");
        }
    }
}
