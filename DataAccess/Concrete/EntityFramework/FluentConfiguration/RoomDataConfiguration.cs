/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:RoomDataConfiguration.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\FluentConfiguration\RoomDataConfiguration.cs
*/

using Microsoft.EntityFrameworkCore;
using Entities.Concrete;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace DataAccess.Concrete.EntityFramework.FluentConfiguration
{
    public class  RoomDataConfiguration : IEntityTypeConfiguration<RoomData>
    {
        public void Configure(EntityTypeBuilder<RoomData> builder)
        {
            builder.Property(a => a.Code)
            .HasColumnName("RoomDataCode");
            builder.HasKey(a => a.Code);


//			OneToOne Relation With	: TypeDescription
//			RoomData ve TypeDescription tablolari arasinda bireBir iliski 
  			builder.HasOne( a => a.TypeDescription ) // bir 'TypeDescription' nesnesi vardir.
  			.WithOne( a => a.RoomData ).HasForeignKey<RoomData>("TypeDescriptionCode");

//			OneToOne Relation With	: CharacteristicDescription
//			RoomData ve CharacteristicDescription tablolari arasinda bireBir iliski 
  			builder.HasOne( a => a.CharacteristicDescription ) // bir 'CharacteristicDescription' nesnesi vardir.
  			.WithOne( a => a.RoomData ).HasForeignKey<RoomData>("CharacteristicDescriptionCode");
        }
    }
}
