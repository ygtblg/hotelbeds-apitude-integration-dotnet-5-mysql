/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:WildcardConfiguration.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\FluentConfiguration\WildcardConfiguration.cs
*/

using Microsoft.EntityFrameworkCore;
using Entities.Concrete;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace DataAccess.Concrete.EntityFramework.FluentConfiguration
{
    public class  WildcardConfiguration : IEntityTypeConfiguration<Wildcard>
    {
        public void Configure(EntityTypeBuilder<Wildcard> builder)
        {
            builder.HasKey(a => a. WildcardId);


//			OneToMany Relation With	: Hotel
//			Wildcard ve Hotel tablolari arasinda bireCok iliski 
  			builder.HasOne(a => a.Hotel ) // Wildcard nesnesi sadece bir Hotel nesnesine baglidir.
  			.WithMany( a => a.Wildcards ).HasForeignKey( a => a.HotelCode ); // Hotel nesnesinin bircok Wildcard nesnesi vardir ve bu Wildcard nesnesini, Wildcard nesnesindeki 'HotelId' ile iliskilendirir.

//			OneToOne Relation With	: HotelRoomDescription
//			Wildcard ve HotelRoomDescription tablolari arasinda bireBir iliski 
  			builder.HasOne( a => a.HotelRoomDescription ) // bir 'HotelRoomDescription' nesnesi vardir.
  			.WithOne( a => a.Wildcard ).HasForeignKey<Wildcard>("HotelRoomDescriptionId");
        }
    }
}
