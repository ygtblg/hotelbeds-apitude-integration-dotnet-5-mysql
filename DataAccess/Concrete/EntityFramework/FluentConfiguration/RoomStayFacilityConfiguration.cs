/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:RoomStayFacilityConfiguration.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\FluentConfiguration\RoomStayFacilityConfiguration.cs
*/

using Microsoft.EntityFrameworkCore;
using Entities.Concrete;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace DataAccess.Concrete.EntityFramework.FluentConfiguration
{
    public class  RoomStayFacilityConfiguration : IEntityTypeConfiguration<RoomStayFacility>
    {
        public void Configure(EntityTypeBuilder<RoomStayFacility> builder)
        {
            builder.HasKey(a => a. RoomStayFacilityId);


//			OneToMany Relation With	: RoomStay
//			RoomStayFacility ve RoomStay tablolari arasinda bireCok iliski 
  			builder.HasOne(a => a.RoomStay ) // RoomStayFacility nesnesi sadece bir RoomStay nesnesine baglidir.
  			.WithMany( a => a.RoomStayFacilities ).HasForeignKey( a => a.RoomStayId ); // RoomStay nesnesinin bircok RoomStayFacility nesnesi vardir ve bu RoomStayFacility nesnesini, RoomStayFacility nesnesindeki 'RoomStayId' ile iliskilendirir.

//			OneToOne Relation With	: FacilityData
//			RoomStayFacility ve FacilityData tablolari arasinda bireBir iliski 
  			builder.HasOne( a => a.FacilityData ) // bir 'FacilityData' nesnesi vardir.
  			.WithOne( a => a.RoomStayFacility ).HasForeignKey<RoomStayFacility>("FacilityDataCode");
        }
    }
}
