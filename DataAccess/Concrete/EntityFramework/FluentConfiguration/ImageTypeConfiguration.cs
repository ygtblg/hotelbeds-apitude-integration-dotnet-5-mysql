/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:ImageTypeConfiguration.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\FluentConfiguration\ImageTypeConfiguration.cs
*/

using Microsoft.EntityFrameworkCore;
using Entities.Concrete;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace DataAccess.Concrete.EntityFramework.FluentConfiguration
{
    public class  ImageTypeConfiguration : IEntityTypeConfiguration<ImageType>
    {
        public void Configure(EntityTypeBuilder<ImageType> builder)
        {
            builder.Property(a => a.Code)
            .HasColumnName("ImageTypeCode");
            builder.HasKey(a => a.Code);


//			OneToOne Relation With	: Description
//			ImageType ve Description tablolari arasinda bireBir iliski 
  			builder.HasOne( a => a.Description ) // bir 'Description' nesnesi vardir.
  			.WithOne( a => a.ImageType ).HasForeignKey<ImageType>("DescriptionCode");
        }
    }
}
