/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:TypeMultiDescriptionConfiguration.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\FluentConfiguration\TypeMultiDescriptionConfiguration.cs
*/

using Microsoft.EntityFrameworkCore;
using Entities.Concrete;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace DataAccess.Concrete.EntityFramework.FluentConfiguration
{
    public class  TypeMultiDescriptionConfiguration : IEntityTypeConfiguration<TypeMultiDescription>
    {
        public void Configure(EntityTypeBuilder<TypeMultiDescription> builder)
        {
            builder.HasKey(a => a. TypeMultiDescriptionId);

        }
    }
}
