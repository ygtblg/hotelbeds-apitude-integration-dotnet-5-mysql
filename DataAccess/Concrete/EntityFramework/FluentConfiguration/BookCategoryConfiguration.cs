/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 15/08/2021 11:07:46
   ThisFileName:BookCategoryConfiguration.cs
   ThisFilePath:C:\projects\BackendPartner\dotnet\agile-ionic-generator-001-api\DataAccess\Concrete\EntityFramework\FluentConfiguration\BookCategoryConfiguration.cs
*/

//		REF Module Name : Book
//			REF Model Field : uuid : string
//			REF Model Field : name : string
//			REF Model Field : bookDetail : IBookDetail
//			REF Model Field : bookCategoryUUID : string
//			REF Model Field : bookPublishers : IBookPublisher[]
//		PARENT Module Name : Layout
//		Current field[0].name : name
//		Current field[1].name : books
//		Current field[2].name : layoutUUID
using Microsoft.EntityFrameworkCore;
using Entities.Concrete;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace DataAccess.Concrete.EntityFramework.FluentConfiguration
{
    public class  BookCategoryConfiguration : IEntityTypeConfiguration<BookCategory>
    {
        public void Configure(EntityTypeBuilder<BookCategory> builder)
        {

            builder.HasKey(a => a. BookCategoryId);

//			OneToMany Relation With	: Layout
//			BookCategory ve Layout tablolari arasinda bireCok iliski 
  			builder.HasOne(a => a.Layout ) // BookCategory nesnesi sadece bir Layout nesnesine baglidir.
  			.WithMany( a => a.BookCategories ).HasForeignKey( a => a.LayoutId ); // Layout nesnesinin bircok BookCategory nesnesi vardir ve bu BookCategory nesnesini, BookCategory nesnesindeki 'LayoutId' ile iliskilendirir.
        }
    }
}
