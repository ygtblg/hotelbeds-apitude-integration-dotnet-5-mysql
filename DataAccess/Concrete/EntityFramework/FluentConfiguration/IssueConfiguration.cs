/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:IssueConfiguration.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\FluentConfiguration\IssueConfiguration.cs
*/

using Microsoft.EntityFrameworkCore;
using Entities.Concrete;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
namespace DataAccess.Concrete.EntityFramework.FluentConfiguration
{
    public class  IssueConfiguration : IEntityTypeConfiguration<Issue>
    {
        public void Configure(EntityTypeBuilder<Issue> builder)
        {
            builder.Property(a => a.IssueCode)
            .HasColumnName("IssueCode");
            builder.HasKey(a => a.IssueCode);

            builder.Property(c => c.DateFrom)
            .HasColumnType(@"time").HasConversion(v => v.TimeOfDay, v => DateTime.Now.Date.Add(v) );

            builder.Property(c => c.DateTo)
            .HasColumnType("date");


//			OneToMany Relation With	: Hotel
//			Issue ve Hotel tablolari arasinda bireCok iliski 
  			builder.HasOne(a => a.Hotel ) // Issue nesnesi sadece bir Hotel nesnesine baglidir.
  			.WithMany( a => a.Issues ).HasForeignKey( a => a.HotelCode ); // Hotel nesnesinin bircok Issue nesnesi vardir ve bu Issue nesnesini, Issue nesnesindeki 'HotelId' ile iliskilendirir.

//			OneToOne Relation With	: IssueData
//			Issue ve IssueData tablolari arasinda bireBir iliski 
  			builder.HasOne( a => a.IssueData ) // bir 'IssueData' nesnesi vardir.
  			.WithOne( a => a.Issue ).HasForeignKey<Issue>("IssueDataCode");
        }
    }
}
