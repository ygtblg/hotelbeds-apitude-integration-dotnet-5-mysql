/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 15/08/2021 11:07:46
   ThisFileName:EfPublisherDal.cs
   ThisFilePath:C:\projects\BackendPartner\dotnet\agile-ionic-generator-001-api\DataAccess\Concrete\EntityFramework\EfPublisherDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfPublisherDal : EfEntityRepositoryBase<Publisher, ApplicationDbContext>, IPublisherDal
    {
        
        public async Task<IList<Publisher>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<Publisher> query = db.Set<Publisher>().AsQueryable<Publisher>();
                query = query
                    .Select(
                        a => new Publisher
                        {

                                // primitive fields
                                PublisherId = a.PublisherId,
                                Uuid = a.Uuid,
                                Name = a.Name,
                                // object fields
                                BookPublishers = (ICollection<BookPublisher>)a.BookPublishers.Select(b => new BookPublisher
                                    {
                                        Book = b.Book,
                                    }),
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



