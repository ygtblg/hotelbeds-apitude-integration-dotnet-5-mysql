/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:EfFacilityGroupDal.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\App\EfFacilityGroupDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfFacilityGroupDal : EfEntityRepositoryBase<FacilityGroup, ApplicationDbContext>, IFacilityGroupDal
    {
        
        public async Task<IList<FacilityGroup>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<FacilityGroup> query = db.Set<FacilityGroup>().AsQueryable<FacilityGroup>();
                query = query
                    .Select(
                        a => new FacilityGroup
                        {

                                // primitive fields
                                FacilityGroupId = a.FacilityGroupId,
                                Code = a.Code,
                                // object fields
                                Description = a.Description,
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



