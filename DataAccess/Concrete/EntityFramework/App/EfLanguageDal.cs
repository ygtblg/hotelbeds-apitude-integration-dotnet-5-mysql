/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:EfLanguageDal.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\App\EfLanguageDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfLanguageDal : EfEntityRepositoryBase<Language, ApplicationDbContext>, ILanguageDal
    {
        
        public async Task<IList<Language>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<Language> query = db.Set<Language>().AsQueryable<Language>();
                query = query
                    .Select(
                        a => new Language
                        {

                                // primitive fields
                                LanguageId = a.LanguageId,
                                Code = a.Code,
                                Name = a.Name,
                                // object fields
                                Description = a.Description,
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



