/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:EfRoomFacilityDal.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\App\EfRoomFacilityDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfRoomFacilityDal : EfEntityRepositoryBase<RoomFacility, ApplicationDbContext>, IRoomFacilityDal
    {
        
        public async Task<IList<RoomFacility>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<RoomFacility> query = db.Set<RoomFacility>().AsQueryable<RoomFacility>();
                query = query
                    .Select(
                        a => new RoomFacility
                        {

                                // primitive fields
                                RoomFacilityId = a.RoomFacilityId,
                                FacilityCode = a.FacilityCode,
                                FacilityGroupCode = a.FacilityGroupCode,
                                IndLogic = a.IndLogic,
                                Number = a.Number,
                                Voucher = a.Voucher,
                                // object fields
                                FacilityData = a.FacilityData,
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



