/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:EfDescriptionDal.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\App\EfDescriptionDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfDescriptionDal : EfEntityRepositoryBase<Description, ApplicationDbContext>, IDescriptionDal
    {
        
        public async Task<IList<Description>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<Description> query = db.Set<Description>().AsQueryable<Description>();
                query = query
                    .Select(
                        a => new Description
                        {

                                // primitive fields
                                DescriptionId = a.DescriptionId,
                                LanguageCode = a.LanguageCode,
                                Content = a.Content,
                                // object fields
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



