/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:EfBookingSiteDal.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\App\EfBookingSiteDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfBookingSiteDal : EfEntityRepositoryBase<BookingSite, ApplicationDbContext>, IBookingSiteDal
    {
        
        public async Task<IList<BookingSite>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<BookingSite> query = db.Set<BookingSite>().AsQueryable<BookingSite>();
                query = query
                    .Select(
                        a => new BookingSite
                        {

                                // primitive fields
                                BookingSiteId = a.BookingSiteId,
                                LoggedIn = a.LoggedIn,
                                Error = a.Error,
                                Success = a.Success,
                                IsRunningMobile = a.IsRunningMobile,
                                WindowWidth = a.WindowWidth,
                                WindowHeight = a.WindowHeight,
                                // object fields
                                Hotels = (ICollection<Hotel>)a.Hotels.Select(b => new Hotel
                                    {
                                    // primitive fields
                                    HotelId = b.HotelId,
                                    Code = b.Code,
                                    ZoneCode = b.ZoneCode,
                                    // object fields
                                    HotelBoard = (ICollection<HotelBoard>)b.HotelBoard.Select(c => new HotelBoard
                                        {
                                        // primitive fields
                                        HotelBoardId = c.HotelBoardId,
                                        HotelCode = c.HotelCode,
                                        // object fields
                                        }),
                                    HotelSegment = (ICollection<HotelSegment>)b.HotelSegment.Select(c => new HotelSegment
                                        {
                                        // primitive fields
                                        HotelSegmentId = c.HotelSegmentId,
                                        HotelCode = c.HotelCode,
                                        SegmentCode = c.SegmentCode,
                                        // object fields
                                        }),
                                    Name = b.Name,
                                    Description = b.Description,
                                    Country = b.Country,
                                    State = b.State,
                                    Destination = b.Destination,
                                    Zone = b.Zone,
                                    Coordinates = b.Coordinates,
                                    Category = b.Category,
                                    CategoryGroup = b.CategoryGroup,
                                    Chain = b.Chain,
                                    Accommodation = b.Accommodation,
                                    Address = b.Address,
                                    City = b.City,
                                    Phones = (ICollection<Phone>)b.Phones.Select(c => new Phone
                                        {
                                        // primitive fields
                                        PhoneId = c.PhoneId,
                                        // object fields
                                        }),
                                    Rooms = (ICollection<Room>)b.Rooms.Select(c => new Room
                                        {
                                        // primitive fields
                                        RoomId = c.RoomId,
                                        MinPax = c.MinPax,
                                        MaxPax = c.MaxPax,
                                        MaxAdults = c.MaxAdults,
                                        MaxChildren = c.MaxChildren,
                                        MinAdults = c.MinAdults,
                                        // object fields
                                        RoomFacilities = (ICollection<RoomFacility>)c.RoomFacilities.Select(d => new RoomFacility
                                            {
                                            // primitive fields
                                            RoomFacilityId = d.RoomFacilityId,
                                            FacilityCode = d.FacilityCode,
                                            FacilityGroupCode = d.FacilityGroupCode,
                                            IndLogic = d.IndLogic,
                                            Number = d.Number,
                                            Voucher = d.Voucher,
                                            // object fields
                                            FacilityData = d.FacilityData,
                                            }),
                                        RoomStays = (ICollection<RoomStay>)c.RoomStays.Select(d => new RoomStay
                                            {
                                            // primitive fields
                                            RoomStayId = d.RoomStayId,
                                            // object fields
                                            RoomStayFacilities = (ICollection<RoomStayFacility>)d.RoomStayFacilities.Select(e => new RoomStayFacility
                                                {
                                                // primitive fields
                                                RoomStayFacilityId = e.RoomStayFacilityId,
                                                FacilityCode = e.FacilityCode,
                                                FacilityGroupCode = e.FacilityGroupCode,
                                                Number = e.Number,
                                                // object fields
                                                FacilityData = e.FacilityData,
                                                }),
                                            }),
                                        RoomData = c.RoomData,
                                        }),
                                    Facilities = (ICollection<Facility>)b.Facilities.Select(c => new Facility
                                        {
                                        // primitive fields
                                        FacilityId = c.FacilityId,
                                        FacilityCode = c.FacilityCode,
                                        FacilityGroupCode = c.FacilityGroupCode,
                                        Order = c.Order,
                                        IndYesOrNo = c.IndYesOrNo,
                                        Number = c.Number,
                                        Voucher = c.Voucher,
                                        IndLogic = c.IndLogic,
                                        IndFee = c.IndFee,
                                        Distance = c.Distance,
                                        // object fields
                                        FacilityData = c.FacilityData,
                                        }),
                                    Terminals = (ICollection<Terminal>)b.Terminals.Select(c => new Terminal
                                        {
                                        // primitive fields
                                        TerminalId = c.TerminalId,
                                        Distance = c.Distance,
                                        // object fields
                                        TerminalData = c.TerminalData,
                                        }),
                                    Issues = (ICollection<Issue>)b.Issues.Select(c => new Issue
                                        {
                                        // primitive fields
                                        IssueId = c.IssueId,
                                        Order = c.Order,
                                        Alternative = c.Alternative,
                                        // object fields
                                        IssueData = c.IssueData,
                                        }),
                                    InterestPoints = (ICollection<InterestPoint>)b.InterestPoints.Select(c => new InterestPoint
                                        {
                                        // primitive fields
                                        InterestPointId = c.InterestPointId,
                                        FacilityCode = c.FacilityCode,
                                        FacilityGroupCode = c.FacilityGroupCode,
                                        Order = c.Order,
                                        // object fields
                                        FacilityData = c.FacilityData,
                                        }),
                                    Images = (ICollection<Image>)b.Images.Select(c => new Image
                                        {
                                        // primitive fields
                                        ImageId = c.ImageId,
                                        Order = c.Order,
                                        VisualOrder = c.VisualOrder,
                                        // object fields
                                        ImageType = c.ImageType,
                                        }),
                                    Wildcards = (ICollection<Wildcard>)b.Wildcards.Select(c => new Wildcard
                                        {
                                        // primitive fields
                                        WildcardId = c.WildcardId,
                                        // object fields
                                        HotelRoomDescription = c.HotelRoomDescription,
                                        }),
                                    }),
                                Languages = (ICollection<Language>)a.Languages.Select(b => new Language
                                    {
                                    // primitive fields
                                    LanguageId = b.LanguageId,
                                    // object fields
                                    Description = b.Description,
                                    }),
                                Boards = (ICollection<Board>)a.Boards.Select(b => new Board
                                    {
                                    // primitive fields
                                    BoardId = b.BoardId,
                                    // object fields
                                    Description = b.Description,
                                    HotelBoard = (ICollection<HotelBoard>)b.HotelBoard.Select(c => new HotelBoard
                                        {
                                        // primitive fields
                                        HotelBoardId = c.HotelBoardId,
                                        HotelCode = c.HotelCode,
                                        // object fields
                                        }),
                                    }),
                                Segments = (ICollection<Segment>)a.Segments.Select(b => new Segment
                                    {
                                    // primitive fields
                                    SegmentId = b.SegmentId,
                                    Code = b.Code,
                                    // object fields
                                    Description = b.Description,
                                    HotelSegment = (ICollection<HotelSegment>)b.HotelSegment.Select(c => new HotelSegment
                                        {
                                        // primitive fields
                                        HotelSegmentId = c.HotelSegmentId,
                                        HotelCode = c.HotelCode,
                                        SegmentCode = c.SegmentCode,
                                        // object fields
                                        }),
                                    }),
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



