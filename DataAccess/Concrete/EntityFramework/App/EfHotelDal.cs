/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:EfHotelDal.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\App\EfHotelDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfHotelDal : EfEntityRepositoryBase<Hotel, ApplicationDbContext>, IHotelDal
    {
        
        public async Task<IList<Hotel>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<Hotel> query = db.Set<Hotel>().AsQueryable<Hotel>();
                query = query
                    .Select(
                        a => new Hotel
                        {

                                // primitive fields
                                HotelId = a.HotelId,
                                Code = a.Code,
                                CountryCode = a.CountryCode,
                                StateCode = a.StateCode,
                                DestinationCode = a.DestinationCode,
                                ZoneCode = a.ZoneCode,
                                CategoryCode = a.CategoryCode,
                                CategoryGroupCode = a.CategoryGroupCode,
                                ChainCode = a.ChainCode,
                                AccommodationTypeCode = a.AccommodationTypeCode,
                                PostalCode = a.PostalCode,
                                Email = a.Email,
                                License = a.License,
                                // object fields
                                HotelBoard = (ICollection<HotelBoard>)a.HotelBoard.Select(b => new HotelBoard
                                    {
                                    // primitive fields
                                    HotelBoardId = b.HotelBoardId,
                                    HotelCode = b.HotelCode,
                                    // object fields
                                    }),
                                HotelSegment = (ICollection<HotelSegment>)a.HotelSegment.Select(b => new HotelSegment
                                    {
                                    // primitive fields
                                    HotelSegmentId = b.HotelSegmentId,
                                    HotelCode = b.HotelCode,
                                    SegmentCode = b.SegmentCode,
                                    // object fields
                                    }),
                                Name = a.Name,
                                Description = a.Description,
                                Country = a.Country,
                                State = a.State,
                                Destination = a.Destination,
                                Zone = a.Zone,
                                Coordinates = a.Coordinates,
                                Category = a.Category,
                                CategoryGroup = a.CategoryGroup,
                                Chain = a.Chain,
                                Accommodation = a.Accommodation,
                                Address = a.Address,
                                City = a.City,
                                Phones = (ICollection<Phone>)a.Phones.Select(b => new Phone
                                    {
                                    // primitive fields
                                    PhoneId = b.PhoneId,
                                    // object fields
                                    }),
                                Rooms = (ICollection<Room>)a.Rooms.Select(b => new Room
                                    {
                                    // primitive fields
                                    RoomId = b.RoomId,
                                    MinPax = b.MinPax,
                                    MaxPax = b.MaxPax,
                                    MaxAdults = b.MaxAdults,
                                    MaxChildren = b.MaxChildren,
                                    MinAdults = b.MinAdults,
                                    // object fields
                                    RoomFacilities = (ICollection<RoomFacility>)b.RoomFacilities.Select(c => new RoomFacility
                                        {
                                        // primitive fields
                                        RoomFacilityId = c.RoomFacilityId,
                                        FacilityCode = c.FacilityCode,
                                        FacilityGroupCode = c.FacilityGroupCode,
                                        IndLogic = c.IndLogic,
                                        Number = c.Number,
                                        Voucher = c.Voucher,
                                        // object fields
                                        FacilityData = c.FacilityData,
                                        }),
                                    RoomStays = (ICollection<RoomStay>)b.RoomStays.Select(c => new RoomStay
                                        {
                                        // primitive fields
                                        RoomStayId = c.RoomStayId,
                                        // object fields
                                        RoomStayFacilities = (ICollection<RoomStayFacility>)c.RoomStayFacilities.Select(d => new RoomStayFacility
                                            {
                                            // primitive fields
                                            RoomStayFacilityId = d.RoomStayFacilityId,
                                            FacilityCode = d.FacilityCode,
                                            FacilityGroupCode = d.FacilityGroupCode,
                                            Number = d.Number,
                                            // object fields
                                            FacilityData = d.FacilityData,
                                            }),
                                        }),
                                    RoomData = b.RoomData,
                                    }),
                                Facilities = (ICollection<Facility>)a.Facilities.Select(b => new Facility
                                    {
                                    // primitive fields
                                    FacilityId = b.FacilityId,
                                    FacilityCode = b.FacilityCode,
                                    FacilityGroupCode = b.FacilityGroupCode,
                                    Order = b.Order,
                                    IndYesOrNo = b.IndYesOrNo,
                                    Number = b.Number,
                                    Voucher = b.Voucher,
                                    IndLogic = b.IndLogic,
                                    IndFee = b.IndFee,
                                    Distance = b.Distance,
                                    // object fields
                                    FacilityData = b.FacilityData,
                                    }),
                                Terminals = (ICollection<Terminal>)a.Terminals.Select(b => new Terminal
                                    {
                                    // primitive fields
                                    TerminalId = b.TerminalId,
                                    Distance = b.Distance,
                                    // object fields
                                    TerminalData = b.TerminalData,
                                    }),
                                Issues = (ICollection<Issue>)a.Issues.Select(b => new Issue
                                    {
                                    // primitive fields
                                    IssueId = b.IssueId,
                                    Order = b.Order,
                                    Alternative = b.Alternative,
                                    // object fields
                                    IssueData = b.IssueData,
                                    }),
                                InterestPoints = (ICollection<InterestPoint>)a.InterestPoints.Select(b => new InterestPoint
                                    {
                                    // primitive fields
                                    InterestPointId = b.InterestPointId,
                                    FacilityCode = b.FacilityCode,
                                    FacilityGroupCode = b.FacilityGroupCode,
                                    Order = b.Order,
                                    // object fields
                                    FacilityData = b.FacilityData,
                                    }),
                                Images = (ICollection<Image>)a.Images.Select(b => new Image
                                    {
                                    // primitive fields
                                    ImageId = b.ImageId,
                                    Order = b.Order,
                                    VisualOrder = b.VisualOrder,
                                    // object fields
                                    ImageType = b.ImageType,
                                    }),
                                Wildcards = (ICollection<Wildcard>)a.Wildcards.Select(b => new Wildcard
                                    {
                                    // primitive fields
                                    WildcardId = b.WildcardId,
                                    // object fields
                                    HotelRoomDescription = b.HotelRoomDescription,
                                    }),
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



