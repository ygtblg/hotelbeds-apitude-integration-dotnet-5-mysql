/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:EfRoomDataDal.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\App\EfRoomDataDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfRoomDataDal : EfEntityRepositoryBase<RoomData, ApplicationDbContext>, IRoomDataDal
    {
        
        public async Task<IList<RoomData>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<RoomData> query = db.Set<RoomData>().AsQueryable<RoomData>();
                query = query
                    .Select(
                        a => new RoomData
                        {

                                // primitive fields
                                RoomDataId = a.RoomDataId,
                                Code = a.Code,
                                Type = a.Type,
                                Characteristic = a.Characteristic,
                                MinPax = a.MinPax,
                                MaxPax = a.MaxPax,
                                MaxAdults = a.MaxAdults,
                                MaxChildren = a.MaxChildren,
                                MinAdults = a.MinAdults,
                                Description = a.Description,
                                // object fields
                                TypeDescription = a.TypeDescription,
                                CharacteristicDescription = a.CharacteristicDescription,
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



