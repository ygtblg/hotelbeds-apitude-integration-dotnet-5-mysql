/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:EfHotelSegmentDal.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\App\EfHotelSegmentDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfHotelSegmentDal : EfEntityRepositoryBase<HotelSegment, ApplicationDbContext>, IHotelSegmentDal
    {
        
        public async Task<IList<HotelSegment>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<HotelSegment> query = db.Set<HotelSegment>().AsQueryable<HotelSegment>();
                query = query
                    .Select(
                        a => new HotelSegment
                        {

                                // primitive fields
                                HotelSegmentId = a.HotelSegmentId,
                                HotelCode = a.HotelCode,
                                SegmentCode = a.SegmentCode,
                                // object fields
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



