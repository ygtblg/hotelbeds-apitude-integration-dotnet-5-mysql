/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:EfImageTypeDal.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\App\EfImageTypeDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfImageTypeDal : EfEntityRepositoryBase<ImageType, ApplicationDbContext>, IImageTypeDal
    {
        
        public async Task<IList<ImageType>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<ImageType> query = db.Set<ImageType>().AsQueryable<ImageType>();
                query = query
                    .Select(
                        a => new ImageType
                        {

                                // primitive fields
                                ImageTypeId = a.ImageTypeId,
                                Code = a.Code,
                                // object fields
                                Description = a.Description,
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



