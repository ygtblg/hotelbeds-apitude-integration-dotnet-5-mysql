/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:EfSegmentDal.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\App\EfSegmentDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfSegmentDal : EfEntityRepositoryBase<Segment, ApplicationDbContext>, ISegmentDal
    {
        
        public async Task<IList<Segment>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<Segment> query = db.Set<Segment>().AsQueryable<Segment>();
                query = query
                    .Select(
                        a => new Segment
                        {

                                // primitive fields
                                SegmentId = a.SegmentId,
                                Code = a.Code,
                                // object fields
                                Description = a.Description,
                                HotelSegment = (ICollection<HotelSegment>)a.HotelSegment.Select(b => new HotelSegment
                                    {
                                    // primitive fields
                                    HotelSegmentId = b.HotelSegmentId,
                                    HotelCode = b.HotelCode,
                                    SegmentCode = b.SegmentCode,
                                    // object fields
                                    }),
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



