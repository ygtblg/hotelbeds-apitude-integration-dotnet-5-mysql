/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:EfDestinationDal.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\App\EfDestinationDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfDestinationDal : EfEntityRepositoryBase<Destination, ApplicationDbContext>, IDestinationDal
    {
        
        public async Task<IList<Destination>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<Destination> query = db.Set<Destination>().AsQueryable<Destination>();
                query = query
                    .Select(
                        a => new Destination
                        {

                                // primitive fields
                                DestinationId = a.DestinationId,
                                Code = a.Code,
                                CountryCode = a.CountryCode,
                                IsoCode = a.IsoCode,
                                // object fields
                                Name = a.Name,
                                Zones = (ICollection<Zone>)a.Zones.Select(b => new Zone
                                    {
                                    // primitive fields
                                    ZoneId = b.ZoneId,
                                    ZoneCode = b.ZoneCode,
                                    // object fields
                                    Description = b.Description,
                                    }),
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



