/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:EfCategoryGroupDal.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\App\EfCategoryGroupDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfCategoryGroupDal : EfEntityRepositoryBase<CategoryGroup, ApplicationDbContext>, ICategoryGroupDal
    {
        
        public async Task<IList<CategoryGroup>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<CategoryGroup> query = db.Set<CategoryGroup>().AsQueryable<CategoryGroup>();
                query = query
                    .Select(
                        a => new CategoryGroup
                        {

                                // primitive fields
                                CategoryGroupId = a.CategoryGroupId,
                                Code = a.Code,
                                Order = a.Order,
                                // object fields
                                Name = a.Name,
                                Description = a.Description,
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



