/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:EfIssueDataDal.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\App\EfIssueDataDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfIssueDataDal : EfEntityRepositoryBase<IssueData, ApplicationDbContext>, IIssueDataDal
    {
        
        public async Task<IList<IssueData>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<IssueData> query = db.Set<IssueData>().AsQueryable<IssueData>();
                query = query
                    .Select(
                        a => new IssueData
                        {

                                // primitive fields
                                IssueDataId = a.IssueDataId,
                                Code = a.Code,
                                Type = a.Type,
                                Alternative = a.Alternative,
                                // object fields
                                Description = a.Description,
                                Name = a.Name,
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



