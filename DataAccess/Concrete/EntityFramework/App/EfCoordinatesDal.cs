/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:EfCoordinatesDal.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\App\EfCoordinatesDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfCoordinatesDal : EfEntityRepositoryBase<Coordinates, ApplicationDbContext>, ICoordinatesDal
    {
        
        public async Task<IList<Coordinates>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<Coordinates> query = db.Set<Coordinates>().AsQueryable<Coordinates>();
                query = query
                    .Select(
                        a => new Coordinates
                        {

                                // primitive fields
                                CoordinatesId = a.CoordinatesId,
                                Longitude = a.Longitude,
                                Latitude = a.Latitude,
                                // object fields
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



