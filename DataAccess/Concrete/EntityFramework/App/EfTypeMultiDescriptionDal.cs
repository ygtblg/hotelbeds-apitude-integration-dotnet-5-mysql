/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:EfTypeMultiDescriptionDal.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\App\EfTypeMultiDescriptionDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfTypeMultiDescriptionDal : EfEntityRepositoryBase<TypeMultiDescription, ApplicationDbContext>, ITypeMultiDescriptionDal
    {
        
        public async Task<IList<TypeMultiDescription>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<TypeMultiDescription> query = db.Set<TypeMultiDescription>().AsQueryable<TypeMultiDescription>();
                query = query
                    .Select(
                        a => new TypeMultiDescription
                        {

                                // primitive fields
                                TypeMultiDescriptionId = a.TypeMultiDescriptionId,
                                LanguageCode = a.LanguageCode,
                                Content = a.Content,
                                // object fields
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



