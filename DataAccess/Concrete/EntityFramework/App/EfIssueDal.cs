/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:EfIssueDal.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\App\EfIssueDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfIssueDal : EfEntityRepositoryBase<Issue, ApplicationDbContext>, IIssueDal
    {
        
        public async Task<IList<Issue>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<Issue> query = db.Set<Issue>().AsQueryable<Issue>();
                query = query
                    .Select(
                        a => new Issue
                        {

                                // primitive fields
                                IssueId = a.IssueId,
                                IssueCode = a.IssueCode,
                                IssueType = a.IssueType,
                                DateFrom = a.DateFrom,
                                DateTo = a.DateTo,
                                Order = a.Order,
                                Alternative = a.Alternative,
                                // object fields
                                IssueData = a.IssueData,
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



