/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:EfCharacteristicDescriptionDal.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\App\EfCharacteristicDescriptionDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfCharacteristicDescriptionDal : EfEntityRepositoryBase<CharacteristicDescription, ApplicationDbContext>, ICharacteristicDescriptionDal
    {
        
        public async Task<IList<CharacteristicDescription>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<CharacteristicDescription> query = db.Set<CharacteristicDescription>().AsQueryable<CharacteristicDescription>();
                query = query
                    .Select(
                        a => new CharacteristicDescription
                        {

                                // primitive fields
                                CharacteristicDescriptionId = a.CharacteristicDescriptionId,
                                CharacteristicDescriptionField = a.CharacteristicDescriptionField,
                                LanguageCode = a.LanguageCode,
                                Content = a.Content,
                                // object fields
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



