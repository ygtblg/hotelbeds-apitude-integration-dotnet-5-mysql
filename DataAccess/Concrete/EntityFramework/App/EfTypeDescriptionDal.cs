/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:EfTypeDescriptionDal.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\App\EfTypeDescriptionDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfTypeDescriptionDal : EfEntityRepositoryBase<TypeDescription, ApplicationDbContext>, ITypeDescriptionDal
    {
        
        public async Task<IList<TypeDescription>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<TypeDescription> query = db.Set<TypeDescription>().AsQueryable<TypeDescription>();
                query = query
                    .Select(
                        a => new TypeDescription
                        {

                                // primitive fields
                                TypeDescriptionId = a.TypeDescriptionId,
                                TypeDescriptionField = a.TypeDescriptionField,
                                LanguageCode = a.LanguageCode,
                                Content = a.Content,
                                // object fields
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



