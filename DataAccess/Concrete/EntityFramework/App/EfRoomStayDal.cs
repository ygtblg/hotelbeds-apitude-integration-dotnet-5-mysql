/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:EfRoomStayDal.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\App\EfRoomStayDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfRoomStayDal : EfEntityRepositoryBase<RoomStay, ApplicationDbContext>, IRoomStayDal
    {
        
        public async Task<IList<RoomStay>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<RoomStay> query = db.Set<RoomStay>().AsQueryable<RoomStay>();
                query = query
                    .Select(
                        a => new RoomStay
                        {

                                // primitive fields
                                RoomStayId = a.RoomStayId,
                                StayType = a.StayType,
                                Order = a.Order,
                                Description = a.Description,
                                // object fields
                                RoomStayFacilities = (ICollection<RoomStayFacility>)a.RoomStayFacilities.Select(b => new RoomStayFacility
                                    {
                                    // primitive fields
                                    RoomStayFacilityId = b.RoomStayFacilityId,
                                    FacilityCode = b.FacilityCode,
                                    FacilityGroupCode = b.FacilityGroupCode,
                                    Number = b.Number,
                                    // object fields
                                    FacilityData = b.FacilityData,
                                    }),
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



