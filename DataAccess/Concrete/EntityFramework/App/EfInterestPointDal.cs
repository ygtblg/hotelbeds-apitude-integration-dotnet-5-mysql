/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:EfInterestPointDal.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\App\EfInterestPointDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfInterestPointDal : EfEntityRepositoryBase<InterestPoint, ApplicationDbContext>, IInterestPointDal
    {
        
        public async Task<IList<InterestPoint>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<InterestPoint> query = db.Set<InterestPoint>().AsQueryable<InterestPoint>();
                query = query
                    .Select(
                        a => new InterestPoint
                        {

                                // primitive fields
                                InterestPointId = a.InterestPointId,
                                FacilityCode = a.FacilityCode,
                                FacilityGroupCode = a.FacilityGroupCode,
                                Order = a.Order,
                                PoiName = a.PoiName,
                                Distance = a.Distance,
                                // object fields
                                FacilityData = a.FacilityData,
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



