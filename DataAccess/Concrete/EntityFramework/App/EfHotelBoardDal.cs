/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:EfHotelBoardDal.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\App\EfHotelBoardDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfHotelBoardDal : EfEntityRepositoryBase<HotelBoard, ApplicationDbContext>, IHotelBoardDal
    {
        
        public async Task<IList<HotelBoard>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<HotelBoard> query = db.Set<HotelBoard>().AsQueryable<HotelBoard>();
                query = query
                    .Select(
                        a => new HotelBoard
                        {

                                // primitive fields
                                HotelBoardId = a.HotelBoardId,
                                HotelCode = a.HotelCode,
                                BoardCode = a.BoardCode,
                                // object fields
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



