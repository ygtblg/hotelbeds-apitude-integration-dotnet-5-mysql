/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:EfHotelRoomDescriptionDal.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\App\EfHotelRoomDescriptionDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfHotelRoomDescriptionDal : EfEntityRepositoryBase<HotelRoomDescription, ApplicationDbContext>, IHotelRoomDescriptionDal
    {
        
        public async Task<IList<HotelRoomDescription>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<HotelRoomDescription> query = db.Set<HotelRoomDescription>().AsQueryable<HotelRoomDescription>();
                query = query
                    .Select(
                        a => new HotelRoomDescription
                        {

                                // primitive fields
                                HotelRoomDescriptionId = a.HotelRoomDescriptionId,
                                HotelRoomDescriptionField = a.HotelRoomDescriptionField,
                                Content = a.Content,
                                // object fields
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



