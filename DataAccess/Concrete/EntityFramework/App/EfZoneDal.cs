/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:EfZoneDal.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\App\EfZoneDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfZoneDal : EfEntityRepositoryBase<Zone, ApplicationDbContext>, IZoneDal
    {
        
        public async Task<IList<Zone>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<Zone> query = db.Set<Zone>().AsQueryable<Zone>();
                query = query
                    .Select(
                        a => new Zone
                        {

                                // primitive fields
                                ZoneId = a.ZoneId,
                                ZoneCode = a.ZoneCode,
                                Name = a.Name,
                                // object fields
                                Description = a.Description,
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



