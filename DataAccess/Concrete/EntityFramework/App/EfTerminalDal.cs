/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:EfTerminalDal.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\App\EfTerminalDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfTerminalDal : EfEntityRepositoryBase<Terminal, ApplicationDbContext>, ITerminalDal
    {
        
        public async Task<IList<Terminal>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<Terminal> query = db.Set<Terminal>().AsQueryable<Terminal>();
                query = query
                    .Select(
                        a => new Terminal
                        {

                                // primitive fields
                                TerminalId = a.TerminalId,
                                TerminalCode = a.TerminalCode,
                                Distance = a.Distance,
                                // object fields
                                TerminalData = a.TerminalData,
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



