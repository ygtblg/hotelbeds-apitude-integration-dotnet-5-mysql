/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:EfCategoryDal.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\App\EfCategoryDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfCategoryDal : EfEntityRepositoryBase<Category, ApplicationDbContext>, ICategoryDal
    {
        
        public async Task<IList<Category>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<Category> query = db.Set<Category>().AsQueryable<Category>();
                query = query
                    .Select(
                        a => new Category
                        {

                                // primitive fields
                                CategoryId = a.CategoryId,
                                Code = a.Code,
                                SimpleCode = a.SimpleCode,
                                AccommodationType = a.AccommodationType,
                                Group = a.Group,
                                // object fields
                                Description = a.Description,
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



