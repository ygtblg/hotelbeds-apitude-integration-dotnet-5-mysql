/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:EfCountryDal.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\App\EfCountryDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfCountryDal : EfEntityRepositoryBase<Country, ApplicationDbContext>, ICountryDal
    {
        
        public async Task<IList<Country>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<Country> query = db.Set<Country>().AsQueryable<Country>();
                query = query
                    .Select(
                        a => new Country
                        {

                                // primitive fields
                                CountryId = a.CountryId,
                                Code = a.Code,
                                IsoCode = a.IsoCode,
                                // object fields
                                Description = a.Description,
                                States = (ICollection<State>)a.States.Select(b => new State
                                    {
                                    // primitive fields
                                    StateId = b.StateId,
                                    Code = b.Code,
                                    // object fields
                                    }),
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



