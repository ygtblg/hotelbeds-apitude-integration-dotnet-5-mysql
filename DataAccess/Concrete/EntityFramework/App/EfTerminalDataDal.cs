/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:EfTerminalDataDal.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\App\EfTerminalDataDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfTerminalDataDal : EfEntityRepositoryBase<TerminalData, ApplicationDbContext>, ITerminalDataDal
    {
        
        public async Task<IList<TerminalData>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<TerminalData> query = db.Set<TerminalData>().AsQueryable<TerminalData>();
                query = query
                    .Select(
                        a => new TerminalData
                        {

                                // primitive fields
                                TerminalDataId = a.TerminalDataId,
                                Code = a.Code,
                                Type = a.Type,
                                Country = a.Country,
                                // object fields
                                Name = a.Name,
                                Description = a.Description,
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



