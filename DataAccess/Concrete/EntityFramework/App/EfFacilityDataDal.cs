/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:EfFacilityDataDal.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\App\EfFacilityDataDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfFacilityDataDal : EfEntityRepositoryBase<FacilityData, ApplicationDbContext>, IFacilityDataDal
    {
        
        public async Task<IList<FacilityData>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<FacilityData> query = db.Set<FacilityData>().AsQueryable<FacilityData>();
                query = query
                    .Select(
                        a => new FacilityData
                        {

                                // primitive fields
                                FacilityDataId = a.FacilityDataId,
                                Code = a.Code,
                                FacilityGroupCode = a.FacilityGroupCode,
                                FacilityTypologyCode = a.FacilityTypologyCode,
                                // object fields
                                Description = a.Description,
                                FacilityTypology = a.FacilityTypology,
                                FacilityGroup = a.FacilityGroup,
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



