/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:EfBoardDal.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\App\EfBoardDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfBoardDal : EfEntityRepositoryBase<Board, ApplicationDbContext>, IBoardDal
    {
        
        public async Task<IList<Board>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<Board> query = db.Set<Board>().AsQueryable<Board>();
                query = query
                    .Select(
                        a => new Board
                        {

                                // primitive fields
                                BoardId = a.BoardId,
                                Code = a.Code,
                                MultiLingualCode = a.MultiLingualCode,
                                // object fields
                                Description = a.Description,
                                HotelBoard = (ICollection<HotelBoard>)a.HotelBoard.Select(b => new HotelBoard
                                    {
                                    // primitive fields
                                    HotelBoardId = b.HotelBoardId,
                                    HotelCode = b.HotelCode,
                                    // object fields
                                    }),
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



