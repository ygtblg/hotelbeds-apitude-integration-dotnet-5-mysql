/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:EfFacilityTypologyDal.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\App\EfFacilityTypologyDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfFacilityTypologyDal : EfEntityRepositoryBase<FacilityTypology, ApplicationDbContext>, IFacilityTypologyDal
    {
        
        public async Task<IList<FacilityTypology>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<FacilityTypology> query = db.Set<FacilityTypology>().AsQueryable<FacilityTypology>();
                query = query
                    .Select(
                        a => new FacilityTypology
                        {

                                // primitive fields
                                FacilityTypologyId = a.FacilityTypologyId,
                                Code = a.Code,
                                NumberFlag = a.NumberFlag,
                                LogicFlag = a.LogicFlag,
                                FeeFlag = a.FeeFlag,
                                DistanceFlag = a.DistanceFlag,
                                AgeFromFlag = a.AgeFromFlag,
                                AgeToFlag = a.AgeToFlag,
                                DateFromFlag = a.DateFromFlag,
                                DateToFlag = a.DateToFlag,
                                TimeFromFlag = a.TimeFromFlag,
                                TimeToFlag = a.TimeToFlag,
                                IndYesOrNoFlag = a.IndYesOrNoFlag,
                                AmountFlag = a.AmountFlag,
                                CurrencyFlag = a.CurrencyFlag,
                                AppTypeFlag = a.AppTypeFlag,
                                TextFlag = a.TextFlag,
                                // object fields
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



