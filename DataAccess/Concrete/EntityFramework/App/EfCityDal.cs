/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:EfCityDal.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\App\EfCityDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfCityDal : EfEntityRepositoryBase<City, ApplicationDbContext>, ICityDal
    {
        
        public async Task<IList<City>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<City> query = db.Set<City>().AsQueryable<City>();
                query = query
                    .Select(
                        a => new City
                        {

                                // primitive fields
                                CityId = a.CityId,
                                LanguageCode = a.LanguageCode,
                                CityField = a.CityField,
                                Content = a.Content,
                                // object fields
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



