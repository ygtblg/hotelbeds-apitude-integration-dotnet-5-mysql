/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:EfPhoneDal.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\App\EfPhoneDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfPhoneDal : EfEntityRepositoryBase<Phone, ApplicationDbContext>, IPhoneDal
    {
        
        public async Task<IList<Phone>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<Phone> query = db.Set<Phone>().AsQueryable<Phone>();
                query = query
                    .Select(
                        a => new Phone
                        {

                                // primitive fields
                                PhoneId = a.PhoneId,
                                PhoneNumber = a.PhoneNumber,
                                PhoneType = a.PhoneType,
                                // object fields
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



