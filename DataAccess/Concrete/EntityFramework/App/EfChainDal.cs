/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:EfChainDal.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\App\EfChainDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfChainDal : EfEntityRepositoryBase<Chain, ApplicationDbContext>, IChainDal
    {
        
        public async Task<IList<Chain>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<Chain> query = db.Set<Chain>().AsQueryable<Chain>();
                query = query
                    .Select(
                        a => new Chain
                        {

                                // primitive fields
                                ChainId = a.ChainId,
                                Code = a.Code,
                                // object fields
                                Description = a.Description,
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



