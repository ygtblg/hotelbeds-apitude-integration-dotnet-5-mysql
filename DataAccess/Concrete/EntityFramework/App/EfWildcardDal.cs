/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:EfWildcardDal.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\App\EfWildcardDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfWildcardDal : EfEntityRepositoryBase<Wildcard, ApplicationDbContext>, IWildcardDal
    {
        
        public async Task<IList<Wildcard>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<Wildcard> query = db.Set<Wildcard>().AsQueryable<Wildcard>();
                query = query
                    .Select(
                        a => new Wildcard
                        {

                                // primitive fields
                                WildcardId = a.WildcardId,
                                RoomType = a.RoomType,
                                RoomCode = a.RoomCode,
                                CharacteristicCode = a.CharacteristicCode,
                                // object fields
                                HotelRoomDescription = a.HotelRoomDescription,
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



