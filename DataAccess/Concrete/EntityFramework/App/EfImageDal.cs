/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:EfImageDal.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\App\EfImageDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfImageDal : EfEntityRepositoryBase<Image, ApplicationDbContext>, IImageDal
    {
        
        public async Task<IList<Image>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<Image> query = db.Set<Image>().AsQueryable<Image>();
                query = query
                    .Select(
                        a => new Image
                        {

                                // primitive fields
                                ImageId = a.ImageId,
                                ImageTypeCode = a.ImageTypeCode,
                                Path = a.Path,
                                Order = a.Order,
                                VisualOrder = a.VisualOrder,
                                // object fields
                                ImageType = a.ImageType,
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



