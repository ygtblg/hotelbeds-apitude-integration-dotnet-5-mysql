/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:EfNameDal.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\App\EfNameDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfNameDal : EfEntityRepositoryBase<Name, ApplicationDbContext>, INameDal
    {
        
        public async Task<IList<Name>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<Name> query = db.Set<Name>().AsQueryable<Name>();
                query = query
                    .Select(
                        a => new Name
                        {

                                // primitive fields
                                NameId = a.NameId,
                                NameField = a.NameField,
                                LanguageCode = a.LanguageCode,
                                Content = a.Content,
                                // object fields
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



