/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:EfRoomStayFacilityDal.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\App\EfRoomStayFacilityDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfRoomStayFacilityDal : EfEntityRepositoryBase<RoomStayFacility, ApplicationDbContext>, IRoomStayFacilityDal
    {
        
        public async Task<IList<RoomStayFacility>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<RoomStayFacility> query = db.Set<RoomStayFacility>().AsQueryable<RoomStayFacility>();
                query = query
                    .Select(
                        a => new RoomStayFacility
                        {

                                // primitive fields
                                RoomStayFacilityId = a.RoomStayFacilityId,
                                FacilityCode = a.FacilityCode,
                                FacilityGroupCode = a.FacilityGroupCode,
                                Number = a.Number,
                                // object fields
                                FacilityData = a.FacilityData,
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



