/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:EfFacilityDal.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\App\EfFacilityDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfFacilityDal : EfEntityRepositoryBase<Facility, ApplicationDbContext>, IFacilityDal
    {
        
        public async Task<IList<Facility>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<Facility> query = db.Set<Facility>().AsQueryable<Facility>();
                query = query
                    .Select(
                        a => new Facility
                        {

                                // primitive fields
                                FacilityId = a.FacilityId,
                                FacilityCode = a.FacilityCode,
                                FacilityGroupCode = a.FacilityGroupCode,
                                Order = a.Order,
                                IndYesOrNo = a.IndYesOrNo,
                                Number = a.Number,
                                Voucher = a.Voucher,
                                IndLogic = a.IndLogic,
                                IndFee = a.IndFee,
                                Distance = a.Distance,
                                TimeFrom = a.TimeFrom,
                                TimeTo = a.TimeTo,
                                DateTo = a.DateTo,
                                // object fields
                                FacilityData = a.FacilityData,
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



