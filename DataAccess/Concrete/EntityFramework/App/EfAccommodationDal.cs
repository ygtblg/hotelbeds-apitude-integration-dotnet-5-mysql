/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:EfAccommodationDal.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\App\EfAccommodationDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfAccommodationDal : EfEntityRepositoryBase<Accommodation, ApplicationDbContext>, IAccommodationDal
    {
        
        public async Task<IList<Accommodation>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<Accommodation> query = db.Set<Accommodation>().AsQueryable<Accommodation>();
                query = query
                    .Select(
                        a => new Accommodation
                        {

                                // primitive fields
                                AccommodationId = a.AccommodationId,
                                Code = a.Code,
                                TypeDescription = a.TypeDescription,
                                // object fields
                                TypeMultiDescription = a.TypeMultiDescription,
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



