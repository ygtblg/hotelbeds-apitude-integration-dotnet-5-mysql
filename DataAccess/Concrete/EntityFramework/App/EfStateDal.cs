/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:EfStateDal.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\App\EfStateDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfStateDal : EfEntityRepositoryBase<State, ApplicationDbContext>, IStateDal
    {
        
        public async Task<IList<State>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<State> query = db.Set<State>().AsQueryable<State>();
                query = query
                    .Select(
                        a => new State
                        {

                                // primitive fields
                                StateId = a.StateId,
                                Code = a.Code,
                                Name = a.Name,
                                // object fields
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



