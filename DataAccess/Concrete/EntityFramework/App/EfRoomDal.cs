/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:EfRoomDal.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\App\EfRoomDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfRoomDal : EfEntityRepositoryBase<Room, ApplicationDbContext>, IRoomDal
    {
        
        public async Task<IList<Room>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<Room> query = db.Set<Room>().AsQueryable<Room>();
                query = query
                    .Select(
                        a => new Room
                        {

                                // primitive fields
                                RoomId = a.RoomId,
                                RoomCode = a.RoomCode,
                                MinPax = a.MinPax,
                                MaxPax = a.MaxPax,
                                MaxAdults = a.MaxAdults,
                                MaxChildren = a.MaxChildren,
                                MinAdults = a.MinAdults,
                                RoomType = a.RoomType,
                                CharacteristicCode = a.CharacteristicCode,
                                // object fields
                                RoomFacilities = (ICollection<RoomFacility>)a.RoomFacilities.Select(b => new RoomFacility
                                    {
                                    // primitive fields
                                    RoomFacilityId = b.RoomFacilityId,
                                    FacilityCode = b.FacilityCode,
                                    FacilityGroupCode = b.FacilityGroupCode,
                                    IndLogic = b.IndLogic,
                                    Number = b.Number,
                                    Voucher = b.Voucher,
                                    // object fields
                                    FacilityData = b.FacilityData,
                                    }),
                                RoomStays = (ICollection<RoomStay>)a.RoomStays.Select(b => new RoomStay
                                    {
                                    // primitive fields
                                    RoomStayId = b.RoomStayId,
                                    // object fields
                                    RoomStayFacilities = (ICollection<RoomStayFacility>)b.RoomStayFacilities.Select(c => new RoomStayFacility
                                        {
                                        // primitive fields
                                        RoomStayFacilityId = c.RoomStayFacilityId,
                                        FacilityCode = c.FacilityCode,
                                        FacilityGroupCode = c.FacilityGroupCode,
                                        Number = c.Number,
                                        // object fields
                                        FacilityData = c.FacilityData,
                                        }),
                                    }),
                                RoomData = a.RoomData,
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



