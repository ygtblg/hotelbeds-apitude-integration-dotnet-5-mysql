/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:EfAddressDal.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Concrete\EntityFramework\App\EfAddressDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfAddressDal : EfEntityRepositoryBase<Address, ApplicationDbContext>, IAddressDal
    {
        
        public async Task<IList<Address>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<Address> query = db.Set<Address>().AsQueryable<Address>();
                query = query
                    .Select(
                        a => new Address
                        {

                                // primitive fields
                                AddressId = a.AddressId,
                                Content = a.Content,
                                Street = a.Street,
                                Number = a.Number,
                                // object fields
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



