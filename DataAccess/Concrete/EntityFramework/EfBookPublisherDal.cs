/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 15/08/2021 11:07:46
   ThisFileName:EfBookPublisherDal.cs
   ThisFilePath:C:\projects\BackendPartner\dotnet\agile-ionic-generator-001-api\DataAccess\Concrete\EntityFramework\EfBookPublisherDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfBookPublisherDal : EfEntityRepositoryBase<BookPublisher, ApplicationDbContext>, IBookPublisherDal
    {
        
        public async Task<IList<BookPublisher>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<BookPublisher> query = db.Set<BookPublisher>().AsQueryable<BookPublisher>();
                query = query
                    .Select(
                        a => new BookPublisher
                        {

                                // primitive fields
                                BookPublisherId = a.BookPublisherId,
                                // object fields
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



