/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 15/08/2021 11:07:46
   ThisFileName:EfLayoutDal.cs
   ThisFilePath:C:\projects\BackendPartner\dotnet\agile-ionic-generator-001-api\DataAccess\Concrete\EntityFramework\EfLayoutDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfLayoutDal : EfEntityRepositoryBase<Layout, ApplicationDbContext>, ILayoutDal
    {
        
        public async Task<IList<Layout>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<Layout> query = db.Set<Layout>().AsQueryable<Layout>();
                query = query
                    .Select(
                        a => new Layout
                        {

                                // primitive fields
                                LayoutId = a.LayoutId,
                                Uuid = a.Uuid,
                                LoggedIn = a.LoggedIn,
                                Error = a.Error,
                                Success = a.Success,
                                IsRunningMobile = a.IsRunningMobile,
                                WindowWidth = a.WindowWidth,
                                WindowHeight = a.WindowHeight,
                                // object fields
                                LayoutParams = (ICollection<LayoutParam>)a.LayoutParams.Select(b => new LayoutParam
                                    {
                                    // primitive fields
                                    LayoutParamId = b.LayoutParamId,
                                    Uuid = b.Uuid,
                                    Key = b.Key,
                                    Val = b.Val,
                                    LayoutUUID = b.LayoutUUID,
                                    // object fields
                                    }),
                                BookCategories = (ICollection<BookCategory>)a.BookCategories.Select(b => new BookCategory
                                    {
                                    // primitive fields
                                    BookCategoryId = b.BookCategoryId,
                                    Uuid = b.Uuid,
                                    Name = b.Name,
                                    LayoutUUID = b.LayoutUUID,
                                    // object fields
                                    Books = (ICollection<Book>)b.Books.Select(c => new Book
                                        {
                                        // primitive fields
                                        BookId = c.BookId,
                                        Uuid = c.Uuid,
                                        Name = c.Name,
                                        BookCategoryUUID = c.BookCategoryUUID,
                                        // object fields
                                        BookDetail = c.BookDetail,
                                        BookPublishers = (ICollection<BookPublisher>)c.BookPublishers.Select(d => new BookPublisher
                                            {
                                                Publisher = d.Publisher,
                                            }),
                                        }),
                                    }),
                                Publishers = (ICollection<Publisher>)a.Publishers.Select(b => new Publisher
                                    {
                                    // primitive fields
                                    PublisherId = b.PublisherId,
                                    Uuid = b.Uuid,
                                    Name = b.Name,
                                    // object fields
                                    BookPublishers = (ICollection<BookPublisher>)b.BookPublishers.Select(c => new BookPublisher
                                        {
                                            Book = c.Book,
                                        }),
                                    }),
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



