﻿using Microsoft.EntityFrameworkCore;
using System;
using Entities.Concrete;
using Core.Entities.Concrete;
using DataAccess.Concrete.EntityFramework.FluentConfiguration;
namespace DataAccess.Concrete.EntityFramework.Contexts
{
    public class ApplicationDbContext:DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            
            optionsBuilder.UseLazyLoadingProxies(false)
            .EnableDetailedErrors()
            .UseMySql("server=localhost;port=3306;user=xxxxxxxxxx;password=xxxxxxx;database=xxxxxxxxxxxxxxxxx;default command timeout=0;",
                        new MySqlServerVersion(new Version(8, 0, 22))
                    )
                    .EnableSensitiveDataLogging();
        }
        //  DbSetAddingsStart 
        public DbSet<BookingSite> BookingSite { get; set; }
        public DbSet<Board> Board { get; set; }
        public DbSet<Description> Description { get; set; }
        public DbSet<HotelBoard> HotelBoard { get; set; }
        public DbSet<Hotel> Hotel { get; set; }
        public DbSet<Accommodation> Accommodation { get; set; }
        public DbSet<TypeMultiDescription> TypeMultiDescription { get; set; }
        public DbSet<Address> Address { get; set; }
        public DbSet<Category> Category { get; set; }
        public DbSet<CategoryGroup> CategoryGroup { get; set; }
        public DbSet<Name> Name { get; set; }
        public DbSet<Chain> Chain { get; set; }
        public DbSet<City> City { get; set; }
        public DbSet<Coordinates> Coordinates { get; set; }
        public DbSet<Country> Country { get; set; }
        public DbSet<State> State { get; set; }
        public DbSet<Destination> Destination { get; set; }
        public DbSet<Zone> Zone { get; set; }
        public DbSet<Facility> Facility { get; set; }
        public DbSet<FacilityData> FacilityData { get; set; }
        public DbSet<FacilityGroup> FacilityGroup { get; set; }
        public DbSet<FacilityTypology> FacilityTypology { get; set; }
        public DbSet<HotelSegment> HotelSegment { get; set; }
        public DbSet<Image> Image { get; set; }
        public DbSet<ImageType> ImageType { get; set; }
        public DbSet<InterestPoint> InterestPoint { get; set; }
        public DbSet<Issue> Issue { get; set; }
        public DbSet<IssueData> IssueData { get; set; }
        public DbSet<Phone> Phone { get; set; }
        public DbSet<Room> Room { get; set; }
        public DbSet<RoomData> RoomData { get; set; }
        public DbSet<CharacteristicDescription> CharacteristicDescription { get; set; }
        public DbSet<TypeDescription> TypeDescription { get; set; }
        public DbSet<RoomFacility> RoomFacility { get; set; }
        public DbSet<RoomStay> RoomStay { get; set; }
        public DbSet<RoomStayFacility> RoomStayFacility { get; set; }
        public DbSet<Terminal> Terminal { get; set; }
        public DbSet<TerminalData> TerminalData { get; set; }
        public DbSet<Wildcard> Wildcard { get; set; }
        public DbSet<HotelRoomDescription> HotelRoomDescription { get; set; }
        public DbSet<Language> Language { get; set; }
        public DbSet<Segment> Segment { get; set; }
        //  DbSetAddingsEnd
        public DbSet<Layout> Layout { get; set; }
        public DbSet<BookCategory> BookCategory { get; set; }
        public DbSet<Book> Book { get; set; }
        public DbSet<BookDetail> BookDetail { get; set; }
        public DbSet<BookPublisher> BookPublisher { get; set; }
        public DbSet<LayoutParam> LayoutParam { get; set; }
        public DbSet<Publisher> Publisher { get; set; }
        
        public DbSet<User> Users { get; set; }
        
        public DbSet<UserOperationClaim> UserOperationClaims { get; set; }
        
        public DbSet<OperationClaim> OperationClaims { get; set; }
        
        public DbSet<UserImage> UserImages { get; set; }
        
        protected override void OnModelCreating(ModelBuilder builder){
 
            
             builder.ApplyConfiguration(new LayoutConfiguration());
            builder.ApplyConfiguration(new BookCategoryConfiguration());
            builder.ApplyConfiguration(new BookConfiguration());
            builder.ApplyConfiguration(new BookDetailConfiguration());
            builder.ApplyConfiguration(new BookPublisherConfiguration());
            builder.ApplyConfiguration(new LayoutParamConfiguration());
            builder.ApplyConfiguration(new PublisherConfiguration());
            
            // addingFluentConfigurationStart
            builder.ApplyConfiguration(new BookingSiteConfiguration());
            builder.ApplyConfiguration(new BoardConfiguration());
            builder.ApplyConfiguration(new DescriptionConfiguration());
            builder.ApplyConfiguration(new HotelBoardConfiguration());
            builder.ApplyConfiguration(new HotelConfiguration());
            builder.ApplyConfiguration(new AccommodationConfiguration());
            builder.ApplyConfiguration(new TypeMultiDescriptionConfiguration());
            builder.ApplyConfiguration(new AddressConfiguration());
            builder.ApplyConfiguration(new CategoryConfiguration());
            builder.ApplyConfiguration(new CategoryGroupConfiguration());
            builder.ApplyConfiguration(new NameConfiguration());
            builder.ApplyConfiguration(new ChainConfiguration());
            builder.ApplyConfiguration(new CityConfiguration());
            builder.ApplyConfiguration(new CoordinatesConfiguration());
            builder.ApplyConfiguration(new CountryConfiguration());
            builder.ApplyConfiguration(new StateConfiguration());
            builder.ApplyConfiguration(new DestinationConfiguration());
            builder.ApplyConfiguration(new ZoneConfiguration());
            builder.ApplyConfiguration(new FacilityConfiguration());
            builder.ApplyConfiguration(new FacilityDataConfiguration());
            builder.ApplyConfiguration(new FacilityGroupConfiguration());
            builder.ApplyConfiguration(new FacilityTypologyConfiguration());
            builder.ApplyConfiguration(new HotelSegmentConfiguration());
            builder.ApplyConfiguration(new ImageConfiguration());
            builder.ApplyConfiguration(new ImageTypeConfiguration());
            builder.ApplyConfiguration(new InterestPointConfiguration());
            builder.ApplyConfiguration(new IssueConfiguration());
            builder.ApplyConfiguration(new IssueDataConfiguration());
            builder.ApplyConfiguration(new PhoneConfiguration());
            builder.ApplyConfiguration(new RoomConfiguration());
            builder.ApplyConfiguration(new RoomDataConfiguration());
            builder.ApplyConfiguration(new CharacteristicDescriptionConfiguration());
            builder.ApplyConfiguration(new TypeDescriptionConfiguration());
            builder.ApplyConfiguration(new RoomFacilityConfiguration());
            builder.ApplyConfiguration(new RoomStayConfiguration());
            builder.ApplyConfiguration(new RoomStayFacilityConfiguration());
            builder.ApplyConfiguration(new TerminalConfiguration());
            builder.ApplyConfiguration(new TerminalDataConfiguration());
            builder.ApplyConfiguration(new WildcardConfiguration());
            builder.ApplyConfiguration(new HotelRoomDescriptionConfiguration());
            builder.ApplyConfiguration(new LanguageConfiguration());
            builder.ApplyConfiguration(new SegmentConfiguration());
            // addingFluentConfigurationEnd
            
        
        
        } // end of OnModelCreating
        
    } // end of class 'ApplicationDbContext'
}
