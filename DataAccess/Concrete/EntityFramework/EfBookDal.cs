/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 15/08/2021 11:07:46
   ThisFileName:EfBookDal.cs
   ThisFilePath:C:\projects\BackendPartner\dotnet\agile-ionic-generator-001-api\DataAccess\Concrete\EntityFramework\EfBookDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfBookDal : EfEntityRepositoryBase<Book, ApplicationDbContext>, IBookDal
    {
        
        public async Task<IList<Book>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<Book> query = db.Set<Book>().AsQueryable<Book>();
                query = query
                    .Select(
                        a => new Book
                        {

                                // primitive fields
                                BookId = a.BookId,
                                Uuid = a.Uuid,
                                Name = a.Name,
                                BookCategoryUUID = a.BookCategoryUUID,
                                // object fields
                                BookDetail = a.BookDetail,
                                BookPublishers = (ICollection<BookPublisher>)a.BookPublishers.Select(b => new BookPublisher
                                    {
                                        Publisher = b.Publisher,
                                    }),
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



