/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 15/08/2021 11:07:46
   ThisFileName:EfBookCategoryDal.cs
   ThisFilePath:C:\projects\BackendPartner\dotnet\agile-ionic-generator-001-api\DataAccess\Concrete\EntityFramework\EfBookCategoryDal.cs
*/

using System.Collections.Generic;
using Core.DataAccess.EntityFramework;
using DataAccess.Abstract;
using DataAccess.Concrete.EntityFramework.Contexts;
using Entities.Concrete;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace DataAccess.Concrete.EntityFramework
{
    public class EfBookCategoryDal : EfEntityRepositoryBase<BookCategory, ApplicationDbContext>, IBookCategoryDal
    {
        
        public async Task<IList<BookCategory>> getAllLevels()
        {
            using (var db = new ApplicationDbContext())
            {
                IQueryable<BookCategory> query = db.Set<BookCategory>().AsQueryable<BookCategory>();
                query = query
                    .Select(
                        a => new BookCategory
                        {

                                // primitive fields
                                BookCategoryId = a.BookCategoryId,
                                Uuid = a.Uuid,
                                Name = a.Name,
                                LayoutUUID = a.LayoutUUID,
                                // object fields
                                Books = (ICollection<Book>)a.Books.Select(b => new Book
                                    {
                                    // primitive fields
                                    BookId = b.BookId,
                                    Uuid = b.Uuid,
                                    Name = b.Name,
                                    BookCategoryUUID = b.BookCategoryUUID,
                                    // object fields
                                    BookDetail = b.BookDetail,
                                    BookPublishers = (ICollection<BookPublisher>)b.BookPublishers.Select(c => new BookPublisher
                                        {
                                            Publisher = c.Publisher,
                                        }),
                                    }),
                        }
                    );
                System.Console.WriteLine("QUERY :: " + query.ToQueryString());
                return await query.ToListAsync();
            }
        }
    }
}



