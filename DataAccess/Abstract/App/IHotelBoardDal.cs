/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:IHotelBoardDal.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Abstract\App\IHotelBoardDal.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.DataAccess;
using Entities.Concrete;

namespace DataAccess.Abstract
{
    public interface IHotelBoardDal:IEntityRepository<HotelBoard>
    {
    		Task<IList<HotelBoard>> getAllLevels();
    }
}
