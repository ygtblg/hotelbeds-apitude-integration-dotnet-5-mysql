/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:ITerminalDataDal.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Abstract\App\ITerminalDataDal.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.DataAccess;
using Entities.Concrete;

namespace DataAccess.Abstract
{
    public interface ITerminalDataDal:IEntityRepository<TerminalData>
    {
    		Task<IList<TerminalData>> getAllLevels();
    }
}
