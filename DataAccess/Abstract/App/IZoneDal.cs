/*
 * CREATED by YGTBLG (yigitbilge@gmail.com) on 01/09/2021 18:53:03
   ThisFileName:IZoneDal.cs
   ThisFilePath:C:\Generated\booking-site\server\DataAccess\Abstract\App\IZoneDal.cs
*/

using System.Collections.Generic;
using System.Threading.Tasks;
using Core.DataAccess;
using Entities.Concrete;

namespace DataAccess.Abstract
{
    public interface IZoneDal:IEntityRepository<Zone>
    {
    		Task<IList<Zone>> getAllLevels();
    }
}
